Run profile:
	mvn clean spring-boot:run -Dspring.profiles.active=beta5test88
	java -jar ./target/ibdbAccess.war --server.port=8081 --spring.profiles.active=beta5test92

To debug test:
    mvn clean test -Dmaven.surefire.debug
    mvn clean test -Dmaven.surefire.debug -Dtest_group=jarojas
    mvn clean test -Dmaven.surefire.debug -Dtest_group=jarojas -DargLine="-Dspring.profiles.active=beta5test92"

debug in 'spring-boot:run':
	mvn clean -Drun.jvmArguments="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5005" spring-boot:run

JProfilear agent for maven tests:
    mvn clean test -DargLine="-agentpath:/home/zero/jprofiler9/bin/linux-x64/libjprofilerti.so=port=8849"

set default spring profile in environment:
    add to .bashrc : export SPRING_PROFILES_ACTIVE=jarojas

ADD TAG
	git tag -a v0.9.1 -m 'for BMS v4.0.0-M6 and upper. New services for clients.'
    
Foundation for apps install

	-install ruby, nodejs and npm
	 	sudo apt-get install ruby2.0
	 	sudo apt-get install nodejs
		sudo apt-get install npm
				
	-update nodejs
		sudo npm cache clean -f
		sudo npm install -g n
		sudo n stable
				
	if sass module needs rebuild:
		npm rebuild node-sass
		
	-install foundation-cli, bower and gulp
		sudo npm install -g foundation-cli bower gulp
	 
	 fix error of "/usr/bin/env: node: No such file or directory"
	 	ln -s /usr/bin/nodejs /usr/bin/node
	 	
	 
	 new project:
	 	foundation new