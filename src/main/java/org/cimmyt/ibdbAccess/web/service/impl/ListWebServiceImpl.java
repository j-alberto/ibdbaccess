package org.cimmyt.ibdbAccess.web.service.impl;

import org.cimmyt.ibdbAccess.core.domain.Listnm;
import org.cimmyt.ibdbAccess.front.manager.ListManager;
import org.cimmyt.ibdbAccess.web.service.ListWebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/list", method=RequestMethod.GET)
public class ListWebServiceImpl implements ListWebService{

	private static final Logger LOG = LoggerFactory.getLogger(ListWebServiceImpl.class);
	
	@Autowired
	private ListManager listManager;
	
	@RequestMapping("/{id}")
	@Override
	public Listnm getList(@PathVariable Integer id){
		LOG.info("searching list with id: {}", id);
		return listManager.findList(id);
	}

}
