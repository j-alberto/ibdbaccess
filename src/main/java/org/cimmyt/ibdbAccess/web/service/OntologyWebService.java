package org.cimmyt.ibdbAccess.web.service;

import java.util.List;

import org.cimmyt.ibdbAccess.front.domain.StudyProperty;
import org.cimmyt.ibdbAccess.front.service.OntologyService;
/**
 * Web interface exposing REST methods to consume {@link OntologyService} methods
 * @author jarojas
 *
 */
public interface OntologyWebService {

	/**
	 * Returns the definition of the requested study properties
	 * @param studyPropertyIds the ids which a definition is needed
	 * @return a list of definitions for the given study properties
	 */
	List<StudyProperty> findStudyPropertyNames(List<Integer> studyPropertyIds);
	
}
