package org.cimmyt.ibdbAccess.web.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.ValidationException;

import org.cimmyt.ibdbAccess.front.domain.ErrorMessage;
import org.cimmyt.ibdbAccess.front.domain.GermplasmListEntry;
import org.cimmyt.ibdbAccess.front.domain.OccurrenceSimple;
import org.cimmyt.ibdbAccess.front.domain.Study;
import org.cimmyt.ibdbAccess.front.domain.StudyEntry;
import org.cimmyt.ibdbAccess.front.domain.StudySimple;
import org.cimmyt.ibdbAccess.front.service.StudyService;
import org.cimmyt.ibdbAccess.web.service.StudyWebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Implementation of {@link StudyWebService}
 * @author jarojas
 *
 */
@RestController
@RequestMapping(value="/siuapi/study", method=RequestMethod.GET)
public class StudyWebServiceImpl implements StudyWebService{

	private static final Logger LOG = LoggerFactory.getLogger(StudyWebServiceImpl.class);
	private StudyService studyService; 

	@Autowired
	public StudyWebServiceImpl(StudyService studyService) {
		super();
		this.studyService = studyService;
	}

	@RequestMapping(method=RequestMethod.POST)
	@Override
	public ResponseEntity<Integer> saveStudy(@RequestBody @Valid Study study, BindingResult bindingResult, 
			@RequestParam(name="withPenoData", defaultValue="true") boolean withPenoData){
		if(bindingResult.hasErrors()){
			throw new ValidationException(getAllErrors(bindingResult));
		}
		
		LOG.info("calling saveStudy for: {}, TID: {}", study.getName(), study.getTid());		
		int studyId = studyService.saveStudy(study, withPenoData);
		
		ResponseEntity<Integer> response = new ResponseEntity<>(studyId, HttpStatus.OK);
		return response;
	}
	
	/**
	 * Returns a String representation of all validation errors found
	 * @param bindingResult object containing possible validation errors
	 * @return a String representation of such errors
	 */
	private String getAllErrors(BindingResult bindingResult){
		List<ErrorMessage> errors = new ArrayList<ErrorMessage>(bindingResult.getAllErrors().size());
		
		for(FieldError err : bindingResult.getFieldErrors()){
			errors.add(new ErrorMessage(err.getField(), err.getRejectedValue(), err.getDefaultMessage()));
		}
		return errors.toString();
		
	}

	@Override
	@RequestMapping(method=RequestMethod.GET, params={"name"})
	public Page<StudySimple> findByName(@PageableDefault(page=0,size=50)Pageable pageRequest
			,@RequestParam(defaultValue="", value="name") String studyName) {
		LOG.info("Searching studies containing: '{}' for {}",studyName, pageRequest);
		return studyService.findByName(pageRequest, studyName);
	}

	@Override
	@RequestMapping(value="/{studyId}/occurrence", method=RequestMethod.GET)
	public Page<OccurrenceSimple> findOccurences(@PageableDefault(page=0,size=50) Pageable pageRequest
			, @PathVariable("studyId") Integer studyId) {
		LOG.info("Searching occurrences in study {}, for {}",studyId, pageRequest);
		return studyService.findOccurrences(pageRequest, studyId);
	}

	@Override
	@RequestMapping(value="/{studyId}/germplasm", method=RequestMethod.GET)
	public List<GermplasmListEntry> findStudyGermplasmList(@PathVariable("studyId") int studyId) {
		LOG.info("Searching germplasm list of study {}",studyId);
		return studyService.findStudyList(studyId);
	}

	@Override
	@RequestMapping(value="/{studyId}/occurrence/{occId}", method=RequestMethod.GET)
	public List<StudyEntry> findDesign(@PathVariable("occId") int occurrenceId) {
		LOG.info("Searching plot data in occurrence {}",occurrenceId);
		return studyService.findDesign(occurrenceId);
	}
	
}
