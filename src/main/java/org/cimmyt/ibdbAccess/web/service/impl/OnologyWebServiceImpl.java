package org.cimmyt.ibdbAccess.web.service.impl;

import java.util.List;

import org.cimmyt.ibdbAccess.front.domain.StudyProperty;
import org.cimmyt.ibdbAccess.front.service.OntologyService;
import org.cimmyt.ibdbAccess.web.service.OntologyWebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/siuapi/onto", method=RequestMethod.GET)
public class OnologyWebServiceImpl implements OntologyWebService{

	private static final Logger LOG = LoggerFactory.getLogger(OnologyWebServiceImpl.class);

	private OntologyService ontologyService;

	@Autowired
	public OnologyWebServiceImpl(OntologyService ontologyService) {
		super();
		this.ontologyService = ontologyService;
	}

	@Override
	@RequestMapping(value="/studyTerms", params={"ids"})
	public List<StudyProperty> findStudyPropertyNames(@RequestParam(defaultValue="", value="ids") List<Integer> studyPropertyIds) {
		LOG.info("searching for study properties: " + studyPropertyIds);
		return ontologyService.findStudyPropertyNames(studyPropertyIds);
	}

}
