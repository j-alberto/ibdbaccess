package org.cimmyt.ibdbAccess.web.service;

import org.cimmyt.ibdbAccess.core.domain.Listnm;

public interface ListWebService {

	Listnm getList(Integer id);
}
