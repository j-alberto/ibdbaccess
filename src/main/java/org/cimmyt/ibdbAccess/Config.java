package org.cimmyt.ibdbAccess;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
/**
 * We want our application to be able to run in a servlet container (tomcat, jboss, etc) so
 * we extend from SpringBootServletInitializer, just to specify where to find {@link @Configuration} classes
 * @author jarojas
 *
 */
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
@SpringBootApplication
public class Config extends SpringBootServletInitializer{

	public static void main(String[] args) {
        SpringApplication app = new SpringApplication(Config.class);
        app.run(args);
    }
	
	@Override
    protected SpringApplicationBuilder configure(
            SpringApplicationBuilder application) {
        application.sources(Config.class);
        return application;
    }
	
	@Bean
	public WebMvcConfigurerAdapter corsConfigurer(){
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/siuapi/**")
					.allowedOrigins("http://localhost:8079");
			}
		};
	}
}
