package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


/**
 * The persistent class for the nd_experiment_phenotype database table.
 * 
 */
@Entity
@Table(name="nd_experiment_phenotype")
@NamedQuery(name="NdExperimentPhenotype.findAll", query="SELECT n FROM NdExperimentPhenotype n")
public class NdExperimentPhenotype implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GenericGenerator(name="increment", strategy = "increment") 
    @GeneratedValue(generator="increment")
	@Column(name="nd_experiment_phenotype_id")
	private Integer ndExperimentPhenotypeId;

	//bi-directional many-to-one association to NdExperiment
	@ManyToOne
	@JoinColumn(name="nd_experiment_id")
	private NdExperiment ndExperiment;

	//bi-directional many-to-one association to Phenotype
	@ManyToOne
	@JoinColumn(name="phenotype_id")
	private Phenotype phenotype;

	public NdExperimentPhenotype() {
	}
	
	public NdExperimentPhenotype(NdExperiment ndExperiment, Phenotype phenotype) {
        super();
        this.ndExperiment = ndExperiment;
        this.phenotype = phenotype;
    }



    public Integer getNdExperimentPhenotypeId() {
		return this.ndExperimentPhenotypeId;
	}

	public void setNdExperimentPhenotypeId(Integer ndExperimentPhenotypeId) {
		this.ndExperimentPhenotypeId = ndExperimentPhenotypeId;
	}

	public NdExperiment getNdExperiment() {
		return this.ndExperiment;
	}

	public void setNdExperiment(NdExperiment ndExperiment) {
		this.ndExperiment = ndExperiment;
	}

	public Phenotype getPhenotype() {
		return this.phenotype;
	}

	public void setPhenotype(Phenotype phenotype) {
		this.phenotype = phenotype;
	}

}