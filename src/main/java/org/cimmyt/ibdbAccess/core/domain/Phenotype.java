package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


/**
 * The persistent class for the phenotype database table.
 * 
 */
@Entity
@Table(name="phenotype")
@NamedQuery(name="Phenotype.findAll", query="SELECT p FROM Phenotype p")
public class Phenotype implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name="increment", strategy = "increment")
	@GeneratedValue(generator="increment")
	@Column(name="phenotype_id")
	private int phenotypeId;

	private String name;

	private String uniquename;

	private String value;

    @ManyToOne
    @JoinColumn(name="observable_id")
	private Cvterm observableId;

    @ManyToOne
    @JoinColumn(name="attr_id")
	private Cvterm attrId;

    @ManyToOne
    @JoinColumn(name="cvalue_id")
	private Cvterm cValueId;

    @ManyToOne
    @JoinColumn(name="assay_id")
	private Cvterm assayId;

	public Phenotype() {
	}

	public Phenotype(Cvterm trait, String value) {
        super();
        this.name = trait.getCvtermId().toString();
        this.observableId = trait;
        this.value = value;
    }

    public int getPhenotypeId() {
        return phenotypeId;
    }

    public void setPhenotypeId(int phenotypeId) {
        this.phenotypeId = phenotypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUniquename() {
        return uniquename;
    }

    public void setUniquename(String uniquename) {
        this.uniquename = uniquename;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Cvterm getObservableId() {
        return observableId;
    }

    public void setObservableId(Cvterm observableId) {
        this.observableId = observableId;
    }

    public Cvterm getAttrId() {
        return attrId;
    }

    public void setAttrId(Cvterm attrId) {
        this.attrId = attrId;
    }

    public Cvterm getcValueId() {
        return cValueId;
    }

    public void setcValueId(Cvterm cValueId) {
        this.cValueId = cValueId;
    }

    public Cvterm getAssayId() {
        return assayId;
    }

    public void setAssayId(Cvterm assayId) {
        this.assayId = assayId;
    }
}