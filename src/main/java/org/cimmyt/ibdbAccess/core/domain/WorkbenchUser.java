package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the users database table.
 * 
 */
@Entity
@Table(name="workbench.users")
@NamedQuery(name="WorkbenchUser.findAll", query="SELECT u FROM WorkbenchUser u")
public class WorkbenchUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int userid;

	private int adate;

	private int cdate;

	private int instalid;

	private int personid;

	private int uaccess;

	private String uname;

	private String upswd;

	private int ustatus;

	private int utype;

	//bi-directional many-to-one association to UsersRole
	@OneToMany(mappedBy="user")
	private List<WorkbenchUserRole> usersRoles;

	public WorkbenchUser() {
	}

	public int getUserid() {
		return this.userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public int getAdate() {
		return this.adate;
	}

	public void setAdate(int adate) {
		this.adate = adate;
	}

	public int getCdate() {
		return this.cdate;
	}

	public void setCdate(int cdate) {
		this.cdate = cdate;
	}

	public int getInstalid() {
		return this.instalid;
	}

	public void setInstalid(int instalid) {
		this.instalid = instalid;
	}

	public int getPersonid() {
		return this.personid;
	}

	public void setPersonid(int personid) {
		this.personid = personid;
	}

	public int getUaccess() {
		return this.uaccess;
	}

	public void setUaccess(int uaccess) {
		this.uaccess = uaccess;
	}

	public String getUname() {
		return this.uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getUpswd() {
		return this.upswd;
	}

	public void setUpswd(String upswd) {
		this.upswd = upswd;
	}

	public int getUstatus() {
		return this.ustatus;
	}

	public void setUstatus(int ustatus) {
		this.ustatus = ustatus;
	}

	public int getUtype() {
		return this.utype;
	}

	public void setUtype(int utype) {
		this.utype = utype;
	}

	public List<WorkbenchUserRole> getUsersRoles() {
		return this.usersRoles;
	}

	public void setUsersRoles(List<WorkbenchUserRole> usersRoles) {
		this.usersRoles = usersRoles;
	}

	public WorkbenchUserRole addUsersRole(WorkbenchUserRole usersRole) {
		getUsersRoles().add(usersRole);
		usersRole.setUser(this);

		return usersRole;
	}

	public WorkbenchUserRole removeUsersRole(WorkbenchUserRole usersRole) {
		getUsersRoles().remove(usersRole);
		usersRole.setUser(null);

		return usersRole;
	}
}