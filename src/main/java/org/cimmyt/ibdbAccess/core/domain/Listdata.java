package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;


/**
 * The persistent class for the listdata database table.
 * 
 */
@Entity
@Table(name="listdata")
@NamedQuery(name="Listdata.findAll", query="SELECT l FROM Listdata l")
public class Listdata implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name="increment", strategy = "increment")
	@GeneratedValue(generator="increment")
	private int lrecid;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="listid")
	private Listnm list;

	private String desig;

	private String entrycd;

	private Integer entryid;

	private Integer gid;

	private String grpname;

	private Integer llrecid;

	private Integer lrstatus;

	private String source;

	public Listdata() {
	}

	public String getDesig() {
		return this.desig;
	}

	public void setDesig(String desig) {
		this.desig = desig;
	}

	public String getEntrycd() {
		return this.entrycd;
	}

	public void setEntrycd(String entrycd) {
		this.entrycd = entrycd;
	}

	public Integer getEntryid() {
		return this.entryid;
	}

	public void setEntryid(Integer entryid) {
		this.entryid = entryid;
	}

	public Integer getGid() {
		return this.gid;
	}

	public void setGid(Integer gid) {
		this.gid = gid;
	}

	public String getGrpname() {
		return this.grpname;
	}

	public void setGrpname(String grpname) {
		this.grpname = grpname;
	}

	public Integer getLlrecid() {
		return this.llrecid;
	}

	public void setLlrecid(Integer llrecid) {
		this.llrecid = llrecid;
	}

	public Integer getLrstatus() {
		return this.lrstatus;
	}

	public void setLrstatus(Integer lrstatus) {
		this.lrstatus = lrstatus;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public int getLrecid() {
		return lrecid;
	}

	public void setLrecid(int lrecid) {
		this.lrecid = lrecid;
	}

	public Listnm getList() {
		return list;
	}

	public void setList(Listnm list) {
		this.list = list;
	}
}