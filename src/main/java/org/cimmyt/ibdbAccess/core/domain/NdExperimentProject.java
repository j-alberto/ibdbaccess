package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;


/**
 * The persistent class for the nd_experiment_project database table.
 * 
 */
@Entity
@Table(name="nd_experiment_project")
@NamedQuery(name="NdExperimentProject.findAll", query="SELECT n FROM NdExperimentProject n")
public class NdExperimentProject implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GenericGenerator(name="increment", strategy = "increment") 
    @GeneratedValue(generator="increment")
	@Column(name="nd_experiment_project_id")
	private Integer ndExperimentProjectId;

	//bi-directional many-to-one association to Project
	@ManyToOne
	@JoinColumn(name="project_id")
	private Project project;

	//bi-directional many-to-one association to NdExperiment
	@ManyToOne
	@JoinColumn(name="nd_experiment_id")
	private NdExperiment ndExperiment;

	public NdExperimentProject() {
	}

	public NdExperimentProject(Project project, NdExperiment ndExperiment) {
		this.project = project;
		this.ndExperiment = ndExperiment;
	}

	public Integer getNdExperimentProjectId() {
		return this.ndExperimentProjectId;
	}

	public void setNdExperimentProjectId(Integer ndExperimentProjectId) {
		this.ndExperimentProjectId = ndExperimentProjectId;
	}

	public Project getProject() {
		return this.project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public NdExperiment getNdExperiment() {
		return this.ndExperiment;
	}

	public void setNdExperiment(NdExperiment ndExperiment) {
		this.ndExperiment = ndExperiment;
	}

}