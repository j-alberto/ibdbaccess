package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;


/**
 * The persistent class for the workbench_project database table.
 * 
 */
@Entity
@Table(name="workbench.workbench_project")
@NamedQuery(name="WorkbenchProject.findAll", query="SELECT w FROM WorkbenchProject w")
public class WorkbenchProject implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="project_id")
	@GenericGenerator(name="increment", strategy = "increment") 
    @GeneratedValue(generator="increment")
	private int projectId;

	@Column(name="last_open_date")
	private Timestamp lastOpenDate;

	@Column(name="project_name")
	private String projectName;

	@Column(name="project_uuid")
	private String projectUuid;

	@Temporal(TemporalType.DATE)
	@Column(name="start_date")
	private Date startDate;

	@Column(name="user_id")
	private int userId;

	//bi-directional many-to-one association to WorkbenchCrop
	@ManyToOne
	@JoinColumn(name="crop_type")
	private WorkbenchCrop workbenchCrop;

	public WorkbenchProject() {
	}

	public int getProjectId() {
		return this.projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public Timestamp getLastOpenDate() {
		return this.lastOpenDate;
	}

	public void setLastOpenDate(Timestamp lastOpenDate) {
		this.lastOpenDate = lastOpenDate;
	}

	public String getProjectName() {
		return this.projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectUuid() {
		return this.projectUuid;
	}

	public void setProjectUuid(String projectUuid) {
		this.projectUuid = projectUuid;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public WorkbenchCrop getWorkbenchCrop() {
		return this.workbenchCrop;
	}

	public void setWorkbenchCrop(WorkbenchCrop workbenchCrop) {
		this.workbenchCrop = workbenchCrop;
	}

}