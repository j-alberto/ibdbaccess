package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the workbench_crop database table.
 * 
 */
@Entity
@Table(name="workbench.workbench_crop")
@NamedQuery(name="WorkbenchCrop.findAll", query="SELECT w FROM WorkbenchCrop w")
public class WorkbenchCrop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="crop_name")
	private String cropName;

	@Column(name="db_name")
	private String dbName;

	@Column(name="schema_version")
	private String schemaVersion;

	public WorkbenchCrop() {
	}

	public String getCropName() {
		return this.cropName;
	}

	public void setCropName(String cropName) {
		this.cropName = cropName;
	}

	public String getDbName() {
		return this.dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public String getSchemaVersion() {
		return this.schemaVersion;
	}

	public void setSchemaVersion(String schemaVersion) {
		this.schemaVersion = schemaVersion;
	}

}