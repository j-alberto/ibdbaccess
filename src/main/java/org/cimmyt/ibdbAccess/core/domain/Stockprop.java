package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;


/**
 * The persistent class for the stockprop database table.
 * 
 */
@Entity
@Table(name="stockprop")
@NamedQuery(name="Stockprop.findAll", query="SELECT s FROM Stockprop s")
public class Stockprop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GenericGenerator(name="increment", strategy = "increment") 
    @GeneratedValue(generator="increment")
	@Column(name="stockprop_id")
	private int stockpropId;

	private int rank;

	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm type;

	private String value;

	//bi-directional many-to-one association to Stock
	@ManyToOne
	@JoinColumn(name="stock_id")
	private Stock stock;

	public Stockprop() {
	}

	public Stockprop(Stock stock, int rank, Cvterm type, String value) {
		super();
		this.stock = stock;
		this.rank = rank;
		this.type = type;
		this.value = value;
	}



	public int getStockpropId() {
		return this.stockpropId;
	}

	public void setStockpropId(int stockpropId) {
		this.stockpropId = stockpropId;
	}

	public int getRank() {
		return this.rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public Cvterm getType() {
		return this.type;
	}

	public void setType(Cvterm type) {
		this.type = type;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Stock getStock() {
		return this.stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

}