package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the workbench_project_user_role database table.
 * 
 */
@Entity
@Table(name="workbench.workbench_project_user_role")
@NamedQuery(name="WorkbenchProjectUserRole.findAll", query="SELECT w FROM WorkbenchProjectUserRole w")
public class WorkbenchProjectUserRole implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="project_user_id")
	private int projectUserId;

	@Column(name="role_id")
	private int roleId;

	@Column(name="user_id")
	private int userId;

	//bi-directional many-to-one association to WorkbenchProject
	@ManyToOne
	@JoinColumn(name="project_id")
	private WorkbenchProject workbenchProject;

	public WorkbenchProjectUserRole() {
	}

	public int getProjectUserId() {
		return this.projectUserId;
	}

	public void setProjectUserId(int projectUserId) {
		this.projectUserId = projectUserId;
	}

	public int getRoleId() {
		return this.roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public WorkbenchProject getWorkbenchProject() {
		return this.workbenchProject;
	}

	public void setWorkbenchProject(WorkbenchProject workbenchProject) {
		this.workbenchProject = workbenchProject;
	}

}