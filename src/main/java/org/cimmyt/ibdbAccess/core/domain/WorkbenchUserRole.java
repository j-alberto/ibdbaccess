package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the users_roles database table.
 * 
 */
@Entity
@Table(name="workbench.users_roles")
@NamedQuery(name="WorkbenchUserRole.findAll", query="SELECT u FROM WorkbenchUserRole u")
public class WorkbenchUserRole implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String role;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="userid")
	private WorkbenchUser user;

	public WorkbenchUserRole() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public WorkbenchUser getUser() {
		return this.user;
	}

	public void setUser(WorkbenchUser user) {
		this.user = user;
	}

}