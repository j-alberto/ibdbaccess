package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;


/**
 * The persistent class for the location database table.
 * 
 */
@Entity
@Table(name="location")
@NamedQuery(name="Location.findAll", query="SELECT l FROM Location l")
public class Location implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GenericGenerator(name="increment", strategy = "increment") 
    @GeneratedValue(generator="increment")
	private int locid;

	private int cntryid;

	private String labbr;

	private String lname;

	private int lrplce;

	private int ltype;

	private int nllp;

	private int nnpid;

	@Column(name="program_uuid")
	private String programUuid;

	private int snl1id;

	private int snl2id;

	private int snl3id;

	public Location() {
	}

	public int getLocid() {
		return this.locid;
	}

	public void setLocid(int locid) {
		this.locid = locid;
	}

	public int getCntryid() {
		return this.cntryid;
	}

	public void setCntryid(int cntryid) {
		this.cntryid = cntryid;
	}

	public String getLabbr() {
		return this.labbr;
	}

	public void setLabbr(String labbr) {
		this.labbr = labbr;
	}

	public String getLname() {
		return this.lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public int getLrplce() {
		return this.lrplce;
	}

	public void setLrplce(int lrplce) {
		this.lrplce = lrplce;
	}

	public int getLtype() {
		return this.ltype;
	}

	public void setLtype(int ltype) {
		this.ltype = ltype;
	}

	public int getNllp() {
		return this.nllp;
	}

	public void setNllp(int nllp) {
		this.nllp = nllp;
	}

	public int getNnpid() {
		return this.nnpid;
	}

	public void setNnpid(int nnpid) {
		this.nnpid = nnpid;
	}

	public String getProgramUuid() {
		return this.programUuid;
	}

	public void setProgramUuid(String programUuid) {
		this.programUuid = programUuid;
	}

	public int getSnl1id() {
		return this.snl1id;
	}

	public void setSnl1id(int snl1id) {
		this.snl1id = snl1id;
	}

	public int getSnl2id() {
		return this.snl2id;
	}

	public void setSnl2id(int snl2id) {
		this.snl2id = snl2id;
	}

	public int getSnl3id() {
		return this.snl3id;
	}

	public void setSnl3id(int snl3id) {
		this.snl3id = snl3id;
	}

}