package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;


/**
 * The persistent class for the cvterm database table.
 * 
 */
@Entity
@Table(name="cvterm")
@NamedQuery(name="Cvterm.findAll", query="SELECT c FROM Cvterm c")
@Cache(region="cvterm", usage=CacheConcurrencyStrategy.READ_ONLY)
public class Cvterm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="cvterm_id")
	private Integer cvtermId;

	@Column(name="dbxref_id")
	private Integer dbxrefId;

	private String definition;

	@Column(name="is_obsolete")
	private Integer isObsolete;

	@Column(name="is_relationshiptype")
	private Integer isRelationshiptype;

	private String name;

	//bi-directional many-to-one association to Cv
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cv_id")
	private Cv cv;

	public Cvterm() {
	}
	
	public Cvterm(int cvtermId){
		this.cvtermId = cvtermId;
	}

	public Integer getCvtermId() {
		return this.cvtermId;
	}

	public void setCvtermId(Integer cvtermId) {
		this.cvtermId = cvtermId;
	}

	public Integer getDbxrefId() {
		return this.dbxrefId;
	}

	public void setDbxrefId(Integer dbxrefId) {
		this.dbxrefId = dbxrefId;
	}

	public String getDefinition() {
		return this.definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public Integer getIsObsolete() {
		return this.isObsolete;
	}

	public void setIsObsolete(Integer isObsolete) {
		this.isObsolete = isObsolete;
	}

	public Integer getIsRelationshiptype() {
		return this.isRelationshiptype;
	}

	public void setIsRelationshiptype(Integer isRelationshiptype) {
		this.isRelationshiptype = isRelationshiptype;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Cv getCv() {
		return this.cv;
	}

	public void setCv(Cv cv) {
		this.cv = cv;
	}
}