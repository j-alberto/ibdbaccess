package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


/**
 * The persistent class for the stock database table.
 * 
 */
@Entity
@Table(name="stock")
@NamedQuery(name="Stock.findAll", query="SELECT s FROM Stock s")
public class Stock implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GenericGenerator(name="increment", strategy = "increment") 
    @GeneratedValue(generator="increment")
	@Column(name="stock_id")
	private int stockId;

	@Column(name="dbxref_id")
	private int dbxrefId;

	private String description;

	@Column(name="is_obsolete")
	private byte isObsolete;

	private String name;

	@Column(name="organism_id")
	private Integer organismId;

	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm type;

	private String uniquename;

	private String value;

	public Stock() {
	}

	public Stock(int dbxrefId, byte isObsolete, String name, Cvterm type,
			String uniquename) {
		super();
		this.dbxrefId = dbxrefId;
		this.isObsolete = isObsolete;
		this.name = name;
		this.type = type;
		this.uniquename = uniquename;
	}



	public int getStockId() {
		return this.stockId;
	}

	public void setStockId(int stockId) {
		this.stockId = stockId;
	}

	public int getDbxrefId() {
		return this.dbxrefId;
	}

	public void setDbxrefId(int dbxrefId) {
		this.dbxrefId = dbxrefId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public byte getIsObsolete() {
		return this.isObsolete;
	}

	public void setIsObsolete(byte isObsolete) {
		this.isObsolete = isObsolete;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOrganismId() {
		return this.organismId;
	}

	public void setOrganismId(Integer organismId) {
		this.organismId = organismId;
	}

	public Cvterm getType() {
		return this.type;
	}

	public void setType(Cvterm type) {
		this.type = type;
	}

	public String getUniquename() {
		return this.uniquename;
	}

	public void setUniquename(String uniquename) {
		this.uniquename = uniquename;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}