package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


/**
 * The persistent class for the project_relationship database table.
 * 
 */
@Entity
@Table(name="project_relationship")
@NamedQuery(name="ProjectRelationship.findAll", query="SELECT p FROM ProjectRelationship p")
public class ProjectRelationship implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="project_relationship_id")
	@GenericGenerator(name="increment", strategy = "increment")
	@GeneratedValue(generator="increment")
	private int projectRelationshipId;

	//bi-directional many-to-one association to Project
	@ManyToOne
	@JoinColumn(name="subject_project_id")
	private Project subject;

	//bi-directional many-to-one association to Project
	@ManyToOne
	@JoinColumn(name="object_project_id")
	private Project object;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm relationshipType;

	public ProjectRelationship(Project subject,
		Project object, Cvterm relationshipType) {
		this.subject = subject;
		this.object = object;
		this.relationshipType = relationshipType;
	}
	
	public ProjectRelationship() {
	}
	

	public int getProjectRelationshipId() {
		return this.projectRelationshipId;
	}

	public void setProjectRelationshipId(int projectRelationshipId) {
		this.projectRelationshipId = projectRelationshipId;
	}

	public Project getObject() {
		return this.object;
	}

	public void setObject(Project object) {
		this.object = object;
	}

	public Project getSubject() {
		return this.subject;
	}

	public void setSubject(Project subject) {
		this.subject = subject;
	}

	public Cvterm getRelationshipType() {
		return this.relationshipType;
	}

	public void setRelationshipType(Cvterm relationshipType) {
		this.relationshipType = relationshipType;
	}

}