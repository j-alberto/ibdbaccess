package org.cimmyt.ibdbAccess.core.domain;

import java.util.Arrays;
import java.util.List;

import org.cimmyt.ibdbAccess.front.domain.Study;


/**
 * Wrapper for the 3 {@link Project} instances that define a single {@link Study} once this is
 * saved into the model. These three instances represent the study, environment and plot datasets.
 * @author jarojas
 *
 */
public class StudyId {

	private final Project mainProject;
	private final Project environmentProject;
	private final Project plotDataProject;
	private List<Integer> idList;

	public StudyId(Project mainProject, Project environmentProject, Project plotDataProject) {
		this.mainProject = mainProject;
		this.environmentProject = environmentProject;
		this.plotDataProject = plotDataProject;
		createIdSet();
	}
	public StudyId(int mainProjectId){
		this(new Project(mainProjectId)
				,new Project(mainProjectId+1)
				,new Project(mainProjectId+2));
	}
	
	public int getMainId() {
		return mainProject.getProjectId();
	}
	public int getEnvironmentId() {
		return environmentProject.getProjectId();
	}
	public int getPlotDataId() {
		return plotDataProject.getProjectId();
	}

	public Project getMainProject() {
		return mainProject;
	}

	public Project getEnvironmentProject() {
		return environmentProject;
	}

	public Project getPlotDataProject() {
		return plotDataProject;
	}
	
	private void createIdSet(){
		idList = Arrays.asList(mainProject.getProjectId()
				,environmentProject.getProjectId()
				,plotDataProject.getProjectId());
	}
	
	public List<Integer> getIds(){
		return idList;
	}
	
}
