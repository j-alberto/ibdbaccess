package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;


/**
 * The persistent class for the nd_experiment_stock database table.
 * 
 */
@Entity
@Table(name="nd_experiment_stock")
@NamedQuery(name="NdExperimentStock.findAll", query="SELECT n FROM NdExperimentStock n")
public class NdExperimentStock implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GenericGenerator(name="increment", strategy = "increment") 
    @GeneratedValue(generator="increment")
	@Column(name="nd_experiment_stock_id")
	private int ndExperimentStockId;

	@ManyToOne
	@JoinColumn(name="nd_experiment_id")
	private NdExperiment ndExperiment;

	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm type;

	//bi-directional many-to-one association to Stock
	@ManyToOne
	@JoinColumn(name="stock_id")
	private Stock stock;

	public NdExperimentStock() {
	}

	public NdExperimentStock(NdExperiment ndExperiment, Cvterm type, Stock stock) {
		super();
		this.ndExperiment = ndExperiment;
		this.type = type;
		this.stock = stock;
	}


	public int getNdExperimentStockId() {
		return this.ndExperimentStockId;
	}

	public void setNdExperimentStockId(int ndExperimentStockId) {
		this.ndExperimentStockId = ndExperimentStockId;
	}

	public NdExperiment getNdExperiment() {
		return this.ndExperiment;
	}

	public void setNdExperiment(NdExperiment ndExperiment) {
		this.ndExperiment = ndExperiment;
	}

	public Cvterm getType() {
		return this.type;
	}

	public void setType(Cvterm type) {
		this.type = type;
	}

	public Stock getStock() {
		return this.stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

}