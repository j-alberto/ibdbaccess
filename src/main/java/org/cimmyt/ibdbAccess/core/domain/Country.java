package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;


/**
 * The persistent class for the cntry database table.
 * 
 */
@Entity
@Table(name="cntry")
@NamedQuery(name="Country.findAll", query="SELECT c FROM Country c")
public class Country implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GenericGenerator(name="increment", strategy = "increment") 
    @GeneratedValue(generator="increment")
	private int cntryid;

	private int cchange;

	private String cont;

	private int ecntry;

	private String faothree;

	private String fips;

	private String isoabbr;

	private String isofull;

	private int isonum;

	private String isothree;

	private String isotwo;

	private int scntry;

	private String wb;

	public Country() {
	}

	public int getCntryid() {
		return this.cntryid;
	}

	public void setCntryid(int cntryid) {
		this.cntryid = cntryid;
	}

	public int getCchange() {
		return this.cchange;
	}

	public void setCchange(int cchange) {
		this.cchange = cchange;
	}

	public String getCont() {
		return this.cont;
	}

	public void setCont(String cont) {
		this.cont = cont;
	}

	public int getEcntry() {
		return this.ecntry;
	}

	public void setEcntry(int ecntry) {
		this.ecntry = ecntry;
	}

	public String getFaothree() {
		return this.faothree;
	}

	public void setFaothree(String faothree) {
		this.faothree = faothree;
	}

	public String getFips() {
		return this.fips;
	}

	public void setFips(String fips) {
		this.fips = fips;
	}

	public String getIsoabbr() {
		return this.isoabbr;
	}

	public void setIsoabbr(String isoabbr) {
		this.isoabbr = isoabbr;
	}

	public String getIsofull() {
		return this.isofull;
	}

	public void setIsofull(String isofull) {
		this.isofull = isofull;
	}

	public int getIsonum() {
		return this.isonum;
	}

	public void setIsonum(int isonum) {
		this.isonum = isonum;
	}

	public String getIsothree() {
		return this.isothree;
	}

	public void setIsothree(String isothree) {
		this.isothree = isothree;
	}

	public String getIsotwo() {
		return this.isotwo;
	}

	public void setIsotwo(String isotwo) {
		this.isotwo = isotwo;
	}

	public int getScntry() {
		return this.scntry;
	}

	public void setScntry(int scntry) {
		this.scntry = scntry;
	}

	public String getWb() {
		return this.wb;
	}

	public void setWb(String wb) {
		this.wb = wb;
	}

}