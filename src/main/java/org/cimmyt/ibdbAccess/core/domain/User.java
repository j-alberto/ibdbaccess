package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the users database table.
 * 
 */
@Entity
@Table(name="users")
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	
	public static int TYPE_LOCAL_ADMIN = 422;
	public static int DEFAULT_ACCESS_RIGTHS = 100;
	
	private static final long serialVersionUID = 1L;

	@Id
	private int userid;

	private int adate;

	private int cdate;

	private int instalid;

	private int personid;

	private int uaccess;

	private String uname;

	private String upswd;

	private int ustatus;

	private int utype;

	public User() {
	}

	public int getUserid() {
		return this.userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public int getAdate() {
		return this.adate;
	}

	public void setAdate(int adate) {
		this.adate = adate;
	}

	public int getCdate() {
		return this.cdate;
	}

	public void setCdate(int cdate) {
		this.cdate = cdate;
	}

	public int getInstalid() {
		return this.instalid;
	}

	public void setInstalid(int instalid) {
		this.instalid = instalid;
	}

	public int getPersonid() {
		return this.personid;
	}

	public void setPersonid(int personid) {
		this.personid = personid;
	}

	public int getUaccess() {
		return this.uaccess;
	}

	public void setUaccess(int uaccess) {
		this.uaccess = uaccess;
	}

	public String getUname() {
		return this.uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getUpswd() {
		return this.upswd;
	}

	public void setUpswd(String upswd) {
		this.upswd = upswd;
	}

	public int getUstatus() {
		return this.ustatus;
	}

	public void setUstatus(int ustatus) {
		this.ustatus = ustatus;
	}

	public int getUtype() {
		return this.utype;
	}

	public void setUtype(int utype) {
		this.utype = utype;
	}

}