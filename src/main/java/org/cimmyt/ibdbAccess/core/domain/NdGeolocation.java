package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import java.util.List;


/**
 * The persistent class for the nd_geolocation database table.
 * 
 */
@Entity
@Table(name="nd_geolocation")
@NamedQuery(name="NdGeolocation.findAll", query="SELECT n FROM NdGeolocation n")
public class NdGeolocation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="nd_geolocation_id")
	@GenericGenerator(name="increment", strategy = "increment")
	@GeneratedValue(generator="increment")
	private int ndGeolocationId;

	private float altitude;

	private String description;

	@Column(name="geodetic_datum")
	private String geodeticDatum;

	private float latitude;

	private float longitude;

	//bi-directional many-to-one association to NdExperiment
	@OneToMany(mappedBy="ndGeolocation")
	private List<NdExperiment> ndExperiments;

	//bi-directional many-to-one association to NdGeolocationprop
	@OneToMany(mappedBy="ndGeolocation")
	private List<NdGeolocationprop> ndGeolocationprops;

	public NdGeolocation() {
	}

    public NdGeolocation(String description) {
        this.description = description;
    }

	public int getNdGeolocationId() {
		return this.ndGeolocationId;
	}

	public void setNdGeolocationId(int ndGeolocationId) {
		this.ndGeolocationId = ndGeolocationId;
	}

	public float getAltitude() {
		return this.altitude;
	}

	public void setAltitude(float altitude) {
		this.altitude = altitude;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getGeodeticDatum() {
		return this.geodeticDatum;
	}

	public void setGeodeticDatum(String geodeticDatum) {
		this.geodeticDatum = geodeticDatum;
	}

	public float getLatitude() {
		return this.latitude;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	public float getLongitude() {
		return this.longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	public List<NdExperiment> getNdExperiments() {
		return this.ndExperiments;
	}

	public void setNdExperiments(List<NdExperiment> ndExperiments) {
		this.ndExperiments = ndExperiments;
	}

	public NdExperiment addNdExperiment(NdExperiment ndExperiment) {
		getNdExperiments().add(ndExperiment);
		ndExperiment.setNdGeolocation(this);

		return ndExperiment;
	}

	public NdExperiment removeNdExperiment(NdExperiment ndExperiment) {
		getNdExperiments().remove(ndExperiment);
		ndExperiment.setNdGeolocation(null);

		return ndExperiment;
	}

	public List<NdGeolocationprop> getNdGeolocationprops() {
		return this.ndGeolocationprops;
	}

	public void setNdGeolocationprops(List<NdGeolocationprop> ndGeolocationprops) {
		this.ndGeolocationprops = ndGeolocationprops;
	}

	public NdGeolocationprop addNdGeolocationprop(NdGeolocationprop ndGeolocationprop) {
		getNdGeolocationprops().add(ndGeolocationprop);
		ndGeolocationprop.setNdGeolocation(this);

		return ndGeolocationprop;
	}

	public NdGeolocationprop removeNdGeolocationprop(NdGeolocationprop ndGeolocationprop) {
		getNdGeolocationprops().remove(ndGeolocationprop);
		ndGeolocationprop.setNdGeolocation(null);

		return ndGeolocationprop;
	}

}