package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;


/**
 * The persistent class for the nd_experimentprop database table.
 * 
 */
@Entity
@Table(name="nd_experimentprop")
@NamedQuery(name="NdExperimentprop.findAll", query="SELECT n FROM NdExperimentprop n")
public class NdExperimentprop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GenericGenerator(name="increment", strategy = "increment") 
    @GeneratedValue(generator="increment")
	@Column(name="nd_experimentprop_id")
	private int ndExperimentpropId;

	private int rank;

	private String value;

	//bi-directional many-to-one association to NdExperiment
	@ManyToOne
	@JoinColumn(name="nd_experiment_id")
	private NdExperiment ndExperiment;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm cvterm;

	public NdExperimentprop() {
	}
	
	

	public NdExperimentprop(NdExperiment ndExperiment, Cvterm cvterm, int rank, String value) {
        super();
        this.rank = rank;
        this.value = value;
        this.ndExperiment = ndExperiment;
        this.cvterm = cvterm;
    }



    public int getNdExperimentpropId() {
		return this.ndExperimentpropId;
	}

	public void setNdExperimentpropId(int ndExperimentpropId) {
		this.ndExperimentpropId = ndExperimentpropId;
	}

	public int getRank() {
		return this.rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public NdExperiment getNdExperiment() {
		return this.ndExperiment;
	}

	public void setNdExperiment(NdExperiment ndExperiment) {
		this.ndExperiment = ndExperiment;
	}

	public Cvterm getCvterm() {
		return this.cvterm;
	}

	public void setCvterm(Cvterm cvterm) {
		this.cvterm = cvterm;
	}

}