package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;


/**
 * The persistent class for the listnms database table.
 * 
 */
@Entity
@Table(name="listnms")
@NamedQuery(name="Listnm.findAll", query="SELECT l FROM Listnm l")
@Cache(region="listnms", usage=CacheConcurrencyStrategy.READ_ONLY)
public class Listnm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name="increment", strategy = "increment")
	@GeneratedValue(generator="increment")
	private Integer listid;

	private Integer edate;

	private Integer lhierarchy;

	private Integer listdate;

	private String listdesc;

	private Integer listlocn;

	private String listname;

	private Integer listref;

	private Integer liststatus;

	private String listtype;

	private Integer listuid;

	@Lob
	private String notes;

	private Integer projectid;

	private Integer sdate;

	@Column(name="program_uuid")
	private String programUuid;

	public Listnm() {
	}

	public Integer getListid() {
		return this.listid;
	}

	public void setListid(Integer listid) {
		this.listid = listid;
	}

	public Integer getEdate() {
		return this.edate;
	}

	public void setEdate(Integer edate) {
		this.edate = edate;
	}

	public Integer getLhierarchy() {
		return this.lhierarchy;
	}

	public void setLhierarchy(Integer lhierarchy) {
		this.lhierarchy = lhierarchy;
	}

	public Integer getListdate() {
		return this.listdate;
	}

	public void setListdate(Integer listdate) {
		this.listdate = listdate;
	}

	public String getListdesc() {
		return this.listdesc;
	}

	public void setListdesc(String listdesc) {
		this.listdesc = listdesc;
	}

	public Integer getListlocn() {
		return this.listlocn;
	}

	public void setListlocn(Integer listlocn) {
		this.listlocn = listlocn;
	}

	public String getListname() {
		return this.listname;
	}

	public void setListname(String listname) {
		this.listname = listname;
	}

	public Integer getListref() {
		return this.listref;
	}

	public void setListref(Integer listref) {
		this.listref = listref;
	}

	public Integer getListstatus() {
		return this.liststatus;
	}

	public void setListstatus(Integer liststatus) {
		this.liststatus = liststatus;
	}

	public String getListtype() {
		return this.listtype;
	}

	public void setListtype(String listtype) {
		this.listtype = listtype;
	}

	public Integer getListuid() {
		return this.listuid;
	}

	public void setListuid(Integer listuid) {
		this.listuid = listuid;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Integer getProjectid() {
		return this.projectid;
	}

	public void setProjectid(Integer projectid) {
		this.projectid = projectid;
	}

	public Integer getSdate() {
		return this.sdate;
	}

	public void setSdate(Integer sdate) {
		this.sdate = sdate;
	}

	public String getProgramUuid() {
		return programUuid;
	}

	public void setProgramUuid(String programUuid) {
		this.programUuid = programUuid;
	}

}