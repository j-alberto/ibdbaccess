package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the nd_geolocationprop database table.
 * 
 */
@Entity
@Table(name="nd_geolocationprop")
@NamedQuery(name="NdGeolocationprop.findAll", query="SELECT n FROM NdGeolocationprop n")
public class NdGeolocationprop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue
	@Column(name="nd_geolocationprop_id")
	private int ndGeolocationpropId;

	private int rank;

	private String value;

	//bi-directional many-to-one association to NdGeolocation
	@ManyToOne
	@JoinColumn(name="nd_geolocation_id")
	private NdGeolocation ndGeolocation;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm type;

	public NdGeolocationprop() {
	}

	public NdGeolocationprop(NdGeolocation ndGeolocation, Cvterm type,
	        int rank, String value) {
	    this.ndGeolocation = ndGeolocation;
	    this.type = type;
	    this.rank = rank;
	    this.value = value;
    }

	public int getNdGeolocationpropId() {
		return this.ndGeolocationpropId;
	}

	public void setNdGeolocationpropId(int ndGeolocationpropId) {
		this.ndGeolocationpropId = ndGeolocationpropId;
	}

	public int getRank() {
		return this.rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public NdGeolocation getNdGeolocation() {
		return this.ndGeolocation;
	}

	public void setNdGeolocation(NdGeolocation ndGeolocation) {
		this.ndGeolocation = ndGeolocation;
	}

	public Cvterm getType() {
		return this.type;
	}

	public void setType(Cvterm type) {
		this.type = type;
	}
	
	@Override
	public String toString(){
	    return new StringBuilder().append("NdGeolocationprop ")
	            .append(ndGeolocationpropId)
	            .append(":{geoLoc: ")
	            .append(ndGeolocation.getNdGeolocationId())
	            .append(", type: ")
	            .append(type.getCvtermId())
	            .append(", rank: ")
	            .append(rank)
	            .append(", value: ")
	            .append(value)
	            .append("}")
	            .toString();
	}
}