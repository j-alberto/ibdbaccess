package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the workbench_project_user_info database table.
 * 
 */
@Entity
@Table(name="workbench.workbench_project_user_info")
@NamedQuery(name="WorkbenchProjectUserInfo.findAll", query="SELECT w FROM WorkbenchProjectUserInfo w")
public class WorkbenchProjectUserInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="user_info_id")
	private int userInfoId;

	@Column(name="last_open_date")
	private Timestamp lastOpenDate;

	//bi-directional many-to-one association to WorkbenchProject
	@ManyToOne
	@JoinColumn(name="project_id")
	private WorkbenchProject workbenchProject;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="user_id")
	private WorkbenchUser user;

	public WorkbenchProjectUserInfo() {
	}

	public int getUserInfoId() {
		return this.userInfoId;
	}

	public void setUserInfoId(int userInfoId) {
		this.userInfoId = userInfoId;
	}

	public Timestamp getLastOpenDate() {
		return this.lastOpenDate;
	}

	public void setLastOpenDate(Timestamp lastOpenDate) {
		this.lastOpenDate = lastOpenDate;
	}

	public WorkbenchProject getWorkbenchProject() {
		return this.workbenchProject;
	}

	public void setWorkbenchProject(WorkbenchProject workbenchProject) {
		this.workbenchProject = workbenchProject;
	}

	public WorkbenchUser getUser() {
		return this.user;
	}

	public void setUser(WorkbenchUser user) {
		this.user = user;
	}

}