package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;


/**
 * The persistent class for the listdata_project database table.
 * 
 */
@Entity
@Table(name="listdata_project")
@NamedQuery(name="ListdataProject.findAll", query="SELECT l FROM ListdataProject l")
public class ListdataProject implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="listdata_project_id")
	@GenericGenerator(name="increment", strategy = "increment")
	@GeneratedValue(generator="increment")
	private int listdataProjectId;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="list_id")
	private Listnm list;

	@Column(name="check_type")
	private int checkType;

	private String designation;

	@Column(name="duplicate_notes")
	private String duplicateNotes;

	@Column(name="entry_code")
	private String entryCode;

	@Column(name="entry_id")
	private int entryId;

	@Column(name="germplasm_id")
	private int germplasmId;

	@Column(name="group_name")
	private String groupName;

	@Column(name="seed_source")
	private String seedSource;

	public ListdataProject() {
	}

	public int getCheckType() {
		return this.checkType;
	}

	public void setCheckType(int checkType) {
		this.checkType = checkType;
	}

	public String getDesignation() {
		return this.designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getDuplicateNotes() {
		return this.duplicateNotes;
	}

	public void setDuplicateNotes(String duplicateNotes) {
		this.duplicateNotes = duplicateNotes;
	}

	public String getEntryCode() {
		return this.entryCode;
	}

	public void setEntryCode(String entryCode) {
		this.entryCode = entryCode;
	}

	public int getEntryId() {
		return this.entryId;
	}

	public void setEntryId(int entryId) {
		this.entryId = entryId;
	}

	public int getGermplasmId() {
		return this.germplasmId;
	}

	public void setGermplasmId(int germplasmId) {
		this.germplasmId = germplasmId;
	}

	public String getGroupName() {
		return this.groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getSeedSource() {
		return this.seedSource;
	}

	public void setSeedSource(String seedSource) {
		this.seedSource = seedSource;
	}

	public int getListdataProjectId() {
		return listdataProjectId;
	}

	public void setListdataProjectId(int listdataProjectId) {
		this.listdataProjectId = listdataProjectId;
	}

	public Listnm getList() {
		return list;
	}

	public void setList(Listnm list) {
		this.list = list;
	}

}