package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;


/**
 * The persistent class for the projectprop database table.
 * 
 */
@Entity
@Table(name="projectprop")
@NamedQuery(name="Projectprop.findAll", query="SELECT p FROM Projectprop p")
//@Cacheable(true)
@Cache(region="projectprop", usage=CacheConcurrencyStrategy.READ_ONLY)
public class Projectprop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="projectprop_id")
	@GenericGenerator(name="increment", strategy = "increment")
	@GeneratedValue(generator="increment")
	private Integer projectpropId;

	private int rank;

	private String value;

	//bi-directional many-to-one association to Project
	@ManyToOne
	@JoinColumn(name="project_id")
	private Project project;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm type;

	public Projectprop() {
	}
	
	public Projectprop(int rank, String value, Project project, Cvterm type) {
		this.rank = rank;
		this.value = value;
		this.project = project;
		this.type = type;
	}

	public Integer getProjectpropId() {
		return this.projectpropId;
	}

	public void setProjectpropId(Integer projectpropId) {
		this.projectpropId = projectpropId;
	}

	public int getRank() {
		return this.rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Project getProject() {
		return this.project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Cvterm getType() {
		return type;
	}

	public void setType(Cvterm type) {
		this.type = type;
	}

    @Override
    public String toString(){
        return new StringBuilder().append("projectpropId ")
                .append(projectpropId)
                .append(":{project: ")
                .append(project.getProjectId())
                .append(", type: ")
                .append(type.getCvtermId())
                .append(", rank: ")
                .append(rank)
                .append(", value: ")
                .append(value)
                .append("}")
                .toString();
    }
}