package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cvterm_relationship database table.
 * 
 */
@Entity
@Table(name="cvterm_relationship")
@NamedQuery(name="CvtermRelationship.findAll", query="SELECT c FROM CvtermRelationship c")
public class CvtermRelationship implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="cvterm_relationship_id")
	private int cvtermRelationshipId;

	@Column(name="type_id")
	private int typeId;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="subject_id")
	private Cvterm subject;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="object_id")
	private Cvterm object;

	public CvtermRelationship() {
	}

	public int getCvtermRelationshipId() {
		return this.cvtermRelationshipId;
	}

	public void setCvtermRelationshipId(int cvtermRelationshipId) {
		this.cvtermRelationshipId = cvtermRelationshipId;
	}

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public Cvterm getSubject() {
		return subject;
	}

	public void setSubject(Cvterm subject) {
		this.subject = subject;
	}

	public Cvterm getObject() {
		return object;
	}

	public void setObject(Cvterm object) {
		this.object = object;
	}

}