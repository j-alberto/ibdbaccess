package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;


/**
 * The persistent class for the nd_experiment database table.
 * 
 */
@Entity
@Table(name="nd_experiment")
@NamedQuery(name="NdExperiment.findAll", query="SELECT n FROM NdExperiment n")
public class NdExperiment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name="increment", strategy = "increment")
	@GeneratedValue(generator="increment")
	@Column(name="nd_experiment_id")
	private int ndExperimentId;

	//bi-directional many-to-one association to NdGeolocation
	@ManyToOne
	@JoinColumn(name="nd_geolocation_id")
	private NdGeolocation ndGeolocation;

	//bi-directional many-to-one association to Cvterm
	@ManyToOne
	@JoinColumn(name="type_id")
	private Cvterm type;
	
	@OneToMany(mappedBy="ndExperiment", fetch=FetchType.LAZY)
	private List<NdExperimentprop> experimentprops;

	
	@Transient
	private Integer entryNum;
	@Transient
	private List<Phenotype> phenotypes = new ArrayList<>(20);
	
	public NdExperiment() {
	}

	public NdExperiment(NdGeolocation ndGeolocation, Cvterm type) {
		this.ndGeolocation = ndGeolocation;
		this.type = type;
	}

	public NdExperiment(int ndExperimentId, NdGeolocation ndGeolocation, Cvterm type) {
		this.ndGeolocation = ndGeolocation;
		this.type = type;
	}

	public int getNdExperimentId() {
		return this.ndExperimentId;
	}

	public void setNdExperimentId(int ndExperimentId) {
		this.ndExperimentId = ndExperimentId;
	}

	public NdGeolocation getNdGeolocation() {
		return this.ndGeolocation;
	}

	public void setNdGeolocation(NdGeolocation ndGeolocation) {
		this.ndGeolocation = ndGeolocation;
	}

	public Cvterm getType() {
		return this.type;
	}

	public void setType(Cvterm type) {
		this.type = type;
	}

	/**
	 * Transient property, is not set when retrieving from DB.
	 * @return entry number
	 */
	public Integer getEntryNum() {
		return entryNum;
	}

	/**
	 * Transient property, is not persisted.
	 * @param entry number
	 */
	public void setEntryNum(Integer entryNum) {
		this.entryNum = entryNum;
	}

	public List<NdExperimentprop> getExperimentprops() {
		return experimentprops;
	}

	public void setExperimentprops(List<NdExperimentprop> experimentprops) {
		this.experimentprops = experimentprops;
	}

	/**
	 * Transient property, is not set when retrieving from DB.
	 * @return phenotypes
	 */
	public List<Phenotype> getPhenotypes() {
		return phenotypes;
	}

	/**
	 * Transient property, is not persisted.
	 * @param phenotypes
	 */
	public void setPhenotypes(List<Phenotype> phenotypes) {
		this.phenotypes = phenotypes;
	}
	
	public void addPhenotype(Phenotype phenotype){
		phenotypes.add(phenotype);
	}
}