package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the workbench_ibdb_user_map database table.
 * 
 */
@Entity
@Table(name="workbench.workbench_ibdb_user_map")
@NamedQuery(name="WorkbenchIbdbUserMap.findAll", query="SELECT w FROM WorkbenchIbdbUserMap w")
public class WorkbenchIbdbUserMap implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ibdb_user_map_id")
	private int ibdbUserMapId;

	@Column(name="ibdb_user_id")
	private int ibdbUserId;

	@Column(name="project_id")
	private int projectId;

	@Column(name="workbench_user_id")
	private int workbenchUserId;

	public WorkbenchIbdbUserMap() {
	}

	public int getIbdbUserMapId() {
		return this.ibdbUserMapId;
	}

	public void setIbdbUserMapId(int ibdbUserMapId) {
		this.ibdbUserMapId = ibdbUserMapId;
	}

	public int getIbdbUserId() {
		return this.ibdbUserId;
	}

	public void setIbdbUserId(int ibdbUserId) {
		this.ibdbUserId = ibdbUserId;
	}

	public int getProjectId() {
		return this.projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public int getWorkbenchUserId() {
		return this.workbenchUserId;
	}

	public void setWorkbenchUserId(int workbenchUserId) {
		this.workbenchUserId = workbenchUserId;
	}

}