package org.cimmyt.ibdbAccess.core.repository;

import java.util.List;
import java.util.Set;

import org.cimmyt.ibdbAccess.core.domain.CvtermRelationship;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 * {@link JpaRepository} for {@link CvtermRelationship}
 * @author jarojas
 *
 */
@Repository
public interface CvTermRelationshipRepository extends JpaRepository<CvtermRelationship, Integer>{

	CvtermRelationship findBySubjectCvtermIdAndTypeId(int termId, int typeId);
	/**
	 * Finds a list of {@link CvtermRelationship} of a particular type for a group of subject terms, 
	 * independently of the object terms they relate to.
	 * @param termId The set of subject ids 
	 * @param typeId the relationship type they must have defined in {@link CvtermRelationship} table
	 * @return
	 */
	List<CvtermRelationship> findBySubjectCvtermIdInAndTypeId(Set<Integer> termId, int typeId);
}
