package org.cimmyt.ibdbAccess.core.repository;

import org.cimmyt.ibdbAccess.core.domain.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * {@link JpaRepository} for {@link Stock}
 * @author jarojas
 *
 */
@Repository
public interface StockRepository extends JpaRepository<Stock, Integer>{

}
