package org.cimmyt.ibdbAccess.core.repository;

import org.cimmyt.ibdbAccess.core.domain.WorkbenchProjectUserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * {@link JpaRepository} for {@link WorkbenchProjectUserRole}
 * @author jarojas
 *
 */
@Repository
public interface WorkbenchProjectUserRoleRepository extends JpaRepository<WorkbenchProjectUserRole, Integer>{
	
}
