package org.cimmyt.ibdbAccess.core.repository;

import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
/**
 * Implementation of BatchRepository. It prevents memory leaks by periodically flushing the objects in the {@link EntityManager} and clearing the context.
 * @author jarojas
 *
 */
@Component
public class BatchRepositoryImpl implements BatchRepository {

    @PersistenceContext
    private EntityManager entityManager;
    @Value("${spring.jpa.properties.hibernate.jdbc.batch_size}")
    private int batchSize;

    @Override
    public <T> List<T> bulkSave(List<T> entities){
        if(entities == null)
            return null;

        int i = 0;
        for (T t : entities) {
          entityManager.persist(t);
          if (++i % batchSize == 0) {
            entityManager.flush();
            entityManager.clear();
          }
        }
        entityManager.flush();
        entityManager.clear();
        return entities;
    }
    
    @Override
    public <T> Set<T> bulkSave(Set<T> entities){
        if(entities == null)
            return null;
        
        int i = 0;
        for (T t : entities) {
          entityManager.persist(t);
          if (++i % batchSize == 0) {
            entityManager.flush();
            entityManager.clear();
          }
        }
        entityManager.flush();
        entityManager.clear();
        return entities;
    }

}
