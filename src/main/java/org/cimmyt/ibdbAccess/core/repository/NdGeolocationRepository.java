package org.cimmyt.ibdbAccess.core.repository;

import org.cimmyt.ibdbAccess.core.domain.NdGeolocation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * {@link JpaRepository} for {@link NdGeolocation}
 * @author jarojas
 *
 */
@Repository
public interface NdGeolocationRepository extends JpaRepository<NdGeolocation, Integer>{

}
