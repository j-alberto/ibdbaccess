package org.cimmyt.ibdbAccess.core.repository;

import java.util.Collection;
import java.util.List;

import org.cimmyt.ibdbAccess.core.domain.Projectprop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * {@link JpaRepository} for {@link Projectprop}
 * @author jarojas
 *
 */
@Repository
public interface ProjectPropRepository extends JpaRepository<Projectprop, Integer>{

	@Query("select max(p.rank) from Projectprop p where project.projectId = ?1")
	Integer maxRankByProject(int projectId);
	
	List<Projectprop> findByProjectProjectIdAndTypeCvtermIdNotIn(Integer projectId, Collection<Integer> termIds);
}
