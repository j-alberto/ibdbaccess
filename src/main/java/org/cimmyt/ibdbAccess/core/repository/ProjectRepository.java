package org.cimmyt.ibdbAccess.core.repository;

import org.cimmyt.ibdbAccess.core.domain.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * {@link JpaRepository} for {@link Project}
 * @author jarojas
 *
 */
@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer>{
	
	Project findByNameAndProgramUuid(String name, String programUuid);
	
}
