package org.cimmyt.ibdbAccess.core.repository;

import org.cimmyt.ibdbAccess.core.domain.WorkbenchProject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * {@link JpaRepository} for {@link WorkbenchProject}
 * @author jarojas
 *
 */
@Repository
public interface WorkbenchProjectRepository extends JpaRepository<WorkbenchProject, Integer>{

	WorkbenchProject findByProjectName(String name);
}
