package org.cimmyt.ibdbAccess.core.repository;

import org.cimmyt.ibdbAccess.core.domain.WorkbenchUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * {@link JpaRepository} for {@link WorkbenchUser}
 * @author jarojas
 *
 */
@Repository
public interface WorkbenchUserRepository extends JpaRepository<WorkbenchUser, Integer>{

	WorkbenchUser findByUname(String name);
}
