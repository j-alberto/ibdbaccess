package org.cimmyt.ibdbAccess.core.repository;

import org.cimmyt.ibdbAccess.core.domain.WorkbenchProjectUserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * {@link JpaRepository} for {@link WorkbenchProjectUserInfo}
 * @author jarojas
 *
 */
@Repository
public interface WorkbenchProjectUserInfoRepository extends JpaRepository<WorkbenchProjectUserInfo, Integer>{
	
}
