package org.cimmyt.ibdbAccess.core.repository;

import java.util.List;

import org.cimmyt.ibdbAccess.core.domain.ListdataProject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 * {@link JpaRepository} from {@link ListdataProject}
 * @author jarojas
 *
 */
@Repository
public interface ListDataProjectRepository extends JpaRepository<ListdataProject, Integer>{
	/**
	 * Finds all elements for a particular study list
	 * @param listId id of the study list
	 * @return a list with all elements associated to given study list, or null if not found
	 */
	List<ListdataProject> findByListListid(int listId);
}
