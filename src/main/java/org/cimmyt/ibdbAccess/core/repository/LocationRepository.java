package org.cimmyt.ibdbAccess.core.repository;

import org.cimmyt.ibdbAccess.core.domain.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * {@link JpaRepository} for {@link Location}
 * @author jarojas
 *
 */
@Repository
public interface LocationRepository extends JpaRepository<Location, Integer>{

	Location findByLabbrAndCntryid(String abbreviation, int countryId);
}
