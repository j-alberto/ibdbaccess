package org.cimmyt.ibdbAccess.core.repository;

import org.cimmyt.ibdbAccess.core.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * {@link JpaRepository} for {@link User}
 * @author jarojas
 *
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
	User findByUname(String name);

}
