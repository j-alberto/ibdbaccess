package org.cimmyt.ibdbAccess.core.repository;

import org.cimmyt.ibdbAccess.core.domain.NdExperimentprop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * {@link JpaRepository} for {@link NdExperimentprop}
 * @author jarojas
 *
 */
@Repository
public interface NdExperimentPropRepository extends JpaRepository<NdExperimentprop, Integer>{

}
