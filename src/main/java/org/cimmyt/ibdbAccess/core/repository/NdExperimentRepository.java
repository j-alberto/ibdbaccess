package org.cimmyt.ibdbAccess.core.repository;

import java.util.List;

import org.cimmyt.ibdbAccess.core.domain.Cvterm;
import org.cimmyt.ibdbAccess.core.domain.NdExperiment;
import org.cimmyt.ibdbAccess.core.domain.NdGeolocation;
import org.cimmyt.ibdbAccess.front.domain.term.ProjectPropDescriptorTerm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * {@link JpaRepository} for {@link NdExperiment}
 * @author jarojas
 *
 */
@Repository
public interface NdExperimentRepository extends JpaRepository<NdExperiment, Integer>{

	/**
	 * Finds all {@link NdExperiment} instances associated with a particular {@link NdGeolocation} and of one type.
	 * @param geolocation to look for all of its experiments
	 * @param type type of experiments desired. Types accepted are three and are defined in {@link ProjectPropDescriptorTerm}:
	 * <div><strong>STUDY_INFO</strong> for associations to main project entities</div>
	 * <div><strong>TRIAL_ENV_INFO</strong> for associations to geolocation definitions</div>
	 * <div><strong>TRIAL_PLOT_INFO</strong> for associations to geolocation plot data(designs)</div>
	 * @return ndExperiments matching given parameters, if found.
	 * <br><strong>NOTE</strong>: If a type other than one of the three defined above is passed, it will always return <code>null</code>
	 */
	List<NdExperiment> findByNdGeolocationAndType(NdGeolocation geolocation, Cvterm type);
}
