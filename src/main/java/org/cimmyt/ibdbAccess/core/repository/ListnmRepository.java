package org.cimmyt.ibdbAccess.core.repository;

import org.cimmyt.ibdbAccess.core.domain.Listnm;
import org.cimmyt.ibdbAccess.core.domain.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * {@link JpaRepository} for {@link Listnm}
 * @author jarojas
 *
 */
@Repository
public interface ListnmRepository extends JpaRepository<Listnm, Integer>{

	/**
	 * Finds the list associated to a {@link Project}
	 * @param projectId id of the 
	 * @return
	 */
	Listnm findByProjectid(Integer projectId);
	
	/**
	 * 
	 */
	Listnm findByListnameAndListtypeAndProgramUuid(String name, String type, String programUuid);
}
