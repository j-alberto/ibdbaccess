package org.cimmyt.ibdbAccess.core.repository;

import java.util.List;

import org.cimmyt.ibdbAccess.core.domain.WorkbenchIbdbUserMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * {@link JpaRepository} for {@link WorkbenchIbdbUserMap}
 * @author jarojas
 *
 */
@Repository
public interface WorkbenchIbdbUserMapRepository extends JpaRepository<WorkbenchIbdbUserMap, Integer>{

	List<WorkbenchIbdbUserMap> findByWorkbenchUserId(int workbenchUserId);
}
