package org.cimmyt.ibdbAccess.core.repository;

import org.cimmyt.ibdbAccess.core.domain.Phenotype;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * {@link JpaRepository} for {@link Phenotype}
 * @author jarojas
 *
 */
@Repository
public interface PhenotypeRepository extends JpaRepository<Phenotype, Integer>{

}
