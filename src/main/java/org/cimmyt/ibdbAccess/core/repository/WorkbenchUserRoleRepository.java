package org.cimmyt.ibdbAccess.core.repository;

import org.cimmyt.ibdbAccess.core.domain.WorkbenchUserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * {@link JpaRepository} for {@link WorkbenchUserRole}
 * @author jarojas
 *
 */
@Repository
public interface WorkbenchUserRoleRepository extends JpaRepository<WorkbenchUserRole, Integer>{

}
