package org.cimmyt.ibdbAccess.core.repository;

import org.cimmyt.ibdbAccess.core.domain.Stockprop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * {@link JpaRepository} for {@link Stockprop}
 * @author jarojas
 *
 */
@Repository
public interface StockPropRepository extends JpaRepository<Stockprop, Integer>{

}
