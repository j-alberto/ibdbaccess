package org.cimmyt.ibdbAccess.core.repository;

import java.util.List;

import org.cimmyt.ibdbAccess.core.domain.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * {@link JpaRepository} for {@link Country}
 * @author jarojas
 *
 */
@Repository
public interface CountryRepository extends JpaRepository<Country, Integer>{

	List<Country> findByIsothreeAndCchange(String iso3Code, int cchange);
}
