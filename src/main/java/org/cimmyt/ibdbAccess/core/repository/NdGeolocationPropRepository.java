package org.cimmyt.ibdbAccess.core.repository;

import java.util.List;

import org.cimmyt.ibdbAccess.core.domain.NdGeolocation;
import org.cimmyt.ibdbAccess.core.domain.NdGeolocationprop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * {@link JpaRepository} for {@link NdGeolocationprop}
 * @author jarojas
 *
 */
@Repository
public interface NdGeolocationPropRepository extends JpaRepository<NdGeolocationprop, Integer>{

	@Query("select max(g.rank) from NdGeolocationprop g where ndGeolocation.ndGeolocationId = ?1")
	Integer maxRankByGeolocation(int ndGeolocationId);
	
	List<NdGeolocationprop> findByNdGeolocationIn(List<NdGeolocation> geolocations);
}
