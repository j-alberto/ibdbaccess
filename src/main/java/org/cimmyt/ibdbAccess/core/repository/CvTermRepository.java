package org.cimmyt.ibdbAccess.core.repository;

import org.cimmyt.ibdbAccess.core.domain.Cvterm;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 * {@link JpaRepository} for {@link Cvterm}
 * @author jarojas
 *
 */
@Repository
public interface CvTermRepository extends JpaRepository<Cvterm, Integer>{

}
