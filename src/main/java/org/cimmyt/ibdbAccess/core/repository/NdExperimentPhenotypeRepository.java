package org.cimmyt.ibdbAccess.core.repository;

import java.util.List;

import org.cimmyt.ibdbAccess.core.domain.NdExperiment;
import org.cimmyt.ibdbAccess.core.domain.NdExperimentPhenotype;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * {@link JpaRepository} for {@link NdExperimentPhenotype}
 * @author jarojas
 *
 */
@Repository
public interface NdExperimentPhenotypeRepository extends JpaRepository<NdExperimentPhenotype, Integer>{

	List<NdExperimentPhenotype> findByNdExperimentIn(List<NdExperiment> experiments);
}
