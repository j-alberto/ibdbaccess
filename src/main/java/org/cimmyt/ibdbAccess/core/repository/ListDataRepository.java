package org.cimmyt.ibdbAccess.core.repository;

import org.cimmyt.ibdbAccess.core.domain.Listdata;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * {@link JpaRepository} for {@link Listdata}
 * @author jarojas
 *
 */
@Repository
public interface ListDataRepository extends JpaRepository<Listdata, Integer>{

}
