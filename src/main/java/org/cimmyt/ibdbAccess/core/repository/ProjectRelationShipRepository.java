package org.cimmyt.ibdbAccess.core.repository;

import java.util.List;

import org.cimmyt.ibdbAccess.core.domain.Cvterm;
import org.cimmyt.ibdbAccess.core.domain.Project;
import org.cimmyt.ibdbAccess.core.domain.ProjectRelationship;
import org.cimmyt.ibdbAccess.front.domain.term.RelationshipTerm;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * {@link JpaRepository} for {@link ProjectRelationship}
 * @author jarojas
 *
 */
@Repository
public interface ProjectRelationShipRepository extends JpaRepository<ProjectRelationship, Integer>{
	
	/**
	 * 
	 * @param relationshipType a {@link Cvterm} provided by {@link RelationshipTerm} class
	 * @return 
	 */
	ProjectRelationship findBySubjectAndRelationshipType(Project subject, Cvterm relationshipType);

	Page<ProjectRelationship> findBySubjectNameLikeAndRelationshipType(Pageable pageRequest, String name, Cvterm relationshipType);

	List<ProjectRelationship> findByObjectAndRelationshipType(Project object, Cvterm relationshipType);
}
