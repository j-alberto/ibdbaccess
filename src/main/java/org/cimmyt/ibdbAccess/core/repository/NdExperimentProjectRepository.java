package org.cimmyt.ibdbAccess.core.repository;

import org.cimmyt.ibdbAccess.core.domain.NdExperimentProject;
import org.cimmyt.ibdbAccess.core.domain.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * {@link JpaRepository} for {@link NdExperimentProject}
 * @author jarojas
 *
 */
@Repository
public interface NdExperimentProjectRepository extends JpaRepository<NdExperimentProject, Integer>{

	Page<NdExperimentProject> findByProject(Pageable pageRequest, Project project);
}
