package org.cimmyt.ibdbAccess.core.repository;

import java.util.List;
import java.util.Set;

/**
 * Convenience class for persisting large amounts of entities of the same type. 
 * @author jarojas
 *
 */
public interface BatchRepository {

    /**
     * Inserts new objects to the persistence unit
     * @param entities a List of objects to add.
     * @return the same list, with generated ids properly assigned to entities.
     */
    <T> List<T> bulkSave(List<T> entities);

    /**
     * Inserts new objects to the persistence unit
     * @param entities a Set of objects to add.
     * @return the same set, with generated ids properly assigned to entities.
     */
    <T> Set<T> bulkSave(Set<T> entities);

}
