package org.cimmyt.ibdbAccess.core.repository;

import org.cimmyt.ibdbAccess.core.domain.NdExperimentStock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * {@link JpaRepository} for {@link NdExperimentStock}
 * @author jarojas
 *
 */
@Repository
public interface NdExperimentStockRepository extends JpaRepository<NdExperimentStock, Integer>{

}
