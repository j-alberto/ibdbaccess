package org.cimmyt.ibdbAccess.front.domain;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Entry in an {@link Occurrence}, defining its place within a design, and possibly measurements for that entry.
 * @author jarojas
 *
 */
public class StudyEntry{

	private Integer plot;
	private Integer rep;
	private Integer subBlock;
	private Integer row;
	private Integer col;
	private Integer entryNum;
	private Map<Integer, String> traitValues = new HashMap<Integer, String>();
	
	@JsonIgnore
	private boolean hasRow;
	@JsonIgnore
	private boolean hasColumn;
	@JsonIgnore
	private boolean hasSubblock;
	
	public Integer getPlot() {
		return plot == null ? 1 : plot;
	}
	public void setPlot(Integer plot) {
		this.plot = plot;
	}
	public Integer getRep() {
		return rep == null ? 1 : rep;
	}
	public void setRep(Integer rep) {
		this.rep = rep;
	}
	public Integer getSubBlock() {
		return subBlock;
	}
	public void setSubBlock(Integer subBlock) {
		this.subBlock = subBlock;
		this.hasSubblock = true;

	}
	public Map<Integer, String> getTraitValues() {
		return traitValues;
	}
	public void setTraitValues(Map<Integer, String> traitValues) {
		this.traitValues = traitValues;
	}
	public void addTraitValue(Integer traitId, String value){
		this.traitValues.put(traitId, value);
	}
	public Integer getRow() {
		return row;
	}
	public void setRow(Integer row) {
		this.row = row;
		this.hasRow = true;
	}
	public Integer getCol() {
		return col;
	}
	public void setCol(Integer col) {
		this.col = col;
		this.hasColumn = true;
	}
	public Integer getEntryNum() {
		return entryNum;
	}
	public void setEntryNum(Integer entryNum) {
		this.entryNum = entryNum;
	}
	public boolean hasRow() {
		return hasRow;
	}
	public boolean hasColumn() {
		return hasColumn;
	}
	public boolean hasSubblock() {
		return hasSubblock;
	}
}
