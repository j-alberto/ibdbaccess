package org.cimmyt.ibdbAccess.front.domain;

import org.cimmyt.ibdbAccess.core.domain.Listnm;

/**
 * Types for {@link Listnm}
 * @author jarojas
 *
 */
public enum ListType {
		LST, F1, NURSERY, TRIAL, FOLDER;
}
