package org.cimmyt.ibdbAccess.front.domain;
/**
 * Simple bean to return id-value pairs of studyProperties
 * @author jarojas
 *
 */
public class StudyProperty {

	private int id;
	private String name;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	} 
}
