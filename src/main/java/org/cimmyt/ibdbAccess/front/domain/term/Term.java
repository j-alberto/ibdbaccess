package org.cimmyt.ibdbAccess.front.domain.term;

import org.cimmyt.ibdbAccess.core.domain.Cvterm;
/**
 * Interface that wraps a {@link Cvterm} and defines properties for studies, abstracting complexity of {@link Cvterm}.
 * It's intended that builders work with {@link Term}s instead of {@link Cvterm}s, to avoid accidentally saving study properties
 * in {@link Cvterm}s that define folders, categorical values, or ontology metadata.
 * @author jarojas
 *
 */
public interface Term {
	/**
	 * Returns the {@link Cvterm} representing this Term
	 * @return
	 */
	public Cvterm asTerm();
	/**
	 * Shortcut for the identifier of {@link Cvterm}. For example, a call <code>myTerm.asTerm().getCvtermId()</code> becomes <code>myTerm.id()</code>
	 * @return
	 */
	public int id();
}
