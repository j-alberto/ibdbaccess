package org.cimmyt.ibdbAccess.front.domain;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cimmyt.ibdbAccess.front.domain.term.StudyTypeTerm;
/**
 * Convenience representation of a {@link Study} for views
 * @author jarojas
 *
 */
public class StudySimple {

	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	private String name;

	private String objective;

	private Integer startDate;
	
	private Integer endDate;
	
	private String description;
	
	private StudyTypeTerm studyType;
	
	private String folder;
	
	private String tid;
	
	private String user;
	
	private Map<Integer, String> extras;

	private List<GermplasmListEntry> entries;

	public Map<Integer, String> getExtras() {
		if(extras == null)
			extras = new HashMap<>();
		
		return extras;
	}

	public void setExtras(Map<Integer, String> extras) {
		this.extras = extras;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getObjective() {
		return objective;
	}

	public void setObjective(String objective) {
		this.objective = objective;
	}

	public Integer getStartDate() {
		return startDate;
	}

	public void setStartDate(Integer startDate) {
		this.startDate = startDate;
	}

	public Integer getEndDate() {
		return endDate;
	}

	public void setEndDate(Integer endDate) {
		this.endDate = endDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public StudyTypeTerm getStudyType() {
		return studyType;
	}

	public void setStudyType(StudyTypeTerm studyType) {
		this.studyType = studyType;
	}

	public String getFolder() {
		return folder;
	}

	public void setFolder(String folder) {
		this.folder = folder;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public List<GermplasmListEntry> getEntries() {
		return entries;
	}

	public void setEntries(List<GermplasmListEntry> entries) {
		this.entries = entries;
	}
}
