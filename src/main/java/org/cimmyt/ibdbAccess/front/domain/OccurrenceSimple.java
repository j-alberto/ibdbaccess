package org.cimmyt.ibdbAccess.front.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

/**
 * Convenience representation of a {@link Occurrence} for views
 * @author jarojas
 *
 */
public class OccurrenceSimple {
	
	@NotNull
	private Integer id;
	@NotNull
	private Integer number;
	private List<StudyEntry> entries = new ArrayList<>();
	private String design;
	private String name;
	private String abbreviation;
	private String newCycle;
	private String numRep;
	private String locationName;
	private String locationAbbreviation;
	private String cycle;
	private Map<Integer,String> extraMetadata;

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public List<StudyEntry> getEntries() {
		return entries;
	}

	public void setEntries(List<StudyEntry> entries) {
		this.entries = entries;
	}

    public String getDesign() {
        return design;
    }

    public void setDesign(String design) {
        this.design = design;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getNumRep() {
        return numRep;
    }

    public void setNumRep(String numRep) {
        this.numRep = numRep;
    }
    
    public Map<Integer, String> getExtraMetadata() {
        if(extraMetadata == null)
            extraMetadata = new HashMap<Integer, String>(20);
        return extraMetadata;
    }
    public void setExtraMetadata(Map<Integer, String> extraMetadata) {
        this.extraMetadata = extraMetadata;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNewCycle() {
		return newCycle;
	}

	public void setNewCycle(String newCycle) {
		this.newCycle = newCycle;
	}

	public String getLocationAbbreviation() {
		return locationAbbreviation;
	}

	public void setLocationAbbreviation(String locationAbbreviation) {
		this.locationAbbreviation = locationAbbreviation;
	}

	public String getCycle() {
		return cycle;
	}

	public void setCycle(String cycle) {
		this.cycle = cycle;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}
