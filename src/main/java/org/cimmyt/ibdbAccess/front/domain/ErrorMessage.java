package org.cimmyt.ibdbAccess.front.domain;

/**
 * Custom response for validation errors
 * @author jarojas
 */
public class ErrorMessage {

	private String field;
	private String value;
	private String message;
	
	public ErrorMessage(String field, Object value, String message) {
		super();
		this.field = field;
		this.value = value == null ? "null" : value.toString();
		this.message = message;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
	public String toString(){
		return "{field: "+field+", value: "+value+", message: "+message+"}";
		
	}
}
