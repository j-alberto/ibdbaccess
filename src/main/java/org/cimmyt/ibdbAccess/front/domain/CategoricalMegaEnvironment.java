package org.cimmyt.ibdbAccess.front.domain;
/**
 * Holds id values for mega environments
 * @author jarojas
 *
 */
public final class CategoricalMegaEnvironment extends AbstractCategorical {

	public CategoricalMegaEnvironment(String termId) {
		super(termId);
	}

}