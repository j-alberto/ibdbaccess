package org.cimmyt.ibdbAccess.front.domain;

import org.cimmyt.ibdbAccess.core.domain.Cvterm;

/**
 * Represents the value of a categorical property. Categorical properties have other {@link Cvterm} as values, or
 * even ids for other entities, like location.
 * This interface provides a way to store the Id of such value-term and help conversion mechanisms
 * to work in coherent mode.
 * @author jarojas
 */
public interface Categorical {
	public String getValue();
}
