package org.cimmyt.ibdbAccess.front.exception;

/**
 * Exception thrown when a user is not found in workbench schema.
 * @author jarojas
 *
 */
public class UserNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private Integer userId;
    
    public UserNotFoundException(Integer userId) {
        this.userId = userId;
    }
    
    @Override
    public String getMessage() {
        return String.format("Couldn't find user for id: %s", userId);
    }
}
