package org.cimmyt.ibdbAccess.front.exception;

import org.cimmyt.ibdbAccess.core.domain.Cvterm;

/**
 * Exception thrown when a trait {@link Cvterm} for a given id doesn't exist in the ontology.
 * @author jarojas
 *
 */
public class MissingOntologyTermException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    int occurrenceNum;
    int entryNumber;
    int termId;
    
    public MissingOntologyTermException(int occurrenceNum, int entryNumber, int termId) {
        this.occurrenceNum = occurrenceNum;
        this.entryNumber = entryNumber;
        this.termId = termId;
    }
    
    @Override
    public String getMessage() {
        return String.format("Cannnot find term with id '%s' in ontology. Occurrence: %s, entry: %s"
        		, termId
        		,occurrenceNum
        		,entryNumber);
    }
}
