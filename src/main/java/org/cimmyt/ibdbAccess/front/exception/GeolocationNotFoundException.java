package org.cimmyt.ibdbAccess.front.exception;

/**
 * Exception thrown when a geolocation look up founds nothing, to prevent further actions.
 * @author jarojas
 *
 */
public class GeolocationNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private Integer geolocationId;
    
    public GeolocationNotFoundException(Integer geolocationId) {
        this.geolocationId = geolocationId;
    }
    
    @Override
    public String getMessage() {
        return String.format("Couldn't find geolocation with id: %s", geolocationId);
    }
}
