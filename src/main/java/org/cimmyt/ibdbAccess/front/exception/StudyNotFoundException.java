package org.cimmyt.ibdbAccess.front.exception;

/**
 * Exception thrown when a study look up founds nothing, to prevent further actions.
 * @author jarojas
 *
 */
public class StudyNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private Integer studyId;
    
    public StudyNotFoundException(Integer studyId) {
        this.studyId = studyId;
    }
    
    @Override
    public String getMessage() {
        return String.format("Couldn't find study for id: %s", studyId);
    }
}
