package org.cimmyt.ibdbAccess.front.builder;

import java.util.ArrayList;
import java.util.List;

import org.cimmyt.ibdbAccess.core.domain.Cvterm;
import org.cimmyt.ibdbAccess.core.domain.Project;
import org.cimmyt.ibdbAccess.core.domain.Projectprop;
import org.cimmyt.ibdbAccess.core.repository.ProjectPropRepository;
import org.cimmyt.ibdbAccess.front.domain.term.ProjectPropDescriptorTerm;
import org.cimmyt.ibdbAccess.front.domain.term.ProjectPropTerm;
import org.cimmyt.ibdbAccess.front.domain.term.StandardTerm;
import org.cimmyt.ibdbAccess.front.domain.term.Term;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Convenience class to build Projectprop records. This class sould be used for higher level builders and managers
 * when they need to store study attributes as {@link Projectprop} records.
 * @author jarojas
 *
 */
@Component
public class StandardTermBuiler {
	
	private int rank = 0;
	private Projectprop prop = null;
	private List<Projectprop> projProperties;
	private Project currentProject;
	private static final Logger LOG = LoggerFactory.getLogger(StandardTermBuiler.class);

	private ProjectPropRepository projectPropRepository;

	@Autowired
	public StandardTermBuiler(ProjectPropRepository projectPropRepository){
		this.projectPropRepository = projectPropRepository;
		projProperties = new ArrayList<>(150);
	}
	
	/**
     * Identfies if the value to store is the Id of a term, or a plain value.
     * @param value the Object value to check
     * @return the String representation of the value, or the Id, if the object passed is a {@link Term}
     */
    public static String stringValueOf(Object value){
        String stringValue;
        if(value == null){
            stringValue = "";
        }else if(value instanceof Term){
            stringValue = ((Term)value).asTerm().getCvtermId().toString();
        }else{
            stringValue = value.toString();
        }
        return stringValue;
    }
   
    /**
     * Inserts 3 {@link Projectprop} records, no value is assigned but these records are structural components of a study.
     * @param project the one to associate the property with
     * @param property the ProjectPropTerm instance storing the values required to build a specific property 
     * @return this builder
     */
    public StandardTermBuiler createTerm(Project project, ProjectPropTerm property){
        
        nextRank(project);
        
        prop = new Projectprop(rank,
                property.title(),
                project,
                property.asTermDescriptor());
        projProperties.add(prop);
        LOG.trace("createTerm {}", prop);
        
        addVarDescriptionAndSandardIdProperties(project, property);

        return this;
    }

	
	/**
	 * Inserts 4 {@link Projectprop} records, one storing the actual value
	 * @param project the one to associate the property with
	 * @param property the ProjectPropTerm storing the values required to build a specific property 
	 * @param value the value for the property, this can be a {@link Term} referencing the actual value or a plain String.
	 * @return this builder
	 */
	public StandardTermBuiler createTerm(Project project, ProjectPropTerm property, Object value){

	    createTerm(project, property);
	    
		prop = new Projectprop(rank,
		        stringValueOf(value),
				project,
				property.asTerm());
		projProperties.add(prop);
		LOG.trace("createTerm(+value) {}" + prop);

		return this;
	}
    
    /**
     * Convenience method that creates two common records, commonly needed when creating new Terms.
     * Those two records are a variable description and a standard variable id.
     * @param project the one to associate the property with
     * @param property the {@link StandardTerm} instance to link with these two records
     */
    private void addVarDescriptionAndSandardIdProperties(Project project, StandardTerm property){
        prop = new Projectprop(rank,
                property.caption(),
                project,
                ProjectPropDescriptorTerm.VARIABLE_DESCRIPTION.asTerm());
        projProperties.add(prop);
        LOG.trace("addVarDescriptionAndSandardIdProperties A {}" + prop);

        prop = new Projectprop(rank,
                String.valueOf(property.id()),
                project,
                ProjectPropDescriptorTerm.STANDARD_VAR_ID.asTerm());
        projProperties.add(prop);
        LOG.trace("addVarDescriptionAndSandardIdProperties B {}" + prop);
    	
    }
    
    /**
     * Inserts 3 {@link Projectprop} records that define a property of a {@link Project}
     * @param project the one to associate the property with
     * @param property the {@link StandardTerm} instance storing the values required to build a specific property 
     * @param termDescriptor describes the standard term provided
     * @return this builder
     */
    public StandardTermBuiler createTerm(Project project, StandardTerm property, Cvterm termDescriptor){
        nextRank(project);
        
        prop = new Projectprop(rank,
                property.title(),
                project,
                termDescriptor);
        projProperties.add(prop);
        LOG.trace("createTerm {}" + prop);

        addVarDescriptionAndSandardIdProperties(project, property);
        
        return this;
    }

    /**
     * Inserts 4 {@link Projectprop} records that define a property of a {@link Project} and its value
     * @param project the one to associate the property with
     * @param property the {@link StandardTerm} instance storing the values required to build a specific property 
     * @param termDescriptor describes the standard term provided
     * @param value the value for this property
     * @return this builder
     */
    public StandardTermBuiler createTerm(Project project, StandardTerm property, Cvterm termDescriptor, Object value){
        createTerm(project, property, termDescriptor);
        
        String stringValue = stringValueOf(value);        

        prop = new Projectprop(rank,
                stringValue,
                project,
                property.asTerm());
        projProperties.add(prop);
        LOG.trace("createTerm {}" + prop);
        
        return this;
    }

    /**
     * Inserts 3 {@link Projectprop} records that define a property of a {@link Project}, with the
     * default descriptor ProjectPropDescriptorTerm.TRIAL_ENV_INFO, meaning it's a property to be found in study instances.
     * @param project the one to associate the property with. It's expected to be a Project defining the environment dataset.
     * @param property the {@link StandardTerm} instance storing the values required to build a specific property 
     * @return this builder
     */
    public StandardTermBuiler createTermForOcc(Project project, StandardTerm property){
        
        nextRank(project);
        
        prop = new Projectprop(rank,
                property.title(),
                project,
                ProjectPropDescriptorTerm.ENVIRONMENT_DETAIL.asTerm());
        projProperties.add(prop);
        LOG.trace("createTermForOcc {}" + prop);
        
        addVarDescriptionAndSandardIdProperties(project, property);

        return this;
    }

	/**
	 * Output of the builder
	 * @return the List of {@link Projectprop} built so far
	 */
	public List<Projectprop> getCurrentProperties(){
		return projProperties;
	}
	
	/**
	 * Sets the rank to the next valid value for a project property, or 1 if there are no properties for the given project yet.
	 * This method must be called every time before creating a term.
	 * @param newProject the project which will have a new property attached
	 */
	private void nextRank(Project newProject){
		if(currentProject == null || !currentProject.equals(newProject)){
			Integer tmp = projectPropRepository.maxRankByProject(newProject.getProjectId());
			rank = tmp == null? 1 : tmp + 1;
			currentProject = newProject;
		}else{
			rank++;
		}
		
	}
	
	/**
	 * Resets this builder to an initial state, that is , it clears all properties built so far, and removes references to any {@link Project}
	 * in order to reset the rank counter.
	 * @return this builder
	 */
	public StandardTermBuiler clean(){
		this.projProperties.clear();
		currentProject = null;
		return this;
	}
	
}
