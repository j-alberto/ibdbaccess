package org.cimmyt.ibdbAccess.front.builder;

import java.util.ArrayList;
import java.util.List;

import org.cimmyt.ibdbAccess.core.domain.NdGeolocation;
import org.cimmyt.ibdbAccess.core.domain.NdGeolocationprop;
import org.cimmyt.ibdbAccess.core.domain.Project;
import org.cimmyt.ibdbAccess.core.domain.Projectprop;
import org.cimmyt.ibdbAccess.core.repository.NdGeolocationPropRepository;
import org.cimmyt.ibdbAccess.front.domain.term.StandardTerm;
import org.cimmyt.ibdbAccess.front.domain.term.Term;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Builds {@link NdGeolocationprop} records, which is the place where attributes for study instances are stored.
 * @author jarojas
 *
 */
@Component
public class NdGeolocationPropBuiler {
	
	private int rank = 0;
	private NdGeolocationprop prop = null;
	private List<NdGeolocationprop> geolocProperties;
	private NdGeolocation currentGeolocation;
	private static final Logger LOG = LoggerFactory.getLogger(NdGeolocationPropBuiler.class);

	private NdGeolocationPropRepository geolocationPropRepo;

	@Autowired
	public NdGeolocationPropBuiler(NdGeolocationPropRepository geolocationPropRepo){
		this.geolocationPropRepo = geolocationPropRepo;
		this.geolocProperties = new ArrayList<>(50);
	}
	   

	
	/**
	 * Inserts 4 {@link Projectprop} records, one storing the actual value
	 * @param project the one to associate the property with
	 * @param property the ProjectPropTerm storing the values required to build a specific property 
	 * @param value the value for the property, this can be a {@link Term} referencing the actual value or a plain String.
	 * @return this builder
	 */
	public NdGeolocationPropBuiler createGeoProperty(NdGeolocation geolocation, StandardTerm property, Object value){
	    String stringVal = StandardTermBuiler.stringValueOf(value);
	    nextRank(geolocation);
	    
		prop = new NdGeolocationprop(geolocation,
		        property.asTerm(),
				rank,
				stringVal);
		geolocProperties.add(prop);
        LOG.trace("createGeoProperty: {}",prop);

		return this;
	}
	

	/**
	 * Returns the project properties built by this instance
	 * @return the List of Projectprop records built so far
	 */
	public List<NdGeolocationprop> getCurrentProperties(){
		return geolocProperties;
	}
	
	/**
	 * Sets the rank to the next valid value for a project property, or 1 if there are no properties for the given project yet.
	 * This method must be called before creating a set of {@link Projectprop} defining a project property.
	 * @param newProject
	 */
	private void nextRank(NdGeolocation newGeolocation){
		if(currentGeolocation == null || !currentGeolocation.equals(newGeolocation)){
			Integer tmp = geolocationPropRepo.maxRankByGeolocation(newGeolocation.getNdGeolocationId());
			rank = tmp == null? 1 : tmp + 1;
			currentGeolocation = newGeolocation;
		}else{
			rank++;
		}
		
	}
	
    /**
     * Resets this builder to an initial state, that is , it clears all properties built so far, and removes references to any {@link Project}
     * in order to reset the rank counter.
     * @return this builder
     */
	public NdGeolocationPropBuiler clean(){
		this.geolocProperties.clear();
		currentGeolocation = null;
		return this;
	}

}
