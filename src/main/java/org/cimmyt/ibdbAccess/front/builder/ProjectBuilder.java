package org.cimmyt.ibdbAccess.front.builder;

import java.util.ArrayList;
import java.util.List;

import org.cimmyt.ibdbAccess.core.domain.CvtermRelationship;
import org.cimmyt.ibdbAccess.core.domain.Project;
import org.cimmyt.ibdbAccess.core.domain.ProjectRelationship;
import org.cimmyt.ibdbAccess.core.domain.Projectprop;
import org.cimmyt.ibdbAccess.core.domain.StudyId;
import org.cimmyt.ibdbAccess.core.repository.CvTermRelationshipRepository;
import org.cimmyt.ibdbAccess.front.domain.Occurrence;
import org.cimmyt.ibdbAccess.front.domain.Study;
import org.cimmyt.ibdbAccess.front.domain.StudyEntry;
import org.cimmyt.ibdbAccess.front.domain.term.BasicStandardTerm;
import org.cimmyt.ibdbAccess.front.domain.term.ProjectPropDescriptorTerm;
import org.cimmyt.ibdbAccess.front.domain.term.ProjectPropTerm;
import org.cimmyt.ibdbAccess.front.domain.term.RelationshipTerm;
import org.cimmyt.ibdbAccess.front.domain.term.StudyTypeTerm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
/**
 * High level builder used to create all the instances needed to define the skeleton of a Study. Note that this class only creates
 * the structure, but don't persist such structure.
 * @author jarojas
 *
 */
@Component
public class ProjectBuilder {

	private Project mainProject;
	private Project enviroProject;
	private Project plotDataProject;
	private ProjectRelationship inFolder;
	private ProjectRelationship datasetInA;
	private ProjectRelationship datasetInB;
	private List<Project> projects;
	private List<ProjectRelationship> projectRelationships;
	private List<Projectprop> projectProperties;
	private CvTermRelationshipRepository cvTermRelationRepo;
	private static final Logger LOG = LoggerFactory.getLogger(ProjectBuilder.class);
	
	private StandardTermBuiler builder;
	
	@Autowired
	public ProjectBuilder(StandardTermBuiler builder, CvTermRelationshipRepository cvTermRelationRepo){
		this.builder = builder;
		this.cvTermRelationRepo = cvTermRelationRepo;
	}
	
	/**
	 * Creates the basic structure for a Study
	 * @param study the {@link Study} to build
	 * @param folder the representation of a folder where the study will be stored
	 * @return a {@link StudyId} instance referencing the Study created.
	 */
	public StudyId buildProject(final Study study, final Project folder){
		
		buildProjectEntities(study.getName().trim(), study.getDescription().trim(), folder, study.getTid());
		buildProjectPropertyEntities(study);
		buildProjectRelationshipEntities(folder);
		return new StudyId(mainProject,	enviroProject, plotDataProject); 
	}
	
	/**
	 * Creates 3 {@link Project} entities needed for a study; one defines the whole study,
	 * other the environment dataset, and other the plot dataset. 
	 * @param maxProjIdx the last Id of {@link Project} entity
	 * @param name name of the study
	 * @param description a description of the study
	 * @param folder the representation of a folder where the study will be stored
	 * @param tid the Id in IWIS referencing the Study being built
	 */
	private void buildProjectEntities(String name, 
			String description, Project folder, String tid){
	    String uniqueName = name+"_"+tid; 
		mainProject = new Project(uniqueName, description, folder.getProgramUuid());
		
		enviroProject = new Project(uniqueName+"-ENVIRONMENT",
			name+"-ENVIRONMENT", folder.getProgramUuid());
		
		plotDataProject = new Project(uniqueName+"-PLOTDATA", 
			name+"-PLOTDATA", folder.getProgramUuid());
		
		LOG.trace("buildProjectEntities for study {}: main: {}, environment: {}, plot: {}",
		        uniqueName, mainProject.getProjectId(), enviroProject.getProjectId(), plotDataProject.getProjectId());
		projects = new ArrayList<>(3);
		projects.add(mainProject);
		projects.add(enviroProject);
		projects.add(plotDataProject);
	}

	/**
	 * Creates {@link Projectprop} entities that define the for the 3
	 * {@link Project} records that define a Study
	 * @param study the Study being built
	 */
	private void buildProjectPropertyEntities(Study study){
		
		StudyTypeTerm studyType;
		if(study.getStudyType().equalsIgnoreCase("T")){
			studyType = StudyTypeTerm.TRIAL;
		}else{
			studyType = StudyTypeTerm.NURSERY;
		}
        study.getExtraMetadata().put(ProjectPropDescriptorTerm.STUDY_TID.asTerm().getCvtermId(), study.getTid());

        builder.clean()
			.createTerm(mainProject, ProjectPropTerm.STUDY_TYPE, studyType)
			.createTerm(mainProject, ProjectPropTerm.STUDY_NAME_ASSIGNED, study.getName())
			.createTerm(mainProject, ProjectPropTerm.STUDY_TITLE_ASSIGNED, study.getDescription())
			.createTerm(mainProject, ProjectPropTerm.STUDY_OBJECTIVE, study.getObjective())
			.createTerm(mainProject, ProjectPropTerm.START_DATE, study.getStartDate())
			.createTerm(mainProject, ProjectPropTerm.END_DATE, study.getEndDate())
			.createTerm(mainProject, ProjectPropTerm.STUDY_USER_ID, study.getUserId());

        createExtraTerms(study);
			
		builder.createTerm(enviroProject, ProjectPropTerm.DATASET_NAME_ASSIGNED)
			.createTerm(enviroProject, ProjectPropTerm.DATASET_TITLE_ASSIGNED)
			.createTerm(enviroProject, ProjectPropTerm.DATASET_TYPE_ASSIGNED, ProjectPropDescriptorTerm.ENV_CONDITIONS)
			.createTerm(enviroProject, ProjectPropTerm.TRIAL_INSTANCE_ENUMERATED)

			.createTerm(plotDataProject, ProjectPropTerm.TRIAL_INSTANCE_ENUMERATED)
			.createTerm(plotDataProject, ProjectPropTerm.DATASET_NAME_ASSIGNED)
			.createTerm(plotDataProject, ProjectPropTerm.DATASET_TITLE_ASSIGNED)
			.createTerm(plotDataProject, ProjectPropTerm.DATASET_TYPE_ASSIGNED, ProjectPropDescriptorTerm.PLOT_DATA)

			.createTerm(plotDataProject, ProjectPropTerm.ENTRY_TYPE_ASSIGNED)
			.createTerm(plotDataProject, ProjectPropTerm.ENTRY_GID_ASSIGNED)
			.createTerm(plotDataProject, ProjectPropTerm.ENTRY_DESIGNATION_ASSIGNED)
			.createTerm(plotDataProject, ProjectPropTerm.ENTRY_NUMBER_ASSIGNED)
			.createTerm(plotDataProject, ProjectPropTerm.CROSS);
		
		createTermsByStudyType(studyType);
		createTermsByDesignInfo(study.getOccurrences());
		
		LOG.trace("buildProjectPropertyEntities total: {}", builder.getCurrentProperties().size());
		projectProperties = builder.getCurrentProperties();

	}
	
	/**
	 * Creates {@link ProjectRelationship} entities responsible of getting all tied up.
	 * @param maxProjRelIdx the last Id of {@link ProjectRelationship} entity
	 * @param folder the representation of a folder where the study will be stored
	 */
	private void buildProjectRelationshipEntities(Project folder){
		inFolder = new ProjectRelationship(mainProject, folder
				, RelationshipTerm.A_STUDY_IN.asTerm());
		
		datasetInA = new ProjectRelationship(enviroProject, mainProject
				, RelationshipTerm.A_DATASET_OF.asTerm());
		
		datasetInB = new ProjectRelationship(plotDataProject, mainProject
				, RelationshipTerm.A_DATASET_OF.asTerm());
		
		projectRelationships = new ArrayList<>(3);
		projectRelationships.add(inFolder);
		projectRelationships.add(datasetInA);
		projectRelationships.add(datasetInB);
	}
	
	/**
	 * Creates optional attributes for a {@link Study}, when defined in extraMetadata attribute. 
	 * @param study Study containing extra metadata to be built as terms.
	 */
	private void createExtraTerms(Study study){
	    
        List<CvtermRelationship> projectPropRels = 
                cvTermRelationRepo.findBySubjectCvtermIdInAndTypeId(
                        study.getExtraMetadata().keySet(),
                    RelationshipTerm.HAS_SCALE.asTerm().getCvtermId());
        
        for(CvtermRelationship propertyRel : projectPropRels){
            builder.createTerm(mainProject, 
                    new BasicStandardTerm(propertyRel.getSubject()),
                    ProjectPropDescriptorTerm.STUDY_DETAIL.asTerm(),
                    study.getExtraMetadata().get(propertyRel.getSubject().getCvtermId()));
        }
	    
	}
	
	/**
	 * Created additional attributes for a study that depend on the study type, e.g. Trial or Nursery
	 * @param studyType the type of study
	 */
	private void createTermsByStudyType(StudyTypeTerm studyType){
	    if(studyType == StudyTypeTerm.TRIAL){
            builder
                .createTerm(plotDataProject, ProjectPropTerm.PLOT_NUMBER)
                .createTerm(plotDataProject, ProjectPropTerm.REP_NUMBER);
        }
	}

	/**
	 * Created additional attributes for a study that depend on the presence or absence of optional 
	 * design information in studyEntries, i.e. row, column, subblock, etc
	 * @param studyOccurences to check if its entries contain extra design info
	 */
	private void createTermsByDesignInfo(List<Occurrence> studyOccurences){
		boolean hasRowValues = false;
		boolean hasColumnsValues = false;
		boolean hasSubBlockValues = false;
		
		for (Occurrence occurrence : studyOccurences) {
			if(!hasRowValues){
				for (StudyEntry entry : occurrence.getEntries()) {
					if(entry.hasRow()){
						builder.createTerm(plotDataProject, ProjectPropTerm.ROW_NUMBER);
						hasRowValues = true;
						break;
					}
				}
			}
			if(!hasColumnsValues){
				for (StudyEntry entry : occurrence.getEntries()) {
					if(entry.hasColumn()){
						builder.createTerm(plotDataProject, ProjectPropTerm.COLUMN_NUMBER);
						hasColumnsValues = true;
						break;
					}
				}
			}
			if(!hasSubBlockValues){
				for (StudyEntry entry : occurrence.getEntries()) {
					if(entry.hasSubblock()){
						builder.createTerm(plotDataProject, ProjectPropTerm.SUB_BLOCK);
						hasSubBlockValues = true;
						break;
					}
				}
			}
			
			if (hasRowValues && hasColumnsValues && hasSubBlockValues){
				break;
			}

		}
	}

	/**
	 * Output of the builder
	 * @return the {@link Project} entities created.
	 */
	public List<Project> getProjects(){
		return projects;
	}

    /**
     * Output of the builder
     * @return the {@link ProjectRelationship} entities created.
     */
	public List<ProjectRelationship> getProjectRelationships(){
		return projectRelationships;
	}

    /**
     * Output of the builder
     * @return the {@link Projectprop} entities created.
     */
	public List<Projectprop> getProjectProperties() {
		return projectProperties;
	}

	/**
	 * Creates a {@link StudyId} with the information provided
	 * @param main project defining a study
	 * @param relationships containing plot and environment datasets. Is mandatory to have both.
	 * @return
	 */
	public StudyId createStudyIdWithProjectAndDatasets(Project main,
			List<ProjectRelationship> relationships){
		Project plotProject = null;
		Project environmentProject = null;
		StudyId foundStudyId = null;
		
		if(relationships != null && relationships.size() == 2){
			String projectName = relationships.get(0).getSubject().getName();
			if(projectName.endsWith("-ENVIRONMENT")){
				environmentProject = relationships.get(0).getSubject();
				plotProject = relationships.get(1).getSubject();
			}else{
				environmentProject = relationships.get(1).getSubject();
				plotProject = relationships.get(0).getSubject();
			}
			foundStudyId = new StudyId(main, environmentProject, plotProject); 
		}
		
		return foundStudyId;
	}

}
