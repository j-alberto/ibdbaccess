package org.cimmyt.ibdbAccess.front.builder;

import java.util.ArrayList;
import java.util.List;

import org.cimmyt.ibdbAccess.core.domain.Stock;
import org.cimmyt.ibdbAccess.core.domain.Stockprop;
import org.cimmyt.ibdbAccess.front.domain.term.ProjectPropTerm;
import org.springframework.stereotype.Component;

/**
 * Creates attributes to be saved as {@link Stockprop} entities. These are normally properties of the study design
 * but need to be defined only for each entry in the original germplasm list.
 * @author jarojas
 *
 */
@Component
public class StockPropBuilder {


	List<Stockprop> stockProperties;
	StandardTermBuiler builder;
	private int rank = 0;
	private Stock currentStock = null;
	
	/**
	 * Creates a {@link Stockprop} entity defining the type of an entry
	 * @param stock the stock representing a germplasm entry
	 * @param entryType the type of such entry
	 * @return this builder
	 */
	public StockPropBuilder buildEntryTypeProperty(Stock stock, int  entryType){
	    nextRank(stock);
		stockProperties.add(new Stockprop(
				stock,
				rank,
				ProjectPropTerm.ENTRY_TYPE_ASSIGNED.asTerm(),
				String.valueOf(entryType)));
		
		return this;
	}

    /**
     * Creates a {@link Stockprop} entity defining the type of an entry
     * @param stock the stock representing a germplasm entry
	 * @param cross the name of the cross for this entry
     * @return this builder
	 */
	public StockPropBuilder buildCrossProperty(Stock stock, String cross){

	    nextRank(stock);

		stockProperties.add(new Stockprop(
				stock,
				rank,
				ProjectPropTerm.CROSS.asTerm(),
				cross));
		
		return this;
	}

    /**
     * Resets this builder to an initial state, that is , it clears all properties built so far, and resets the rank counter.
     * @param initialCapacity the estimated amount of properties to build in the next run.
     * @return this builder
     */
	public StockPropBuilder clean(int initialCapacity){
		stockProperties = new ArrayList<>(initialCapacity * 2);
		rank = 0;
		
		return this;
	}
	
    /**
     * Sets the rank to the next valid value for a stock property, or 1 if there are no properties for the given stock yet.
     * This method must be called every time before creating a stock property.
     * @param newStock the stock which will have a new property attached
     */
	private void nextRank(Stock newStock){
		if(currentStock == null || !currentStock.equals(newStock)){
			rank = 0;
			currentStock = newStock;
		}else{
			rank++;
		}
	}
	
    /**
     * Output of the builder
     * @return the List of {@link Stockprop} built so far
     */
	public List<Stockprop> getStockProperties(){
		return stockProperties;
	}

}
