package org.cimmyt.ibdbAccess.front.manager;

import java.util.List;

import org.cimmyt.ibdbAccess.core.domain.ListdataProject;
import org.cimmyt.ibdbAccess.core.domain.Listnm;
import org.cimmyt.ibdbAccess.core.domain.StudyId;
import org.cimmyt.ibdbAccess.front.domain.Study;
/**
 * Responsible of saving simple germplasm lists as well as study germplasm lists.
 * @author jarojas
 *
 */
public interface ListManager {
	
	Listnm findList(int id);
	/**
	 * Saves a {@link GermplasmList} as a plain {@link Listnm} entity
	 * @param list germplasm list to persist as a plain list
	 * @return id of the generated list
	 */
    int saveList(Study study);
    
    /**
     * Saves a {@link GermplasmList} as a {@link Listnm} entity for an existing study (either a Nursery, a Trial, etc.)
     * @param list germplasm list to persist
     * @param studyId the Id of the Study this germplasm list represents
     * @param parentListId the parent list this study list comes from, or null if this list was not based on a previous one
     * @return id of the generated list
     */
    List<ListdataProject>  saveStudyList(Study study, StudyId studyId, Integer parentListId);
    
    /**
     * Finds all {@link ListdataProject} elements associated to a study
     * @param studyId identifier of the study
     * @return the representation of a germplasm list
     */
    List<ListdataProject> findStudyList(int studyId);

}
