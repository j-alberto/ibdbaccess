package org.cimmyt.ibdbAccess.front.manager.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.cimmyt.ibdbAccess.conversion.BeanConverter;
import org.cimmyt.ibdbAccess.core.domain.Cvterm;
import org.cimmyt.ibdbAccess.core.domain.Location;
import org.cimmyt.ibdbAccess.core.domain.NdExperiment;
import org.cimmyt.ibdbAccess.core.domain.NdExperimentProject;
import org.cimmyt.ibdbAccess.core.domain.NdExperimentprop;
import org.cimmyt.ibdbAccess.core.domain.NdGeolocation;
import org.cimmyt.ibdbAccess.core.domain.NdGeolocationprop;
import org.cimmyt.ibdbAccess.core.domain.Project;
import org.cimmyt.ibdbAccess.core.domain.Projectprop;
import org.cimmyt.ibdbAccess.core.domain.StudyId;
import org.cimmyt.ibdbAccess.core.repository.BatchRepository;
import org.cimmyt.ibdbAccess.core.repository.CvTermRepository;
import org.cimmyt.ibdbAccess.core.repository.NdExperimentProjectRepository;
import org.cimmyt.ibdbAccess.core.repository.NdExperimentPropRepository;
import org.cimmyt.ibdbAccess.core.repository.NdExperimentRepository;
import org.cimmyt.ibdbAccess.core.repository.NdGeolocationPropRepository;
import org.cimmyt.ibdbAccess.core.repository.NdGeolocationRepository;
import org.cimmyt.ibdbAccess.front.builder.NdGeolocationPropBuiler;
import org.cimmyt.ibdbAccess.front.domain.Categorical;
import org.cimmyt.ibdbAccess.front.domain.Occurrence;
import org.cimmyt.ibdbAccess.front.domain.Study;
import org.cimmyt.ibdbAccess.front.domain.StudyEntry;
import org.cimmyt.ibdbAccess.front.domain.term.BasicStandardTerm;
import org.cimmyt.ibdbAccess.front.domain.term.GeolocationPropCategoricalTerm;
import org.cimmyt.ibdbAccess.front.domain.term.LocationTerm;
import org.cimmyt.ibdbAccess.front.domain.term.ProjectPropDescriptorTerm;
import org.cimmyt.ibdbAccess.front.domain.term.ProjectPropTerm;
import org.cimmyt.ibdbAccess.front.exception.GeolocationNotFoundException;
import org.cimmyt.ibdbAccess.front.manager.NdExperimentManager;
import org.cimmyt.ibdbAccess.front.manager.ProjectManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of {@link NdExperimentManager}
 * @author jarojas
 *
 */
@Component
@Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
public class NdExperimentManagerImpl implements NdExperimentManager{

	NdGeolocationRepository ndGeolocationRepo;
	NdGeolocationPropRepository ndGeolocationPropRepo;
	ProjectManager projectManager;
	NdGeolocationPropBuiler geolocPropBuilder;
	NdExperimentRepository ndExperimentRepo;
	NdExperimentProjectRepository ndExpProjectRepo;
	NdExperimentPropRepository ndExperimentPropRepo;
	@Autowired
    private BatchRepository batchRepo;
	@Autowired
	private CvTermRepository cvTermRepo;
	@Autowired
	private BeanConverter converter;
	
	private static final Logger LOG = LoggerFactory.getLogger(NdExperimentManagerImpl.class);
	
	@Autowired
	public NdExperimentManagerImpl(NdGeolocationRepository ndGeolocationRepo,
			NdGeolocationPropRepository ndGeolocationPropRepo,
			ProjectManager projectManager, NdGeolocationPropBuiler geolocPropBuilder,
			NdExperimentRepository ndExperimentRepo, NdExperimentProjectRepository ndExpProjectRepo,
			NdExperimentPropRepository ndExperimentPropRepo) {
		this.ndGeolocationRepo = ndGeolocationRepo;
		this.ndGeolocationPropRepo = ndGeolocationPropRepo;
		this.projectManager = projectManager;
		this.geolocPropBuilder = geolocPropBuilder;
		this.ndExperimentRepo = ndExperimentRepo;
		this.ndExpProjectRepo = ndExpProjectRepo;
		this.ndExperimentPropRepo = ndExperimentPropRepo;
	}

	/**
     * Persist appropriate information in {@link NdGeolocation}, {@link NdGeolocationprop} as well as the 
     * {@link Projectprop} element defining the variables associated to each study instance.
     * @param study containing the {@link Occurrence} elements defining study instances
     * @param studyId the id numbers for the project this instances belong to.
	 */
	@Override
	@Transactional(propagation=Propagation.REQUIRED,readOnly=false)
	public Map<Integer, List<NdExperiment>> saveStudyInstances(Study study, StudyId studyId) {
		
	    saveCommonVariables(study, studyId);
	    List<NdGeolocation> geolocations = saveGeolocations(study);
	    Map<Integer, List<NdExperiment>> experimentsMap = saveInstancesAsExperiments(study, studyId ,geolocations);
		
		return experimentsMap;
	}
	
	/**
	 * creates terms that represent {@link Occurrence} metadata and links them with
	 * an environment dataset, that is, creates new {@link Projectprop} entities}.
	 * @param study study with {@link Occurrence} matadata to save
	 * @param studyId identifier of an existing {@link Study}
	 */
	private void saveCommonVariables(Study study, StudyId studyId){
	    LOG.debug("saveCommonVariables processing...");
		
		Set<Integer> allOccPropIds = new HashSet<>(15);
		for (Occurrence occ : study.getOccurrences()) {
			
			addFixedProperties(occ);
			preparePropertiesForLocation(occ);
			
            allOccPropIds.addAll(occ.getExtraMetadata().keySet());
            for(Integer termId : occ.getExtraMetadata().keySet()){
            	if(GeolocationPropCategoricalTerm.hasGeolocPropTerm(termId)){
            		GeolocationPropCategoricalTerm geoTerm = GeolocationPropCategoricalTerm.forId(termId);
            		allOccPropIds.add( geoTerm.asReferenceTerm().getCvtermId() );
            	}
            }
        }
		projectManager.saveOccurrencePropsAsProjectProperties(studyId, allOccPropIds);
	}
	
	/**
	 * Adds properties common to all occurrences. This properties have arbitrary/default values that 
	 * don't depend on any input.
	 * @param occ occurrence that will common properties set.
	 */
	private void addFixedProperties(Occurrence occ){
		occ.getExtraMetadata().put(LocationTerm.EXPT_DESIGN.id(), LocationTerm.EXPT_DESIGN_COMPLETE_BLOCK);
	}


	
	private void preparePropertiesForLocation(Occurrence occ){
		Location location = converter.convertComposite(occ.getExtraMetadata(), Integer.class, String.class, Location.class);
		
		if(location != null){
			occ.getExtraMetadata().put(LocationTerm.LOC_NAME.id(), location.getLname());
			occ.getExtraMetadata().put(LocationTerm.LOC_ID.id(), String.valueOf(location.getLocid()));
		}
	}

	/**
	 * saves all {@link Occurrence}s in a {@link Study} as {@link NdGeolocation} entities
	 * @param study study with occurrences to be saved
	 * @return list of geolocations representing study occurrences
	 */
	private List<NdGeolocation> saveGeolocations(Study study){
        LOG.debug("saveGeolocations processing...");
	    
	    List<NdGeolocation> geolocations = new ArrayList<>(study.getOccurrences().size());
	    List<NdGeolocationprop> geoProperties = new ArrayList<>(10);
	    
	    int numGeolocations = study.getOccurrences().size();
	    
	    for (int occCount = 0; occCount < numGeolocations; occCount++) {
	        
            Occurrence occ = study.getOccurrences().get(occCount);
            
	        NdGeolocation geolocation = new NdGeolocation(occ.getNumber().toString());
	        geolocations.add(geolocation);
	        
            geoProperties.addAll( createGeoProperties(occ.getExtraMetadata(), geolocation));
        }
	    
        LOG.trace("saveGeolocations ndGeolocationRepo-persisting: {}", geolocations.size());
	    geolocations = ndGeolocationRepo.save(geolocations);
        LOG.trace("saveGeolocations ndGeolocationPropRepo-persisting: {}", geoProperties.size());
	    ndGeolocationPropRepo.save(geoProperties);
	    
	    return geolocations;
	}

	/**
	 * Saves design of each {@link Occurrence} as a series of experiments
	 * @param study Contains occurrences to save
	 * @param studyId Identifier of passed study
	 * @param geolocations list of {@link NdGeolocation} representing the {@link Occurrence}s in the study
	 * @return
	 */
	private Map<Integer, List<NdExperiment>> saveInstancesAsExperiments(Study study, StudyId studyId, List<NdGeolocation> geolocations){
	    LOG.debug("saveInstancesAsExperiments processing...");		
		NdExperiment experiment = null;
		NdExperimentProject expProject = null;
		
        LOG.trace("saving experiment for main project");      
		experiment = ndExperimentRepo.save(
				new NdExperiment(geolocations.get(0), ProjectPropDescriptorTerm.STUDY_INFO.asTerm()));
		ndExpProjectRepo.save(new NdExperimentProject(studyId.getMainProject(), experiment));

		Map<Integer, List<NdExperiment>> allExperiments = 
		        new HashMap<>(study.getOccurrences().size()*2);
		int occIdx = 0;
		
		for(Occurrence occ : study.getOccurrences()){
		    int numExperiments = occ.getEntries().size() + study.getOccurrences().size() + 1;
            List<NdExperiment> experiments = new ArrayList<>(numExperiments);
            List<NdExperimentProject> expProjects = new ArrayList<>(numExperiments);
            ArrayList<NdExperimentprop> expProps = new ArrayList<NdExperimentprop>(numExperiments*2); //for plot+rep

            LOG.trace("saving experiment by occurrence");      
			experiment = ndExperimentRepo.save(
					new NdExperiment(geolocations.get(occIdx), ProjectPropDescriptorTerm.TRIAL_ENV_INFO.asTerm()));
			expProject = new NdExperimentProject(studyId.getEnvironmentProject(), experiment);
			ndExpProjectRepo.save(expProject);

	        LOG.trace("generating experiment by entry");   
			
			for(StudyEntry entry : occ.getEntries()){
                experiment = new NdExperiment(geolocations.get(occIdx), ProjectPropDescriptorTerm.TRIAL_PLOT_INFO.asTerm());
                experiment.setEntryNum(entry.getEntryNum()); // IMPRTANT! experiments must be referenced by entry number
                
                expProject = new NdExperimentProject(studyId.getPlotDataProject(), experiment);
                experiments.add(experiment);
                expProjects.add(expProject);
                
    			if(study.getStudyType().equals("T")){
   			        addDesignProps(expProps, experiment, entry);
    			}

			}
			LOG.trace("saveInstancesAsExperiments ndExperimentRepo-persisting: {}", experiments.size());
			batchRepo.bulkSave(experiments);
			LOG.trace("saveInstancesAsExperiments ndExpProjectRepo-persisting: {}", expProjects.size());
            batchRepo.bulkSave(expProjects);
			LOG.trace("saveInstancesAsExperiments ndExperimentPropRepo-persisting: {}", expProps.size());
		    batchRepo.bulkSave(expProps);
			
			allExperiments.put(occ.getNumber(), experiments);
            
            occIdx++;
		}
		
		return allExperiments;
		
	}
	
	/**
	 * Adds properties depending on the study type
	 * @param expProps list where the properties created will be stored
	 * @param experiment experiment instance representing an entry
	 * @param entry contains the values of the properties to attach to the given experiment
	 */
	private void addDesignProps(ArrayList<NdExperimentprop> expProps, NdExperiment experiment, StudyEntry entry){
		int rankCount = 0;
	    expProps.add(new NdExperimentprop(experiment,
                ProjectPropTerm.PLOT_NUMBER.asTerm(), ++rankCount, entry.getPlot().toString()));
	    expProps.add(new NdExperimentprop(experiment,
                ProjectPropTerm.REP_NUMBER.asTerm(), ++rankCount, entry.getRep().toString()));
	    if(entry.hasRow()) {
	    	expProps.add(new NdExperimentprop(experiment,
                ProjectPropTerm.ROW_NUMBER.asTerm(), ++rankCount, entry.getRow().toString()));
	    }
	    if(entry.hasColumn()) {
	    	expProps.add(new NdExperimentprop(experiment,
                ProjectPropTerm.COLUMN_NUMBER.asTerm(), ++rankCount, entry.getCol().toString()));
	    }
	    if(entry.hasSubblock()) {
	    	expProps.add(new NdExperimentprop(experiment,
                ProjectPropTerm.SUB_BLOCK.asTerm(), ++rankCount, entry.getSubBlock().toString()));
	    }
	    
	}

	/**
	 * Creates metadata for one {@link Occurrence}
	 * @param occProperties map with the ids and values of properties to set
	 * @param geolocation entity to attach the given occProperties.
	 * @return
	 */
	private List<NdGeolocationprop> createGeoProperties(final Map<Integer, String> occProperties, final NdGeolocation geolocation){
		
		Map<Integer, String> occPropsWithIds = new HashMap<>(occProperties);
		
		addIdValuesForCategorical(occPropsWithIds, occProperties);
		
        List<Cvterm> occProps = 
        		cvTermRepo.findAll(occPropsWithIds.keySet());
        
        geolocPropBuilder.clean();
        for(Cvterm occPropTerm : occProps){
        	geolocPropBuilder
        		.createGeoProperty(geolocation, 
                    new BasicStandardTerm(occPropTerm),
                    occPropsWithIds.get(occPropTerm.getCvtermId()));
        }
        return geolocPropBuilder.getCurrentProperties();
	}
	
	/**
	 * Adds properties to an occurrence for categorical properties, since this values need and additional 
	 * id value for proper display.
	 * @param occPropsWithIds map where to add required id values
	 * @param occProperties map to look for categorical text values 
	 */
	private void addIdValuesForCategorical(Map<Integer, String> occPropsWithIds, Map<Integer, String> occProperties){
		for(Entry<Integer, String> termEntry : occProperties.entrySet()) {

			if(GeolocationPropCategoricalTerm.hasGeolocPropTerm(termEntry.getKey())) {
	    		GeolocationPropCategoricalTerm geoProp = GeolocationPropCategoricalTerm.forId(termEntry.getKey());
	    		
	    		Categorical categoricalValue = converter.convert(termEntry.getValue(), geoProp);
	    		occPropsWithIds.put(geoProp.asReferenceTerm().getCvtermId(), categoricalValue.getValue());
	    	}
		}
	}

	@Override
	public Page<NdGeolocation> findByProject(Pageable pageRequest, Project project) {
		Page<NdExperimentProject> experimentProjects = ndExpProjectRepo.findByProject(pageRequest, project);
		List<NdGeolocation> geolocations = null;
		
		if(experimentProjects != null){
			geolocations = extractGeolocations(experimentProjects.getContent());
			List<NdGeolocationprop> allGeolocationProps = ndGeolocationPropRepo.findByNdGeolocationIn(geolocations);
			fillGeolocationsWithProperties(geolocations, allGeolocationProps);
		}
		
		return new PageImpl<NdGeolocation>(geolocations, pageRequest, experimentProjects.getTotalElements());
	}

	/**
	 * retrieves all the {@link NdGeolocation geolocations} associated with a {@link NdExperimentProject} list 
	 * @param experimentProjects element to to be used for searching geolocations
	 * @return the geolocations belonging to the given parameter
	 */
	private List<NdGeolocation> extractGeolocations(List<NdExperimentProject> experimentProjects){
		List<NdGeolocation> geolocations = new ArrayList<>(experimentProjects.size());
		
		for (NdExperimentProject expProject : experimentProjects) {
			geolocations.add(expProject.getNdExperiment().getNdGeolocation());
		}
		
		return geolocations;
	}
	
	/**
	 * Retrieves all {@link NdGeolocation}s associated with the given experiment-project links, 
	 * and fill in those entities with provided {@link NdGeolocationprop}s
	 * @param geolocations to be filled with properties provided
	 * @param allGeolocationProps geolocation properties to be put into corresponding geolocation
	 */
	private void fillGeolocationsWithProperties(List<NdGeolocation> geolocations
			,List<NdGeolocationprop> allGeolocationProps){

		for(NdGeolocation geoloc : geolocations){
			List<NdGeolocationprop> props = new ArrayList<>(10);

			for(NdGeolocationprop prop : allGeolocationProps){
				if(prop.getNdGeolocation().equals(geoloc)){
					props.add(prop);
				}
			}
			geoloc.setNdGeolocationprops(props);
			allGeolocationProps.removeAll(props);
		}

	}

	@Override
	public List<NdExperiment> findExperimentsByGeolocation(int geolocationId){
		NdGeolocation ndGeolocation = ndGeolocationRepo.findOne(geolocationId);
		if(ndGeolocation == null){
			throw new GeolocationNotFoundException(geolocationId);
		}
		
		List<NdExperiment> experiments = ndExperimentRepo
				.findByNdGeolocationAndType(ndGeolocation,
						ProjectPropDescriptorTerm.TRIAL_PLOT_INFO.asTerm());
		return experiments;
	}

	
}
