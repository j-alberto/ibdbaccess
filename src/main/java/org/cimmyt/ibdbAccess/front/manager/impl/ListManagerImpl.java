package org.cimmyt.ibdbAccess.front.manager.impl;

import java.util.List;

import org.cimmyt.ibdbAccess.conversion.BeanConverter;
import org.cimmyt.ibdbAccess.core.domain.Listdata;
import org.cimmyt.ibdbAccess.core.domain.ListdataProject;
import org.cimmyt.ibdbAccess.core.domain.Listnm;
import org.cimmyt.ibdbAccess.core.domain.StudyId;
import org.cimmyt.ibdbAccess.core.domain.WorkbenchIbdbUserMap;
import org.cimmyt.ibdbAccess.core.repository.BatchRepository;
import org.cimmyt.ibdbAccess.core.repository.ListDataProjectRepository;
import org.cimmyt.ibdbAccess.core.repository.ListnmRepository;
import org.cimmyt.ibdbAccess.core.repository.WorkbenchIbdbUserMapRepository;
import org.cimmyt.ibdbAccess.front.domain.GermplasmListEntry;
import org.cimmyt.ibdbAccess.front.domain.ListType;
import org.cimmyt.ibdbAccess.front.domain.Study;
import org.cimmyt.ibdbAccess.front.manager.ListManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
/**
 * Implementation of {@link ListManager}
 * @author jarojas
 *
 */
@Component
@Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
public class ListManagerImpl implements ListManager{

	private ListDataProjectRepository dataProjectRepo;
	private ListnmRepository listnmRepo;
	private BeanConverter beanConverter;
	private WorkbenchIbdbUserMapRepository ibdbUserMapRepo;
	@Autowired
	private BatchRepository batchRepo;
	
	private static final Logger LOG = LoggerFactory.getLogger(ListManagerImpl.class);

	@Autowired
	public ListManagerImpl(ListnmRepository listnmRepo,	ListDataProjectRepository dataProjectRepo,
			WorkbenchIbdbUserMapRepository ibdbUserMapRepo,
			BeanConverter beanConverter){
		this.listnmRepo = listnmRepo;
		this.dataProjectRepo = dataProjectRepo;
		this.beanConverter = beanConverter;
		this.ibdbUserMapRepo = ibdbUserMapRepo;
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED,readOnly=false)
	public int saveList(Study study) {
	    LOG.trace("saving germplasm list");
		Listnm listnm = beanConverter.convert(study, Listnm.class);
		listnm.setListtype(ListType.LST.toString());
		listnm.setListuid(findIbdbUserId(study));
		prepareFolderForList(study, listnm);


		listnm = listnmRepo.save(listnm);
        saveListDataEntries(study.getGermplasmEntries(), listnm);
		
		LOG.trace("saved list with id: {}", listnm.getListid());
		return listnm.getListid();
	}
	
	private void prepareFolderForList(Study study, Listnm list){
		Listnm listFolder = listnmRepo.findByListnameAndListtypeAndProgramUuid(
				study.getFolder().getName(),
				ListType.FOLDER.toString(),
				study.getFolder().getProgramUuid());
		
		if(listFolder == null){
			listFolder =  new Listnm();
			listFolder.setListname(study.getFolder().getName());
			listFolder.setListdate(list.getListdate());
			listFolder.setListtype(ListType.FOLDER.toString());
			listFolder.setListuid(list.getListuid());
			listFolder.setListdesc(study.getFolder().getName());
			listFolder.setListuid( list.getListuid() );
			listFolder.setProgramUuid(study.getFolder().getProgramUuid());
			listFolder.setListstatus(0);
			
			listFolder = listnmRepo.save(listFolder);
			LOG.trace("created list folder with id: {}", listFolder.getListid());
		}
		list.setLhierarchy(listFolder.getListid());
	}
	
	
	private int findIbdbUserId(Study study){
		WorkbenchIbdbUserMap userMap = ibdbUserMapRepo.findByWorkbenchUserId(study.getUserId()).get(0);
		if(userMap != null){
			return userMap.getIbdbUserId();
		}
		return 0;
	}

    private void saveListDataEntries(List<GermplasmListEntry> germplasmEntries, Listnm listnm){
        List<Listdata> listDatas = beanConverter.convert(germplasmEntries, GermplasmListEntry.class, Listdata.class);
        if(listDatas != null && listDatas.size() > 0){
            for(Listdata entry : listDatas){
            	entry.setList(listnm);
            }
            
            batchRepo.bulkSave(listDatas);
        }
    }
    
    @Override
    @Transactional(propagation=Propagation.REQUIRED,readOnly=false)
    public List<ListdataProject> saveStudyList(Study study,
            StudyId studyId, Integer parentListId) {
        
        LOG.trace("saving study list for studyId: {}", studyId.getMainId());
        Listnm listnm = beanConverter.convert(study, Listnm.class);
        listnm.setListtype(ListType.TRIAL.toString());
        listnm.setProjectid(studyId.getMainId());
        listnm.setListref(parentListId);
        listnm.setListuid(findIbdbUserId(study));

        listnm = listnmRepo.save(listnm);
        List<ListdataProject> listDataProjects = beanConverter.convert(study.getGermplasmEntries(), GermplasmListEntry.class, ListdataProject.class);
        saveListDataProjectEntries(listDataProjects, listnm);
    
        LOG.trace("saved study list with id: {}", listnm.getListid());
        return listDataProjects;
    }

	private List<ListdataProject> saveListDataProjectEntries(List<ListdataProject> listDataProjects, Listnm listnm){
		if(listDataProjects != null && listDataProjects.size() > 0){
			
			for(ListdataProject entry : listDataProjects){
				entry.setList(listnm);
			}
			listDataProjects = batchRepo.bulkSave(listDataProjects);
		}
		return listDataProjects;
	}

	@Override
	public Listnm findList(int id) {
		
		return listnmRepo.findOne(id);
	}

	@Override
	public List<ListdataProject> findStudyList(int studyId) {
		Listnm projectList = listnmRepo.findByProjectid(studyId);
		if(projectList != null){
			return dataProjectRepo.findByListListid(projectList.getListid());
		}else{
			return null;
		}
	}
	
}
