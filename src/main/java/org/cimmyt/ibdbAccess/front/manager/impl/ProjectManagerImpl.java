package org.cimmyt.ibdbAccess.front.manager.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.cimmyt.ibdbAccess.core.domain.Cvterm;
import org.cimmyt.ibdbAccess.core.domain.Project;
import org.cimmyt.ibdbAccess.core.domain.ProjectRelationship;
import org.cimmyt.ibdbAccess.core.domain.Projectprop;
import org.cimmyt.ibdbAccess.core.domain.StudyId;
import org.cimmyt.ibdbAccess.core.domain.WorkbenchProject;
import org.cimmyt.ibdbAccess.core.repository.CvTermRepository;
import org.cimmyt.ibdbAccess.core.repository.ProjectPropRepository;
import org.cimmyt.ibdbAccess.core.repository.ProjectRelationShipRepository;
import org.cimmyt.ibdbAccess.core.repository.ProjectRepository;
import org.cimmyt.ibdbAccess.front.builder.ProjectBuilder;
import org.cimmyt.ibdbAccess.front.builder.StandardTermBuiler;
import org.cimmyt.ibdbAccess.front.domain.Study;
import org.cimmyt.ibdbAccess.front.domain.term.BasicStandardTerm;
import org.cimmyt.ibdbAccess.front.domain.term.ProjectPropDescriptorTerm;
import org.cimmyt.ibdbAccess.front.domain.term.RelationshipTerm;
import org.cimmyt.ibdbAccess.front.manager.ProjectManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of {@link ProjectManager}
 * @author jarojas
 *
 */
@Component
@Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
public class ProjectManagerImpl implements ProjectManager {

	private ProjectRepository projectRepo;
	private ProjectRelationShipRepository projRelationshipRepo;
	private ProjectPropRepository projectPropRepository;
	private ProjectBuilder projectBuilder;
	private CvTermRepository cvTermRepository;
	private StandardTermBuiler termBuilder;
	
	private static final Logger LOG = LoggerFactory.getLogger(ProjectManagerImpl.class);

	@Autowired
	public ProjectManagerImpl(ProjectRepository projectRepo,
			ProjectRelationShipRepository projRelationshipRepo,
			ProjectPropRepository projectPropRepository,
			ProjectBuilder projectBuilder,
			CvTermRepository cvTermRepository,
			StandardTermBuiler termBuilder){
		this.projectRepo = projectRepo;
		this.projRelationshipRepo = projRelationshipRepo;
		this.projectPropRepository = projectPropRepository;
		this.projectBuilder = projectBuilder;
		this.cvTermRepository = cvTermRepository;
		this.termBuilder = termBuilder;
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED,readOnly=false)
	public StudyId saveProject(Study study) {
	    LOG.debug("saveProject processing...");
		
		StudyId studyId = projectBuilder.buildProject(study, study.getFolder());
		
		LOG.debug("saveProject persisting...");
		projectRepo.save(projectBuilder.getProjects());
		projRelationshipRepo.save(projectBuilder.getProjectRelationships());
		projectPropRepository.save(projectBuilder.getProjectProperties());
		projectPropRepository.flush();
		
		return studyId;
	}
	
	@Override
	public Project findFolderByNameInProgram(String folderName,	WorkbenchProject program) {
		Project folder = projectRepo.findByNameAndProgramUuid(folderName, program.getProjectUuid());
		if(folder != null){
			ProjectRelationship relationship = projRelationshipRepo.
					findBySubjectAndRelationshipType(folder, RelationshipTerm.A_SUBFOLDER_OF.asTerm());
			
			if(relationship == null){
				folder = null;
			}
		}
		return folder;
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED,readOnly=false)
	public Project saveFolderInProgram(String folderName, WorkbenchProject program) {
		
		Project folder = new Project(folderName,
				folderName,
				program.getProjectUuid());
		projectRepo.save(folder);

		ProjectRelationship relationship = new ProjectRelationship(
				folder,
				new Project(1),
				RelationshipTerm.A_SUBFOLDER_OF.asTerm());
		projRelationshipRepo.save(relationship);
		
		projectRepo.flush();
		projRelationshipRepo.flush();
		
		return folder;
	}

	@Override
	public Page<Project> findByName(Pageable page, String name) {
		String wildcardedName = new StringBuilder()
				.append("%")
				.append(name.trim())
				.append("%")
				.toString();
		
		Page<ProjectRelationship> pageProjRelationship = 
				projRelationshipRepo.findBySubjectNameLikeAndRelationshipType(page, 
						wildcardedName, 
						RelationshipTerm.A_STUDY_IN.asTerm());
		
		List<Project> projects = new ArrayList<>(pageProjRelationship.getContent().size());
		for(ProjectRelationship rel : pageProjRelationship.getContent()){
			projects.add(rel.getSubject());
		}
		
		return  new PageImpl<Project>(projects, page, pageProjRelationship.getTotalElements());
	}
	
	@Override
	public List<Projectprop> findStudyGeneralProperties(Integer projectId){
		
		return projectPropRepository
				.findByProjectProjectIdAndTypeCvtermIdNotIn( projectId,	getMetaProjectProperties());
	}
	/**
	 * Returns the ids of types in {@link Projectprop} used to store metadata about the properties,
	 * and not the actual values. For example, the three ids in bold
	 * <table td style="border:1 solid; margin:0;">
	 * <tr><td>type_id</td><td>value</td></tr>
	 * <tr><td><strong>1805</strong></td><td>STUDY_NAME</td></tr>
	 * <tr><td><strong>1060</strong></td><td>Study - assigned (DBCV)</td></tr>
	 * <tr><td><strong>1070</strong></td><td>8005</td></tr>
	 * <tr><td>8005</td><td>trial1</td></tr>
	 * </table>
	 * @return ids of project properties defining metadata of variables
	 */
	private List<Integer> getMetaProjectProperties(){
		List<Integer> propIds = new ArrayList<>(3);
		propIds.add(ProjectPropDescriptorTerm.STUDY_DETAIL.id());
		propIds.add(ProjectPropDescriptorTerm.VARIABLE_DESCRIPTION.id());
		propIds.add(ProjectPropDescriptorTerm.STANDARD_VAR_ID.id());

		return propIds;
	}

	@Override
	public StudyId findStudyId(int studyId){
		Project mainProject = new Project(studyId);
		
		List<ProjectRelationship> relationships =
				projRelationshipRepo.findByObjectAndRelationshipType(mainProject, RelationshipTerm.A_DATASET_OF.asTerm());
		
		return projectBuilder.createStudyIdWithProjectAndDatasets(mainProject, relationships);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED,readOnly=false)
	public void saveOccurrencePropsAsProjectProperties(StudyId studyId, Set<Integer> occurrencePropertyIds) {
		List<Cvterm> projectProps =	cvTermRepository.findAll(occurrencePropertyIds);
        
        termBuilder.clean();
        for(Cvterm property : projectProps){
            termBuilder
        		.createTermForOcc(studyId.getEnvironmentProject(), 
                    new BasicStandardTerm(property));
        }
        List<Projectprop> properties = termBuilder.getCurrentProperties();
		
        LOG.debug("projectPropRepository-persisting for occ-props...");
		projectPropRepository.save(properties);

	}

}
