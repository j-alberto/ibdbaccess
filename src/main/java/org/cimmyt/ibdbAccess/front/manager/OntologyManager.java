package org.cimmyt.ibdbAccess.front.manager;

import java.util.List;

import org.cimmyt.ibdbAccess.core.domain.Cvterm;
/**
 * Responsible of managing entities for controlled vocabulary.
 * @author jarojas
 *
 */
public interface OntologyManager {
	
	List<Cvterm> findStudyPropertes(List<Integer> propertyIds);

}
