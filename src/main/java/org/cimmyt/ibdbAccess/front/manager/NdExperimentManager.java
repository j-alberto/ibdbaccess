package org.cimmyt.ibdbAccess.front.manager;

import java.util.List;
import java.util.Map;

import org.cimmyt.ibdbAccess.core.domain.NdExperiment;
import org.cimmyt.ibdbAccess.core.domain.NdGeolocation;
import org.cimmyt.ibdbAccess.core.domain.NdGeolocationprop;
import org.cimmyt.ibdbAccess.core.domain.Project;
import org.cimmyt.ibdbAccess.core.domain.StudyId;
import org.cimmyt.ibdbAccess.front.domain.Study;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Responsible of saving a study in proper Experiment tables, which are the ones linking study instances and projects 
 * with phenotype values
 * @author jarojas
 *
 */
public interface NdExperimentManager {

	/**
	 * Saves metadata for each study instance
	 * @param study
	 * @return
	 */
	Map<Integer, List<NdExperiment>> saveStudyInstances(Study study, StudyId studyId);

	/**
	 * Returns the list of {@link NdGeolocation}s for a given study, including its {@link NdGeolocationprop properties}
	 * @param pageRequest subset to retrieve for large results
	 * @param project representation of a ENVIRONMENT dataset for a study,
	 * @return a {@link Page} for the list of geolocations associated to a project.
	 */
	Page<NdGeolocation> findByProject(Pageable pageRequest, Project project);
	
	/**
	 * Retrieves all {@link NdExperiment} instances associated with a particular {@link NdGeolocation}
	 * @param geolocationId id of the geolocation owning the experiment instances.
	 * @return the list of experiments for this geolocation
	 */
	List<NdExperiment> findExperimentsByGeolocation(int geolocationId);
}
