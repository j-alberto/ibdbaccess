package org.cimmyt.ibdbAccess.front.manager;

import org.cimmyt.ibdbAccess.core.domain.WorkbenchProject;

public interface ProgramManager {

	WorkbenchProject saveNewProgram(String name, Integer userId) throws RuntimeException;
	
	WorkbenchProject findByName(String name);
}
