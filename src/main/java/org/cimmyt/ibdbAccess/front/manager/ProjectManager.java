package org.cimmyt.ibdbAccess.front.manager;

import java.util.List;
import java.util.Set;

import org.cimmyt.ibdbAccess.core.domain.Project;
import org.cimmyt.ibdbAccess.core.domain.Projectprop;
import org.cimmyt.ibdbAccess.core.domain.StudyId;
import org.cimmyt.ibdbAccess.core.domain.WorkbenchProject;
import org.cimmyt.ibdbAccess.front.domain.Study;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
/**
 * Controls projects
 * @author jarojas
 *
 */
public interface ProjectManager {
    /**
     * saves a {@link Study} as a set of {@link Project} elements
     * @param study entity to save
     * @return the {@link StudyId} generated for such study.
     */
	StudyId saveProject(Study study);

	Project findFolderByNameInProgram(String folderName, WorkbenchProject program);
	
	Project saveFolderInProgram(String folderName, WorkbenchProject program);
	
	Page<Project> findByName(Pageable page, String name);
	/**
	 * Finds {@link Projectprop} elements containing the actual values of variables for a study
	 * @param projectId identifies the study
	 * @return a list of project properties for the given study
	 */
	List<Projectprop> findStudyGeneralProperties(Integer projectId);
	
	/**
	 * Returns the composite identifier of an existing {@link Study}
	 * @param studyId the id of the main {@link Project} entity
	 * @return the {@link StudyId} representing a trial or nursery, or null if not found
	 */
	
	StudyId findStudyId(int studyId);
	
	/**
	 * saves new {@link Projectprop} elements that describe occurrence properties for one study. This method must be used only once
	 * per study, since multiple calls for the same study may throw duplication exceptions.
	 * @param studyId of the study to add new occurrence properties
	 * @param occurrencePropertyIds to be added as metadata of the given study
	 */
	void saveOccurrencePropsAsProjectProperties(StudyId studyId, Set<Integer> occurrencePropertyIds);
}
