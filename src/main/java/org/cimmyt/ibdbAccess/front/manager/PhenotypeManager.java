package org.cimmyt.ibdbAccess.front.manager;

import java.util.List;
import java.util.Map;

import org.cimmyt.ibdbAccess.core.domain.NdExperiment;
import org.cimmyt.ibdbAccess.core.domain.StudyId;
import org.cimmyt.ibdbAccess.front.domain.Study;
/**
 * Manages phenotype information for a {@link Study}
 * @author jarojas
 *
 */
public interface PhenotypeManager {

    /**
     * Saves Study phenotype information
     * @param studyId {@link StudyId Id} of the {@link Study}
     * @param study contains the phenotype data to be saved
     * @param experiments experiments representing entries to be linked with phenotype values.
     */
	void savePhenotypes(StudyId studyId, Study study, Map<Integer, List<NdExperiment>> experiments);

	/**
	 * Sets phenotypes property for each experiment in the list argument. Only experiments of type 
	 * ProjectPropDescriptorTerm.TRIAL_PLOT_INFO can have phenotype information.
	 * @param experiments to look for phenotype information 
	 */
	void loadPhenotypes(List<NdExperiment> experiments);
}
