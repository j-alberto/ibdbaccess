package org.cimmyt.ibdbAccess.front.manager.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cimmyt.ibdbAccess.core.domain.Cvterm;
import org.cimmyt.ibdbAccess.core.domain.CvtermRelationship;
import org.cimmyt.ibdbAccess.core.domain.NdExperiment;
import org.cimmyt.ibdbAccess.core.domain.NdExperimentPhenotype;
import org.cimmyt.ibdbAccess.core.domain.Phenotype;
import org.cimmyt.ibdbAccess.core.domain.Project;
import org.cimmyt.ibdbAccess.core.domain.StudyId;
import org.cimmyt.ibdbAccess.core.repository.BatchRepository;
import org.cimmyt.ibdbAccess.core.repository.CvTermRelationshipRepository;
import org.cimmyt.ibdbAccess.core.repository.CvTermRepository;
import org.cimmyt.ibdbAccess.core.repository.NdExperimentPhenotypeRepository;
import org.cimmyt.ibdbAccess.core.repository.ProjectPropRepository;
import org.cimmyt.ibdbAccess.front.builder.StandardTermBuiler;
import org.cimmyt.ibdbAccess.front.domain.Occurrence;
import org.cimmyt.ibdbAccess.front.domain.Study;
import org.cimmyt.ibdbAccess.front.domain.StudyEntry;
import org.cimmyt.ibdbAccess.front.domain.term.BasicStandardTerm;
import org.cimmyt.ibdbAccess.front.domain.term.ProjectPropDescriptorTerm;
import org.cimmyt.ibdbAccess.front.domain.term.RelationshipTerm;
import org.cimmyt.ibdbAccess.front.domain.term.Term;
import org.cimmyt.ibdbAccess.front.exception.MissingOntologyTermException;
import org.cimmyt.ibdbAccess.front.manager.PhenotypeManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Implementation of {@link PhenotypeManager}
 * @author jarojas
 *
 */
@Component
public class PhenotypeManagerImpl implements PhenotypeManager {

    private BatchRepository batchRepo;
    int phenotypeId;
    private static final Logger LOG = LoggerFactory.getLogger(NdExperimentManagerImpl.class);
            
    private CvTermRepository cvTermRepo;
    private StandardTermBuiler termBuilder;
    private CvTermRelationshipRepository cvTermRelationRepo;
    private ProjectPropRepository projectPropRepo;
    private NdExperimentPhenotypeRepository experimentPhenotypeRepo;
             
    @Autowired
    public PhenotypeManagerImpl(BatchRepository batchRepo
    		,CvTermRepository cvTermRepo, StandardTermBuiler termBuilder,
    		CvTermRelationshipRepository cvTermRelationRepo,
    		ProjectPropRepository projectPropRepo,
    		NdExperimentPhenotypeRepository experimentPhenotypeRepo) {
        super();
        this.batchRepo = batchRepo;
        this.cvTermRepo = cvTermRepo;
        this.termBuilder = termBuilder;
        this.cvTermRelationRepo = cvTermRelationRepo;
        this.projectPropRepo = projectPropRepo;
        this.experimentPhenotypeRepo = experimentPhenotypeRepo;
    }
    
    @Override
    public void savePhenotypes(StudyId studyId, Study study,
            Map<Integer, List<NdExperiment>> mapExperiments) {

        LOG.debug("savePhenotypes processing study: {}...", studyId.getMainProject().getName());

        saveTraitsAsProjectProperties(studyId.getPlotDataProject(), getAllTraits(study));
        
        for(Occurrence occ : study.getOccurrences()){
            LOG.trace("processing occ: {}, entries: {}", occ.getNumber(), occ.getEntries().size());
            savePhenotypesByOccurrence(mapExperiments.get(occ.getNumber()), occ);
        }
    }
    
    /**
     * Saves {@link Phenotype} entities for one {@link Occurrence}
     * @param experiments entities that link projects with phenotypes
     * @param occ occurrence whose trait values will be saved as {@link Phenotype}s
     * @throws Exception 
     */
    private void savePhenotypesByOccurrence(List<NdExperiment> experiments, Occurrence occ) {
       int traitsInOcc = getAllTraitsInOcc(occ).size();
        List<Phenotype> phenotypes = new ArrayList<Phenotype>(occ.getEntries().size() * traitsInOcc);
        List<NdExperimentPhenotype> expPhenotypes = new ArrayList<NdExperimentPhenotype>(occ.getEntries().size() * traitsInOcc);
        
        int expCounter = 0;
        for(StudyEntry entry : occ.getEntries()){
            for(Integer traitId : entry.getTraitValues().keySet()){
                
                Cvterm trait = cvTermRepo.findOne(traitId);
                
                if(trait == null){
                	throw new MissingOntologyTermException(occ.getNumber(), entry.getEntryNum(), traitId);
                }
                
                Phenotype phenotype = new Phenotype(trait,
                		cleanValue(entry, traitId));
                
                phenotypes.add(phenotype);
                expPhenotypes.add(
                        new NdExperimentPhenotype(experiments.get(expCounter),phenotype));
            }
            expCounter++;
        }
        LOG.trace("persisting phenotypes: {}", phenotypes.size());
        batchRepo.bulkSave(phenotypes);
        LOG.trace("persisting experimentPhenotypes: {}", expPhenotypes.size());
        batchRepo.bulkSave(expPhenotypes);
    }
    
    private String cleanValue(StudyEntry entry, int traitId){
    	String val =  entry.getTraitValues().get(traitId);
    	if(val != null){
    		val = val.trim();
    		if(val.equals("-"))
    			val = "missing";
    	}else{
    		val = "";
    	}
    	return val;
    }

    /**
     * Defines traits used in a study
     * @param project represents a Plot dataset
     * @param termIds list of ids to be checked as valid ontology {@link Term}s.
     */
    private void saveTraitsAsProjectProperties(Project project, Set<Integer> termIds){
    	termBuilder.clean();

		List<CvtermRelationship> traitsRel = 
		    cvTermRelationRepo.findBySubjectCvtermIdInAndTypeId(
		        termIds,
				RelationshipTerm.HAS_SCALE.asTerm().getCvtermId());
    	
    	for (CvtermRelationship traitRel : traitsRel) {
    		termBuilder.createTerm(project,
    		    new BasicStandardTerm(traitRel.getSubject()),
    			ProjectPropDescriptorTerm.TRAIT.asTerm());
		}
    	projectPropRepo.save(termBuilder.getCurrentProperties());
    }
    
    /**
     * Searches for all different trait ids across the occurrences in the given study
     * @param study the study to search traits in
     * @return a Set of trait ids found in the study
     */
	private Set<Integer> getAllTraits(Study study){
		Set<Integer> allTraits = new HashSet<>();
		for (Occurrence occ : study.getOccurrences()) {
			allTraits.addAll(getAllTraitsInOcc(occ));
		}
		return allTraits;
	}
	
	private Set<Integer> getAllTraitsInOcc(Occurrence occ){
		Set<Integer> traits = new HashSet<>();
			for(StudyEntry entry : occ.getEntries()){
				traits.addAll(entry.getTraitValues().keySet());
			}
		return traits;
	}

	@Override
	public void loadPhenotypes(List<NdExperiment> experiments) {
		Map<Integer, NdExperiment> experimentMap = new HashMap<>(experiments.size() * 2);
		for (NdExperiment experiment : experiments) {
			experimentMap.put(experiment.getNdExperimentId(), experiment);
		}
		
		List<NdExperimentPhenotype> ndExperimentPhenotypes =
				experimentPhenotypeRepo.findByNdExperimentIn(experiments);
		for (NdExperimentPhenotype ndExperimentPhenotype : ndExperimentPhenotypes) {
			
			experimentMap.get(ndExperimentPhenotype.getNdExperiment().getNdExperimentId())
				.addPhenotype(ndExperimentPhenotype.getPhenotype());
		}
	}

}
