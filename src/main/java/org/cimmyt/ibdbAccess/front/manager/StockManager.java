package org.cimmyt.ibdbAccess.front.manager;

import java.util.List;
import java.util.Map;

import org.cimmyt.ibdbAccess.core.domain.ListdataProject;
import org.cimmyt.ibdbAccess.core.domain.NdExperiment;
/**
 * Controls stock tables, which store germplasm entries along with design information for every study instance
 * @author jarojas
 *
 */
public interface StockManager {

/**
 * saves in inventory tables the {@link NdExperiment} entities of a study, and its associated {@link ListdataProject} elements.
 * This is required so that a BMS can properly display design information in Trial Manager and Nursery Manager.
 * @param experimentEntries elements representing the germplasm list source for a study design 
 * @param experiments elements representing the entries in a study design
 * @return
 */
	void saveExperimentsInStock(List<ListdataProject> listEntries,
	        Map<Integer, List<NdExperiment>> experiments);

}
