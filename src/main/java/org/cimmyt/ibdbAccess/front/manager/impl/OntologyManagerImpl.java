package org.cimmyt.ibdbAccess.front.manager.impl;

import java.util.List;

import org.cimmyt.ibdbAccess.core.domain.Cvterm;
import org.cimmyt.ibdbAccess.core.repository.CvTermRepository;
import org.cimmyt.ibdbAccess.front.manager.OntologyManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
public class OntologyManagerImpl implements OntologyManager {

	private CvTermRepository cvTermRepo;
	
	
	@Autowired
	public OntologyManagerImpl(CvTermRepository cvTermRepo) {
		super();
		this.cvTermRepo = cvTermRepo;
	}



	@Override
	public List<Cvterm> findStudyPropertes(List<Integer> propertyIds) {
		//TODO: add rules that ensure parameter ids are in fact study properties 
		return cvTermRepo.findAll(propertyIds);
	}

}
