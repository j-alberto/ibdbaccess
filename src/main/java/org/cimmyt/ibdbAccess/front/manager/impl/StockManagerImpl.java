package org.cimmyt.ibdbAccess.front.manager.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cimmyt.ibdbAccess.conversion.BeanConverter;
import org.cimmyt.ibdbAccess.core.domain.ListdataProject;
import org.cimmyt.ibdbAccess.core.domain.NdExperiment;
import org.cimmyt.ibdbAccess.core.domain.NdExperimentStock;
import org.cimmyt.ibdbAccess.core.domain.Stock;
import org.cimmyt.ibdbAccess.core.domain.Stockprop;
import org.cimmyt.ibdbAccess.core.repository.BatchRepository;
import org.cimmyt.ibdbAccess.core.repository.NdExperimentStockRepository;
import org.cimmyt.ibdbAccess.core.repository.StockPropRepository;
import org.cimmyt.ibdbAccess.core.repository.StockRepository;
import org.cimmyt.ibdbAccess.front.builder.StockPropBuilder;
import org.cimmyt.ibdbAccess.front.domain.term.ProjectPropDescriptorTerm;
import org.cimmyt.ibdbAccess.front.manager.StockManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of {@link StockManager}
 * @author jarojas
 *
 */
@Component
@Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
public class StockManagerImpl implements StockManager {

	private BeanConverter converter;
	private static final Logger LOG = LoggerFactory.getLogger(StockManagerImpl.class);
    private BatchRepository batchRepo;
    private StockPropBuilder stockPropBuilder;

	@Autowired
	public StockManagerImpl(NdExperimentStockRepository ndExperimentStockRepo,
			StockRepository stockRepo, StockPropRepository stockPropRepo,
			BeanConverter converter, BatchRepository batchRepo,
			StockPropBuilder stockPropBuilder) {
		super();
		this.converter = converter;
		this.batchRepo =  batchRepo;
		this.stockPropBuilder = stockPropBuilder;
	}


	@Override
	@Transactional(propagation=Propagation.REQUIRED,readOnly=false)
	public void saveExperimentsInStock(
			List<ListdataProject> listEntries,
			Map<Integer,List<NdExperiment>> experiments) {
		
		List<Stock> stocks = saveStocks(listEntries);
		saveStockProps(listEntries, stocks);
		saveExperimentStocks(experiments, stocks);
	}
	
	/**
	 * Saves {@link ListdataProject} entries as {@link Stock} entries
	 * @param listEntries list of entries
	 * @return the {@link Stock} list saved
	 */
	private List<Stock> saveStocks(List<ListdataProject> listEntries){
	    LOG.debug("saveStocks processing...");
		List<Stock> stocks = converter.convert(listEntries, ListdataProject.class, Stock.class);
		
		if(stocks != null){
	        LOG.trace("saveStocks stockRepo-persisting: {}", stocks.size());
	        stocks = batchRepo.bulkSave(stocks);
		}else{
		    LOG.trace("saveStocks no stocks to save");
		    stocks = Collections.emptyList();
		}
		return stocks;
	}

    /**
     * Saves additional information of {@link ListdataProject} entries as {@link Stockprop} entries, 
     * @param listEntries list of entries
     * @return the {@link Stockprop} list saved
     */
	private void saveStockProps(List<ListdataProject> listEntries, List<Stock> stocks){
	    LOG.debug("saveStockProps processing...");
	    
	    stockPropBuilder.clean(stocks.size());
	    int numEntry = 0;
	    for (Stock stock : stocks) {
			stockPropBuilder
			.buildEntryTypeProperty(stock, listEntries.get(numEntry).getCheckType())
			.buildCrossProperty(stock, listEntries.get(numEntry).getGroupName());
			numEntry++;
		}
		
		LOG.trace("saveStockProps stockPropRepo-persisting: {}", stockPropBuilder.getStockProperties().size());
		batchRepo.bulkSave(stockPropBuilder.getStockProperties());
		
	}

	/**
	 * associates experiments with stocks using the entry number as link
	 * @param mapExperiments experiment entities grouped by occurrence(instance) number
	 * @param stocks stock list to associate
	 */
	private void saveExperimentStocks(Map<Integer,List<NdExperiment>> mapExperiments, List<Stock> stocks){
	    LOG.debug("saveExperimentStocks processing...");
		Map<Integer, Stock> stockMap = getStockMap(stocks);
		
		for(Integer occNum : mapExperiments.keySet()){
    		List<NdExperimentStock> experimentStocks = new ArrayList<>(mapExperiments.get(occNum).size());
    		
    		LOG.trace("Processing occ num: {}, entries: {}", occNum, mapExperiments.get(occNum).size());
    		for (NdExperiment experiment : mapExperiments.get(occNum)) {
    			Stock s = stockMap.get(experiment.getEntryNum());
    			experimentStocks.add(
    				new NdExperimentStock(experiment,
    					ProjectPropDescriptorTerm.IBDB_STRUCTURE.asTerm(),
    					s));
    		}
    		
    		LOG.trace("saveExperimentStocks ndExperimentStockRepo-persisting: {}", experimentStocks.size());
    		batchRepo.bulkSave(experimentStocks);
		}
		
	}
	
	/**
	 * Creates a map of {@link Stock} entities using entry number as the key
	 * @param stocks list to convert to a map
	 * @return a map of stocks
	 */
	private Map<Integer, Stock> getStockMap(List<Stock> stocks){
		Map<Integer, Stock> stockMap = new HashMap<>(stocks.size()*2);
		
		for (Stock stock : stocks) {
			stockMap.put(Integer.valueOf(stock.getUniquename()), stock);//entry number is the key
		}
		
		return stockMap;
	}

}
