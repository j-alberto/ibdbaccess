package org.cimmyt.ibdbAccess.front.manager.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.cimmyt.ibdbAccess.conversion.BeanConverter;
import org.cimmyt.ibdbAccess.core.domain.User;
import org.cimmyt.ibdbAccess.core.domain.WorkbenchCrop;
import org.cimmyt.ibdbAccess.core.domain.WorkbenchIbdbUserMap;
import org.cimmyt.ibdbAccess.core.domain.WorkbenchProject;
import org.cimmyt.ibdbAccess.core.domain.WorkbenchProjectUserInfo;
import org.cimmyt.ibdbAccess.core.domain.WorkbenchProjectUserRole;
import org.cimmyt.ibdbAccess.core.domain.WorkbenchUser;
import org.cimmyt.ibdbAccess.core.domain.WorkbenchUserRole;
import org.cimmyt.ibdbAccess.core.repository.UserRepository;
import org.cimmyt.ibdbAccess.core.repository.WorkbenchIbdbUserMapRepository;
import org.cimmyt.ibdbAccess.core.repository.WorkbenchProjectRepository;
import org.cimmyt.ibdbAccess.core.repository.WorkbenchProjectUserInfoRepository;
import org.cimmyt.ibdbAccess.core.repository.WorkbenchProjectUserRoleRepository;
import org.cimmyt.ibdbAccess.core.repository.WorkbenchUserRepository;
import org.cimmyt.ibdbAccess.front.exception.UserNotFoundException;
import org.cimmyt.ibdbAccess.front.manager.ProgramManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
public class ProgramManagerImpl implements ProgramManager {

	private WorkbenchProjectRepository workbenchProjectRepo;
	private WorkbenchUserRepository workbenchUserRepo;
	private UserRepository userRepo;
	private WorkbenchIbdbUserMapRepository workbenchIbdbUserMapRepo;
	private WorkbenchProjectUserInfoRepository workbenchProjectUserInfoRepo;
	private WorkbenchProjectUserRoleRepository workbenchProjectUserRoleRepo;
	
	@Autowired
	private BeanConverter converter;
	
	@Autowired
	public ProgramManagerImpl(
			WorkbenchProjectRepository workbenchProjectRepo,
			WorkbenchUserRepository workbenchUserRepo,
			UserRepository userRepo,
			WorkbenchIbdbUserMapRepository workbenchIbdbUserMapRepo,
			WorkbenchProjectUserInfoRepository workbenchProjectUserInfoRepo,
			WorkbenchProjectUserRoleRepository workbenchProjectUserRoleRepo) {
		super();
		this.workbenchProjectRepo = workbenchProjectRepo;
		this.workbenchUserRepo = workbenchUserRepo;
		this.userRepo = userRepo;
		this.workbenchIbdbUserMapRepo = workbenchIbdbUserMapRepo;
		this.workbenchProjectUserInfoRepo = workbenchProjectUserInfoRepo;
		this.workbenchProjectUserRoleRepo = workbenchProjectUserRoleRepo;
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=false)
	public WorkbenchProject saveNewProgram(String name, Integer userId) throws RuntimeException{
		User cropUser = null;
		WorkbenchUser workbenchUser = null;
		WorkbenchProject program = null;
		
		workbenchUser = workbenchUserRepo.findOne(userId);
		if(workbenchUser == null){
			throw new UserNotFoundException(userId);
		}

		try{
			cropUser = prepareCropUser(workbenchUser);
			program = createProgramAsProject(name, workbenchUser);

			linkUserToProgram(program, workbenchUser, cropUser);
			linkRolesToProgram(program, workbenchUser);
		}catch(Exception ex){
			ex.printStackTrace();
			throw new RuntimeException("cannot save program or associated entities."); 
		}
		
		
		
		
		return program;
	}

	/**
	 * Ensures that a {@link WorkbenchUser} has corresponding {@link User} in crop schema with appropriate permissions.
	 * @param wbUser user to validate against in crop schema
	 * @return the crop user associated to the given workbench user  
	 */
	private User prepareCropUser(WorkbenchUser workbenchUser){
		User cropUser = userRepo.findByUname(workbenchUser.getUname());

		if(cropUser == null){
			cropUser = userRepo.save(converter.convert(workbenchUser, User.class));
		}

		return cropUser;
	}

	/**
	 * Persist the main entity for a program: {@link WorkbenchProject}
	 * @param name name of the program
	 * @param cropUserId id of the user in the crop database
	 * @return the saved WorkbenchProject entity
	 */
	private WorkbenchProject createProgramAsProject(String name, WorkbenchUser user){
		WorkbenchProject program = new WorkbenchProject();
		
		long time = System.currentTimeMillis();
		WorkbenchCrop crop = new WorkbenchCrop();
		crop.setCropName("wheat");

		program.setProjectName(name);
		program.setProjectUuid(UUID.randomUUID().toString());
		program.setStartDate(new Date(time));
		program.setWorkbenchCrop(crop);
		program.setUserId(user.getUserid());
		program = workbenchProjectRepo.save(program);
		workbenchProjectRepo.flush();

		return program;
	}
	
	private void linkUserToProgram(WorkbenchProject program, WorkbenchUser workbenchUser, User cropUser){
		WorkbenchIbdbUserMap userMap = new WorkbenchIbdbUserMap();
		userMap.setIbdbUserId(cropUser.getUserid());
		userMap.setWorkbenchUserId(workbenchUser.getUserid());
		userMap.setProjectId(program.getProjectId());
		workbenchIbdbUserMapRepo.save(userMap);
		
		WorkbenchProjectUserInfo userInfo = new WorkbenchProjectUserInfo();
		userInfo.setUser(workbenchUser);
		userInfo.setWorkbenchProject(program);
		workbenchProjectUserInfoRepo.save(userInfo);
	}
	
	private void linkRolesToProgram(WorkbenchProject program, WorkbenchUser workbenchUser){
		List<WorkbenchUserRole> userRoles = workbenchUser.getUsersRoles();
		List<WorkbenchProjectUserRole> projectRoles = new ArrayList<>();
		
		for (WorkbenchUserRole role : userRoles) {
			WorkbenchProjectUserRole projectUserRole = new WorkbenchProjectUserRole();
			projectUserRole.setRoleId(role.getId());
			projectUserRole.setUserId(workbenchUser.getUserid());
			projectUserRole.setWorkbenchProject(program);
			
			projectRoles.add(projectUserRole);
		}
		workbenchProjectUserRoleRepo.save(projectRoles);
	}

	@Override
	public WorkbenchProject findByName(String name) {
		
		return workbenchProjectRepo.findByProjectName(name);
	}

}
