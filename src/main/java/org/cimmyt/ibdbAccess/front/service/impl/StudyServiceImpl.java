package org.cimmyt.ibdbAccess.front.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.cimmyt.ibdbAccess.conversion.BeanConverter;
import org.cimmyt.ibdbAccess.core.domain.ListdataProject;
import org.cimmyt.ibdbAccess.core.domain.NdExperiment;
import org.cimmyt.ibdbAccess.core.domain.NdGeolocation;
import org.cimmyt.ibdbAccess.core.domain.Project;
import org.cimmyt.ibdbAccess.core.domain.Projectprop;
import org.cimmyt.ibdbAccess.core.domain.StudyId;
import org.cimmyt.ibdbAccess.core.domain.WorkbenchProject;
import org.cimmyt.ibdbAccess.front.domain.GermplasmListEntry;
import org.cimmyt.ibdbAccess.front.domain.OccurrenceSimple;
import org.cimmyt.ibdbAccess.front.domain.Study;
import org.cimmyt.ibdbAccess.front.domain.StudyEntry;
import org.cimmyt.ibdbAccess.front.domain.StudySimple;
import org.cimmyt.ibdbAccess.front.domain.term.ProjectPropDescriptorTerm;
import org.cimmyt.ibdbAccess.front.exception.StudyNotFoundException;
import org.cimmyt.ibdbAccess.front.manager.ListManager;
import org.cimmyt.ibdbAccess.front.manager.NdExperimentManager;
import org.cimmyt.ibdbAccess.front.manager.PhenotypeManager;
import org.cimmyt.ibdbAccess.front.manager.ProgramManager;
import org.cimmyt.ibdbAccess.front.manager.ProjectManager;
import org.cimmyt.ibdbAccess.front.manager.StockManager;
import org.cimmyt.ibdbAccess.front.service.StudyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 * Implementation of {@link StudyService}
 * @author jarojas
 *
 */
@Service
public class StudyServiceImpl implements StudyService {

	private ListManager listManager;
	private ProjectManager projectManager;
	private NdExperimentManager experimentManager;
	private StockManager stockManager;
	private PhenotypeManager phenotypeManager;
	private ProgramManager programManager;
	private BeanConverter converter;
	
	private static final Logger LOG = LoggerFactory.getLogger(StudyServiceImpl.class);
	private static final String UNKNOWN_CYCLE = "Unknown Cycle";
	private static final String UNKNOWN_PROGRAM = "Unknown Program";
	
	/**Maximum year at which a study is considered historical, <br>
	 * i.e. if <code>studyYear <= historicalBreakpointYear</code>
	 * then the study will go to a historical program */
	@Value("${config.historical.breakpoint}")
	private int historicalBreakpointYear;
	
	@Autowired
	public StudyServiceImpl(ListManager listManager, ProjectManager projectManager,
			NdExperimentManager experimentManager,
			StockManager stockManager,
			PhenotypeManager phenotypeManager,
			ProgramManager programManager,
			BeanConverter converter){
		this.listManager = listManager;
		this.projectManager = projectManager;
		this.experimentManager = experimentManager;
		this.stockManager = stockManager;
		this.phenotypeManager = phenotypeManager;
		this.programManager = programManager;
		this.converter = converter;
	}

	@Transactional(readOnly=false)
	@Override
	public Integer saveStudy(Study study){
		return saveStudy(study, true);
	}

	@Transactional(readOnly=false)
	@Override
	public Integer saveStudy(Study study, boolean savePhenotypes) {
		prepareDestinationForStudy(study);
		
		long time = System.currentTimeMillis();
	    LOG.info("Saving new study, creating study metadata");
		StudyId studyId = projectManager.saveProject(study);

		List<ListdataProject> listEntries = saveStudyEntries(study, studyId, true);
        
        if(study.getOccurrences().size() > 0){
            LOG.info("creating experiments for study");
        	Map<Integer,List<NdExperiment>> experiments =  experimentManager.saveStudyInstances(study, studyId);
	        	
			LOG.info("creating stock");
			stockManager.saveExperimentsInStock(listEntries, experiments);
			
			if(savePhenotypes){
				LOG.info("creating phenotypes(observations)");
				phenotypeManager.savePhenotypes(studyId, study, experiments);
			}
        }
		LOG.info("Saved new study with id: {}, total time: {}", studyId.getMainId(), (System.currentTimeMillis()-time)/1000.00);
		return studyId.getMainId();
	}

	
	private List<ListdataProject> saveStudyEntries(Study study, StudyId studyId, boolean createGermplasmList){
		List<ListdataProject> listEntries = null;
        if(study.getGermplasmEntries().size() > 0){
            LOG.info("creating germplasm lists");
            Integer sourceListId = null;
            if(createGermplasmList){
            	sourceListId = listManager.saveList(study);
            }
            listEntries = listManager.saveStudyList(study, studyId, sourceListId);
        }
        return listEntries;
		
	}

	private void prepareDestinationForStudy(Study study) {
		WorkbenchProject program = prepareProgramForStudy(study);
		Project folder = prepareFolderForStudy(study, program);
		study.setFolder(folder);
	}
	
	private WorkbenchProject prepareProgramForStudy(Study study){
		String programName = study.getExtraMetadata().get(ProjectPropDescriptorTerm.STUDY_PROGRAM.asTerm().getCvtermId());

		if(programName == null){
			programName =  UNKNOWN_PROGRAM;
		}else{
			programName = renameProgramWhenHistorical(programName, extractNewCycle(study));
		}
		 
		
		WorkbenchProject program = programManager.findByName(programName);
		if(program == null){
			program = programManager.saveNewProgram(programName, study.getUserId());
		}
		
		return program;
	}
	
	private String renameProgramWhenHistorical(String programName, String newCycle){
		programName = programName.trim();
		
		if(!newCycle.equals(UNKNOWN_CYCLE)){
			int year = Integer.parseInt(newCycle.substring(0, 4));
			if(year <= historicalBreakpointYear){
				programName = programName.concat(" Historical");
			}
		}
		return programName;
	}

	private Project prepareFolderForStudy(Study study, WorkbenchProject program){
		String newCycle = extractNewCycle(study);
		
		Project folder = projectManager.findFolderByNameInProgram(newCycle, program);
		if(folder == null){
			folder = projectManager.saveFolderInProgram(newCycle, program);
		}
		
		return folder;
	}
	
	private String extractNewCycle(final Study study){
		String newCycle = study.getExtraMetadata().get(ProjectPropDescriptorTerm.STUDY_NEW_CYCLE.asTerm().getCvtermId());
		return newCycle == null ?  UNKNOWN_CYCLE : newCycle.trim();
	}

	@Override
	public Page<StudySimple> findByName(Pageable pageRequest, String name) {
		Page<Project> projectPage = projectManager.findByName(pageRequest, name);
		
		List<StudySimple> studies = createCompleteSudies(projectPage.getContent());
		
		Page<StudySimple> studyPage = new PageImpl<StudySimple>(studies, pageRequest, projectPage.getTotalElements());
		return studyPage;
	}
	
	private List<StudySimple> createCompleteSudies(List<Project> projects){
		List<StudySimple> studies = new ArrayList<>(projects.size());
		
		for(Project project : projects){
			List<Projectprop> props = projectManager.findStudyGeneralProperties(project.getProjectId());
			StudySimple study = converter.convertComposite(props, Projectprop.class, StudySimple.class);
			study.setId(project.getProjectId());
			studies.add(study);
		}
		return studies;
	}

	@Override
	public Page<OccurrenceSimple> findOccurrences(Pageable pageRequest, int studyId){
		StudyId identifier = projectManager.findStudyId(studyId);
		
		if(identifier == null){
			throw new StudyNotFoundException(studyId);
		}
		
		Page<NdGeolocation> geolocations = experimentManager.findByProject(pageRequest, identifier.getEnvironmentProject());
		Page<OccurrenceSimple> occurrencesPage = null;
		if(geolocations != null){
			List<OccurrenceSimple> occurences = converter.convert(geolocations.getContent(), NdGeolocation.class, OccurrenceSimple.class);
			occurrencesPage = new PageImpl<>(occurences, pageRequest, geolocations.getTotalElements());
		}
		
		
		return occurrencesPage;
	}

	@Override
	public List<GermplasmListEntry> findStudyList(int studyId){
		List<GermplasmListEntry> germplasms = null;
		List<ListdataProject> listDataProjects = listManager.findStudyList(studyId);
		if(listDataProjects == null){
			germplasms = Collections.emptyList();
		}else{
			germplasms = converter.convert(listDataProjects, ListdataProject.class, GermplasmListEntry.class);
		}
		return germplasms;
	}

	@Override
	public List<StudyEntry> findDesign(int occurrenceId){
		List<NdExperiment> experiments = experimentManager.findExperimentsByGeolocation(occurrenceId);
		phenotypeManager.loadPhenotypes(experiments);
		
		return converter.convert(experiments, NdExperiment.class, StudyEntry.class);
	}

}
