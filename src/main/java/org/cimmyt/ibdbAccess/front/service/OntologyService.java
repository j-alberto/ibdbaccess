package org.cimmyt.ibdbAccess.front.service;

import java.util.List;

import org.cimmyt.ibdbAccess.front.domain.StudyProperty;

/**
 * Service for working with controlled vocabulary
 * @author jarojas
 *
 */
public interface OntologyService {
	/**
	 * Returns the definition of the requested study properties
	 * @param propertyIds the ids which a definition is needed
	 * @return a list of definitions for the given study properties
	 */
	List<StudyProperty> findStudyPropertyNames (List<Integer> propertyIds);
	
	
}
