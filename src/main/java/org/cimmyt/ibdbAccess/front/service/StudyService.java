package org.cimmyt.ibdbAccess.front.service;

import java.util.List;

import org.cimmyt.ibdbAccess.core.domain.NdGeolocation;
import org.cimmyt.ibdbAccess.front.domain.GermplasmListEntry;
import org.cimmyt.ibdbAccess.front.domain.OccurrenceSimple;
import org.cimmyt.ibdbAccess.front.domain.Study;
import org.cimmyt.ibdbAccess.front.domain.StudyEntry;
import org.cimmyt.ibdbAccess.front.domain.StudySimple;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service for working with {@link Study}
 * @author jarojas
 *
 */
public interface StudyService {
    /**
     * persist a Study into the model. It's equivalent to <code>saveStudy(study, true)</code>
     * @param study new entity to persist.
     * @return the id of the {@link Study} created
     */
	Integer saveStudy(Study study);

    /**
     * persist a Study into the model.
     * @param study new entity to persist.
     * @param savePhenotypes flag to enable/disable the saving of phenotype data in the study
     * @return the id of the {@link Study} created
     */
	Integer saveStudy(Study study, boolean savePhenotypes);

	Page<StudySimple> findByName(Pageable pageRequest, String name);

	/**
	 * finds all {@link OccurrenceSimple occurrences} associated with a study
	 * @param pageRequest 
	 * @param name
	 * @return a Page for the list of occurrences in a given study
	 */
	Page<OccurrenceSimple> findOccurrences(Pageable pageRequest, int studyId);

	/**
	 * Finds the germplasm list associated to a particular study
	 * @param studyId study to retrieve its germplasm list
	 * @return the list of germplasm for this study, or an empty list if it doesn't have a list.
	 */
	List<GermplasmListEntry> findStudyList(int studyId);
	
	/**
	 * Returns the design for a specific occurrence. Phenotype information in also included.
	 * @param occurrenceId the id of one {@link NdGeolocation} representing an occurrence
	 * @return a list with all the entries of a design and plot information by entry, or empty list if there is no design
	 */
	List<StudyEntry> findDesign(int occurrenceId);

}
