package org.cimmyt.ibdbAccess.front.service.impl;

import java.util.List;

import org.cimmyt.ibdbAccess.conversion.BeanConverter;
import org.cimmyt.ibdbAccess.core.domain.Cvterm;
import org.cimmyt.ibdbAccess.front.domain.StudyProperty;
import org.cimmyt.ibdbAccess.front.manager.OntologyManager;
import org.cimmyt.ibdbAccess.front.service.OntologyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * Implementation of {@link OntologyService}
 * @author jarojas
 *
 */
@Service
public class OntologyServiceImpl implements OntologyService {

	private OntologyManager ontologyManager;
	private BeanConverter converter;

	@Autowired
	public OntologyServiceImpl(OntologyManager ontologyManager, BeanConverter converter) {
		super();
		this.ontologyManager = ontologyManager;
		this.converter = converter;
	}

	@Override
	public List<StudyProperty> findStudyPropertyNames(List<Integer> propertyIds) {
		List<Cvterm> studyProperties = ontologyManager.findStudyPropertes(propertyIds);
		return converter.convert(studyProperties, Cvterm.class, StudyProperty.class);
	}
	
	

}
