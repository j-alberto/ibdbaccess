package org.cimmyt.ibdbAccess.conversion;

import org.cimmyt.ibdbAccess.core.domain.Cvterm;
import org.cimmyt.ibdbAccess.front.domain.StudyProperty;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Converts from {@link Cvterm} to {@link StudyProperty}
 * @author jarojas
 *
 */

@Component
public class CvTermToStudyProperyConverter implements Converter<Cvterm, StudyProperty>{

	@Override
	public StudyProperty convert(Cvterm source) {
		StudyProperty target = new StudyProperty();
		target.setId(source.getCvtermId());
		target.setName(source.getName());

		return target;
	}

}
