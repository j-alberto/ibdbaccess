package org.cimmyt.ibdbAccess.conversion;

import org.cimmyt.ibdbAccess.core.domain.ListdataProject;
import org.cimmyt.ibdbAccess.core.domain.Stock;
import org.cimmyt.ibdbAccess.front.domain.term.ProjectPropTerm;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Converts from {@link ListdataProject} to {@link Stock}
 * @author jarojas
 *
 */

@Component
public class StockConverter implements Converter<ListdataProject, Stock>{

	@Override
	public Stock convert(ListdataProject source) {
		byte isObsolete=0;
		Stock target = new Stock(source.getGermplasmId(),
				isObsolete,
				source.getDesignation(),
				ProjectPropTerm.ENTRY_CODE_ASSIGNED.asTerm(),
				source.getEntryCode());
		
		return target;
	}

}
