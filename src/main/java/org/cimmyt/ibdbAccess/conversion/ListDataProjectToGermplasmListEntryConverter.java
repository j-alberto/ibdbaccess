package org.cimmyt.ibdbAccess.conversion;

import org.cimmyt.ibdbAccess.core.domain.ListdataProject;
import org.cimmyt.ibdbAccess.front.domain.GermplasmListEntry;
import org.cimmyt.ibdbAccess.front.domain.ListEntryType;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Converts from {@link ListdataProject} to {@link GermplasmListEntry}
 * @author jarojas
 *
 */
@Component
public class ListDataProjectToGermplasmListEntryConverter implements Converter<ListdataProject, GermplasmListEntry>{

	@Override
	public GermplasmListEntry convert(ListdataProject source) {
		GermplasmListEntry target = new GermplasmListEntry();

		target.setEntryNum(source.getEntryId());
		target.setCrossName(source.getGroupName());
		target.setGid(source.getGermplasmId());
		target.setSelHist(source.getDesignation());
		target.setSource(source.getSeedSource());
		target.setType(ListEntryType.valueOf(source.getCheckType()));

		return target;
	}

}
