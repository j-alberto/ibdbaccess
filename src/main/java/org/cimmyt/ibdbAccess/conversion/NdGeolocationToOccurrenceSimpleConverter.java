package org.cimmyt.ibdbAccess.conversion;

import static org.cimmyt.ibdbAccess.front.domain.term.LocationTerm.EXPT_DESIGN;
import static org.cimmyt.ibdbAccess.front.domain.term.LocationTerm.LOC_ABBREVIATION;
import static org.cimmyt.ibdbAccess.front.domain.term.LocationTerm.LOC_NAME;
import static org.cimmyt.ibdbAccess.front.domain.term.LocationTerm.NREP;
import static org.cimmyt.ibdbAccess.front.domain.term.LocationTerm.Occ_Abbr;
import static org.cimmyt.ibdbAccess.front.domain.term.LocationTerm.Occ_Cycle;
import static org.cimmyt.ibdbAccess.front.domain.term.LocationTerm.Occ_Name;
import static org.cimmyt.ibdbAccess.front.domain.term.LocationTerm.Occ_New_Cycle;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cimmyt.ibdbAccess.core.domain.NdGeolocation;
import org.cimmyt.ibdbAccess.core.domain.NdGeolocationprop;
import org.cimmyt.ibdbAccess.front.domain.OccurrenceSimple;
import org.cimmyt.ibdbAccess.front.domain.term.LocationTerm;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Converts from {@link NdGeolocation} to {@link OccurrenceSimple}
 * @author jarojas
 *
 */

@Component
public class NdGeolocationToOccurrenceSimpleConverter implements Converter<NdGeolocation, OccurrenceSimple>{
        public OccurrenceSimple convert(NdGeolocation source) {
	        
	        OccurrenceSimple target = new OccurrenceSimple();
	        target.setNumber(Integer.valueOf(source.getDescription()));
	        target.setId(source.getNdGeolocationId());

	        if(source.getNdGeolocationprops() != null){
	        	List<NdGeolocationprop> geoProperties = source.getNdGeolocationprops();
		        target.setAbbreviation(
		        		findValueAndRemoveProperty(geoProperties, Occ_Abbr));
	        	
		        target.setDesign(
		        		findValueAndRemoveProperty(geoProperties, EXPT_DESIGN));
		        
		        target.setName(
		        		findValueAndRemoveProperty(geoProperties, Occ_Name));
		        
		        target.setNewCycle(
		        		findValueAndRemoveProperty(geoProperties, Occ_New_Cycle));

		        target.setCycle(
		        		findValueAndRemoveProperty(geoProperties, Occ_Cycle));

		        target.setNumRep(
		        		findValueAndRemoveProperty(geoProperties, NREP));

		        target.setLocationName(
		        		findValueAndRemoveProperty(geoProperties, LOC_NAME));

		        target.setLocationAbbreviation(
		        		findValueAndRemoveProperty(geoProperties, LOC_ABBREVIATION));
		        
		        target.setExtraMetadata(mapRemainingProperties(geoProperties));
	        }
            return target;
	    }
        /**
         * Retrieves the value for a {@link LocationTerm}, and remove such term from the {@link NdGeolocationprop geoProperties} 
         * list passed as argument
         * @param geoProperties where to look up a property
         * @param locationTerm the property to look for
         * @return the value of the passed location property, or null if not found
         */
        private String findValueAndRemoveProperty(List<NdGeolocationprop> geoProperties, LocationTerm locationTerm){
        	NdGeolocationprop propFound = null;
        	for (NdGeolocationprop prop : geoProperties) {
				if(prop.getType().getCvtermId().equals(locationTerm.id())){
					propFound = prop;
					break;
				}
			}
        	if(propFound != null){
        		geoProperties.remove(propFound);
            	return propFound.getValue();
        	}else{
        		return null;
        	}
        }
        
        /**
         * converts the {@link NdGeolocationprop geoProperties} list to a Map, to be used
         * as extra metadata for a {@link OccurrenceSimple}
         * @param geoProperties remaining properties not mapped to a {@link OccurrenceSimple} attribute, yet
         * @return a map representing the passed argument, cannot be null but can be empty 
         */
        private Map<Integer,String> mapRemainingProperties(List<NdGeolocationprop> geoProperties){
        	Map<Integer, String> propertyMap = new HashMap<>(geoProperties.size()*2);
        	for (NdGeolocationprop geoProp : geoProperties) {
				propertyMap.put(geoProp.getType().getCvtermId(), geoProp.getValue());
			}
        	return propertyMap;
        }
}


