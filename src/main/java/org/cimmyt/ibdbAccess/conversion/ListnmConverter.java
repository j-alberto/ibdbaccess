package org.cimmyt.ibdbAccess.conversion;

import org.cimmyt.ibdbAccess.core.domain.Listnm;
import org.cimmyt.ibdbAccess.front.domain.ListType;
import org.cimmyt.ibdbAccess.front.domain.Study;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Converts from {@link Study} to {@link Listnm}
 * @author jarojas
 *
 */

@Component
public class ListnmConverter implements Converter<Study, Listnm>{
        public Listnm convert(Study source) {
	        
	        ListType listType = source.getStudyType().equals("T")? ListType.TRIAL : ListType.NURSERY;
	        int listStatus = 0;
	                
	        Listnm target = new Listnm();
            target.setListname(source.getName().trim()+"_"+source.getTid());
            target.setListdate(source.getStartDate());
            target.setSdate(source.getStartDate());
            target.setEdate(source.getStartDate());
            target.setListtype(listType.toString());
            target.setListuid(source.getUserId());
            target.setListdesc(source.getDescription());
            target.setListstatus(listStatus);
            target.setNotes("automatic generation");
            target.setProgramUuid(source.getFolder().getProgramUuid());

            return target;
	    }
}


