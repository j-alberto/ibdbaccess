package org.cimmyt.ibdbAccess.conversion;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.cimmyt.ibdbAccess.core.domain.User;
import org.cimmyt.ibdbAccess.core.domain.WorkbenchUser;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Converts from {@link WorkbenchUser} to {@link User}
 * @author jarojas
 *
 */

@Component
public class WorkbenchUserToCropUserConverter implements Converter<WorkbenchUser, User>{

	@Override
	public User convert(WorkbenchUser source) {
		
		Calendar calendar = GregorianCalendar.getInstance();
		
		String date = String.format("%s%s%s"
				,calendar.get(Calendar.YEAR)
				,calendar.get(Calendar.MONTH+1)
				,calendar.get(Calendar.DAY_OF_MONTH));
		int numDate = Integer.parseInt(date);
		
		User target = new User();
		target.setAdate(numDate);
		target.setCdate(numDate);
		target.setInstalid(0);
		target.setPersonid(source.getPersonid());
		target.setUaccess(User.DEFAULT_ACCESS_RIGTHS);
		target.setUname(source.getUname());
		target.setUpswd(source.getUpswd());
		target.setUstatus(1);
		target.setUtype(User.TYPE_LOCAL_ADMIN);

		return target;
	}

}
