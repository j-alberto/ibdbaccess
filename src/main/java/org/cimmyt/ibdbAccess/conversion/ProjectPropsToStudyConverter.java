package org.cimmyt.ibdbAccess.conversion;

import static org.cimmyt.ibdbAccess.front.domain.term.ProjectPropTerm.END_DATE;
import static org.cimmyt.ibdbAccess.front.domain.term.ProjectPropTerm.START_DATE;
import static org.cimmyt.ibdbAccess.front.domain.term.ProjectPropTerm.STUDY_NAME_ASSIGNED;
import static org.cimmyt.ibdbAccess.front.domain.term.ProjectPropTerm.STUDY_OBJECTIVE;
import static org.cimmyt.ibdbAccess.front.domain.term.ProjectPropTerm.STUDY_TITLE_ASSIGNED;
import static org.cimmyt.ibdbAccess.front.domain.term.ProjectPropTerm.STUDY_TYPE;
import static org.cimmyt.ibdbAccess.front.domain.term.ProjectPropTerm.STUDY_USER_ID;

import java.util.Collection;

import org.cimmyt.ibdbAccess.core.domain.Projectprop;
import org.cimmyt.ibdbAccess.front.domain.StudySimple;
import org.cimmyt.ibdbAccess.front.domain.term.StudyTypeTerm;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * Converts from a collection of {@link Projectprop} to {@link StudySimple}
 * @author jarojas
 *
 */

@Component
public class ProjectPropsToStudyConverter implements Converter<Collection<Projectprop>, StudySimple>{
        public StudySimple convert(Collection<Projectprop> source) {
        	
        	StudySimple target = new StudySimple();
        	
        	for(Projectprop p : source){
        		if(p.getType().getCvtermId() == START_DATE.id() &&
        				!StringUtils.isEmpty(p.getValue())) {
        			target.setStartDate(Integer.parseInt(p.getValue()));
        			
        		}else if(p.getType().getCvtermId() == END_DATE.id() &&
        				!StringUtils.isEmpty(p.getValue())) {
        			
        			target.setEndDate(Integer.parseInt(p.getValue()));
        			
        		}else if(p.getType().getCvtermId() == STUDY_TYPE.id() &&
        				!StringUtils.isEmpty(p.getValue())) {
        			target.setStudyType(StudyTypeTerm.forId(Integer.parseInt(p.getValue())));

        		}else if(p.getType().getCvtermId() == STUDY_USER_ID.id()) {
        			target.setUser(p.getValue());
        			
        		}else if(p.getType().getCvtermId() == STUDY_NAME_ASSIGNED.id()) {
        			target.setName(p.getValue());
        			
        		}else if(p.getType().getCvtermId() == STUDY_TITLE_ASSIGNED.id()) {
        			target.setDescription(p.getValue());
        			
        		}else if(p.getType().getCvtermId() == STUDY_OBJECTIVE.id()) {
        			target.setObjective(p.getValue());
        			
        		}else{
        			target.getExtras().put(p.getType().getCvtermId(), p.getValue());
        		}
        	}
        	
            return target;
	    }
}


