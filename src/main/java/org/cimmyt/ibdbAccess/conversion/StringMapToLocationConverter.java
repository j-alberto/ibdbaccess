package org.cimmyt.ibdbAccess.conversion;

import java.util.List;
import java.util.Map;

import org.cimmyt.ibdbAccess.core.domain.Country;
import org.cimmyt.ibdbAccess.core.domain.Location;
import org.cimmyt.ibdbAccess.core.repository.CountryRepository;
import org.cimmyt.ibdbAccess.core.repository.LocationRepository;
import org.cimmyt.ibdbAccess.front.domain.term.LocationTerm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Converts from Map<Integer, String> to {@link Location}.
 * This conversion is due to lack of ids in iwis to uniquely identify a location.
 * To identify a location two values are needed: country code and location abbreviation
 * @author jarojas
 *
 */


@Component
public class StringMapToLocationConverter implements Converter<Map<Integer, String>, Location>{

	@Autowired
	LocationRepository locationRepo;
	@Autowired
	CountryRepository countryRepo;

	public Location convert(Map<Integer, String> source) {
	        
        	String countryIso3Code = source.get(LocationTerm.OCC_COUNTRY.id());
        	String locationAbbreviation = source.get(LocationTerm.LOC_ABBREVIATION.id());
        	
        	List<Country> countries = countryRepo.findByIsothreeAndCchange(countryIso3Code,0);
        	
        	int countryId = 0;
        	if(countries != null && countries.size() > 0){
        		countryId = countries.get(0).getCntryid();
        	}

            return locationRepo.findByLabbrAndCntryid(locationAbbreviation, countryId);
	    }
}


