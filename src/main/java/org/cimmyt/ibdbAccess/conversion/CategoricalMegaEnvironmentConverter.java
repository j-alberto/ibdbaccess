package org.cimmyt.ibdbAccess.conversion;

import org.cimmyt.ibdbAccess.front.domain.CategoricalMegaEnvironment;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Converts from a text describing a location to a text representing the Id for such location
 * @author jarojas
 *
 */

@Component
public class CategoricalMegaEnvironmentConverter implements Converter<String, CategoricalMegaEnvironment>{
        public CategoricalMegaEnvironment convert(String source) {
	        //TODO: megaenvironment mappings here
	      return new CategoricalMegaEnvironment("-1");
	    }
}


