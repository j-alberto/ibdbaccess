package org.cimmyt.ibdbAccess.conversion;

import org.cimmyt.ibdbAccess.core.domain.ListdataProject;
import org.cimmyt.ibdbAccess.front.domain.GermplasmListEntry;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Converts from {@link GermplasmListEntry} to {@link ListdataProject}
 * @author jarojas
 *
 */
@Component
public class ListDataProjectConverter implements Converter<GermplasmListEntry, ListdataProject>{

	@Override
	public ListdataProject convert(GermplasmListEntry source) {
		ListdataProject target = new ListdataProject();

		target.setCheckType(source.getType().value());
		target.setDesignation(source.getSelHist() == null ? "undefined" : source.getSelHist());
		target.setEntryCode(source.getEntryNum().toString());
		target.setEntryId(source.getEntryNum());
		target.setGermplasmId(source.getGid() == null ? 0 : source.getGid());
		target.setGroupName(source.getCrossName() == null ? "undefined" : source.getCrossName());
		target.setSeedSource(source.getSource() == null ? "undefined" : source.getSource());

		return target;
	}

}
