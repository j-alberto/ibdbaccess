package org.cimmyt.ibdbAccess.conversion;

import java.util.List;

import org.cimmyt.ibdbAccess.core.domain.NdExperiment;
import org.cimmyt.ibdbAccess.core.domain.NdExperimentprop;
import org.cimmyt.ibdbAccess.core.domain.Phenotype;
import org.cimmyt.ibdbAccess.front.domain.StudyEntry;
import org.cimmyt.ibdbAccess.front.domain.term.ProjectPropTerm;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Converts from {@link NdExperiment} to {@link StudyEntry}
 * @author jarojas
 *
 */

@Component
public class NdExperimentToStudyEntryConverter implements Converter<NdExperiment, StudyEntry>{
        public StudyEntry convert(NdExperiment source) {
	        
	        StudyEntry target = new StudyEntry();
	        
	        String plot = getPropertyValueForType(source.getExperimentprops(), ProjectPropTerm.PLOT_NUMBER);
	        if(plot != null){
	        	target.setPlot(Integer.valueOf(plot));
	        }
	        
	        String replicate = getPropertyValueForType(source.getExperimentprops(), ProjectPropTerm.REP_NUMBER);
	        if(replicate != null){
	        	target.setRep(Integer.valueOf(replicate));
	        }

            //target.setEntryNum(entryNum); must come from stock.uniquename
	        for (Phenotype phenotype: source.getPhenotypes()) {
				target.addTraitValue(Integer.valueOf(phenotype.getName()), phenotype.getValue());
			}
            return target;
	    }
        
        
        private String getPropertyValueForType(List<NdExperimentprop> properties, ProjectPropTerm type){
        	for (NdExperimentprop ndExperimentprop : properties) {
				if(ndExperimentprop.getCvterm().getCvtermId().equals(type.id())){
					return ndExperimentprop.getValue();
				}
			}
        	return null;
        }
}


