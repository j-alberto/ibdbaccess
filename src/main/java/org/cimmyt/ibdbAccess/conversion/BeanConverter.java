package org.cimmyt.ibdbAccess.conversion;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cimmyt.ibdbAccess.front.domain.Categorical;
import org.cimmyt.ibdbAccess.front.domain.term.GeolocationPropCategoricalTerm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

/**
 * A wrapper for ConversionService bean, with simplified methods for converting Collections of objects.
 * @author jarojas
 *
 */
@Component
public class BeanConverter {

	@Autowired
	private ConversionService conversionService;
	
	public <T> T convert(Object source, Class<T> targetType){
		return conversionService.convert(source, targetType);
	}
	
	
    @SuppressWarnings("unchecked")
	public <S,T> List<T> convert(List<S> sourceList, Class<S> sourceType, Class<T> targetType) {
	    return (List<T>) conversionService.convert(
	            sourceList,
	            TypeDescriptor.collection(List.class,
	                    TypeDescriptor.valueOf(sourceType)),
	            TypeDescriptor.collection(List.class,
	                    TypeDescriptor.valueOf(targetType)));
    }
    
    @SuppressWarnings("unchecked")
	public <S,T> List<T> convert(Collection<S> sourceList, Class<S> sourceType, Class<T> targetType) {
	    return (List<T>) conversionService.convert(
	            sourceList,
	            TypeDescriptor.collection(Collection.class,
	                    TypeDescriptor.valueOf(sourceType)),
	            TypeDescriptor.collection(List.class,
	                    TypeDescriptor.valueOf(targetType)));
    }
    
    @SuppressWarnings("unchecked")
    public <S,T> Set<T> convert(Set<S> sourceList, Class<S> sourceType, Class<T> targetType) {
        return (Set<T>) conversionService.convert(
                sourceList,
                TypeDescriptor.collection(Set.class,
                        TypeDescriptor.valueOf(sourceType)),
                TypeDescriptor.collection(Set.class,
                        TypeDescriptor.valueOf(targetType)));
    }
    
    public Categorical convert(Object value, GeolocationPropCategoricalTerm geolocTerm){
    	return conversionService.convert(value, geolocTerm.getCategoricalClass());
    }
    
    @SuppressWarnings("unchecked")
	public <S,T> T convertComposite(Collection<S> sourceList, Class<S> sourceType, Class<T> targetType) {
	    return (T) conversionService.convert(
	            sourceList,
	            TypeDescriptor.collection(Collection.class,
	                    TypeDescriptor.valueOf(sourceType)),
	            TypeDescriptor.valueOf(targetType));
    }

    @SuppressWarnings("unchecked")
	public <K,V,T> T convertComposite(Map<K,V> sourceList, Class<K> keySourceType, Class<V> sourceType, Class<T> targetType) {
    	
	    return (T) conversionService.convert(
	            sourceList,
	            TypeDescriptor.map(Map.class
	            		, TypeDescriptor.valueOf(keySourceType)
	                    ,TypeDescriptor.valueOf(sourceType))
	            ,TypeDescriptor.valueOf(targetType));
    }

}
