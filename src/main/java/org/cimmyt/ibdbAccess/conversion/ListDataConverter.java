package org.cimmyt.ibdbAccess.conversion;

import org.cimmyt.ibdbAccess.core.domain.Listdata;
import org.cimmyt.ibdbAccess.front.domain.GermplasmListEntry;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Converts from {@link GermplasmListEntry} to {@link Listdata}
 * @author jarojas
 *
 */
@Component
public class ListDataConverter implements Converter<GermplasmListEntry, Listdata>{

	@Override
	public Listdata convert(GermplasmListEntry source) {
		Listdata target = new Listdata();
		int defaultStatus = 0;

		target.setEntryid(source.getEntryNum());
		target.setEntrycd(source.getEntryNum().toString());
		target.setDesig(source.getSelHist() == null ? "undefined": source.getSelHist());
		target.setGid(source.getGid() == null ? 0: source.getGid());
		target.setGrpname(source.getCrossName() == null ? "undefined":  source.getCrossName());
		target.setLrstatus(defaultStatus);
		target.setSource(source.getSource() == null ? "undefined": source.getSource());
		
		return target;
	}

}
