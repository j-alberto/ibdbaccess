
(function(){
	var app = angular.module('search-study',['ngAnimate']);
	
	app.controller('searchStudyController',['$http',function($http,$scope){
		this.search = {};
		controller = this;
		this.studies = [];
	}]);

	app.directive('searchStudy',function(){
		return {
			restrict: "E",
			templateUrl: "templates/searchStudies.html",
			controller: function($http,$scope){
				controller = this;
				this.studies = [];
				this.search = {};
				this.studyProps = {};
				this.currentPages = [];
				this.pages = [];
				
				
				this.doSearch = function(page){
					if(lastSearch != this.search.text){
						this.pages = [];
					}
					lastSearch = this.search.text;

					if(controller.pages[page]){
						console.log('already have page: '+page);
						controller.page = controller.pages[page];
						controller.currentPages = calculatePagination(controller.page,12);
					}else{
						$http.get('http://localhost:8080/siuapi/study',
								{params:{'name':this.search.text, 'page':page, 'size':20}})
						.success(function(data){
							console.log('calling page: '+page);
							controller.page = data;
							controller.pages[page] = data;
							controller.currentPages = calculatePagination(controller.page,12);
						})
						.error(function(error){
							controller.page = {};
						});
					}
//					controller.page = mockRequest;
//					console.log(pagesToShow(mockRequest));
//					controller.currentPages = showPages(mockRequest);
				};

				/**
				 * Fills in catalog of terms
				 */
				this.seeSettings = function(studyExtras){
					var newProps = [];
					for(key in studyExtras){
						if(controller.studyProps[key] == undefined){
							newProps[newProps.length] = key;
						}
					}
					
					if(newProps.length > 0){
						$http.get('http://localhost:8080/siuapi/onto/studyTerms', {params:{'ids':newProps}})
						.success(function(data){
							data.forEach(function(term){
								controller.studyProps[term.id] = term.name;
							});
						})
						.error(function(error){
							console.log('SWW2!');
						});
					}
					
					//move tab
					var scope = angular.element($(event.target)).scope();
					scope.tab = 2;
				};
				this.seeGeneral = function(){
					var scope = angular.element($(event.target)).scope();
					scope.tab = 1;
				};
				this.seeOccurrences = function(studyId){
					var scope = angular.element($(event.target)).scope();
					scope.tab = 3;

					$http.get('http://localhost:8080/siuapi/study/'+studyId+'/occurrence')
					.success(function(data){
						console.log('calling occ page: '+data.content);
						controller.occPage = data;
					})
					.error(function(error){
						controller.occPage = {};
					});

				};
			
			},
			controllerAs: 'searchCtrl'
			
		};
	});

	var mockRequest = {"content":[{"id":24715,"name":"LINEAS PRECOSES HELMINTH._60942","objective":null,"startDate":19990101,"endDate":null,"description":"LINEAS PRECOSES HELMINTH.","studyType":"TRIAL","folder":null,"tid":null,"user":"3",
		"extras":{"23264":"WIDE CROSSES             ","22611":"V-00 ","22612":"9","22613":"BREAD WHEAT                                       ","22614":"2000A","8680":"LINPREHE","22651":"1156","8060":"","8030":"","28351":"60942"}},{"id":24718,"name":"LINEAS PRECOSES FUSARIUM_60941","objective":null,"startDate":19990101,"endDate":null,"description":"LINEAS PRECOSES FUSARIUM","studyType":"TRIAL","folder":null,"tid":null,"user":"3","extras":{"23264":"WIDE CROSSES             ","22611":"V-00 ","22612":"5","22613":"BREAD WHEAT                                       ","22614":"2000A","8680":"LINPREFU","22651":"1151","8060":"","8030":"","28351":"60941"}},{"id":24721,"name":"LINEAS PRECOSES SEPTORIA_60940","objective":null,"startDate":19990101,"endDate":null,"description":"LINEAS PRECOSES SEPTORIA","studyType":"TRIAL","folder":null,"tid":null,"user":"3","extras":{"23264":"WIDE CROSSES             ","22611":"V-00 ","22612":"10","22613":"BREAD WHEAT                                       ","22614":"2000A","8680":"LINPRESE","22651":"1141","8060":"","8030":"","28351":"60940"}},{"id":24724,"name":"LINEAS PRECOSES KB_60939","objective":null,"startDate":19990101,"endDate":null,"description":"LINEAS PRECOSES KB","studyType":"TRIAL","folder":null,"tid":null,"user":"3","extras":{"23264":"WIDE CROSSES             ","22611":"V-00 ","22612":"6","22613":"BREAD WHEAT                                       ","22614":"2000A","8680":"LINPREKB","22651":"1135","8060":"","8030":"","28351":"60939"}},{"id":24841,"name":"LINEAS PRECOSES HELMINTH._60896","objective":null,"startDate":19990101,"endDate":null,"description":"LINEAS PRECOSES HELMINTH.","studyType":"TRIAL","folder":null,"tid":null,"user":"3","extras":{"23264":"WIDE CROSSES             ","22611":"99-00","22612":"13","22613":"BREAD WHEAT                                       ","22614":"1999B","8680":"LINPREHE","22651":"2421","8060":"","8030":"","28351":"60896"}},{"id":24844,"name":"LINEAS PRECOSES FUSARIUM_60895","objective":null,"startDate":19990101,"endDate":null,"description":"LINEAS PRECOSES FUSARIUM","studyType":"TRIAL","folder":null,"tid":null,"user":"3","extras":{"23264":"WIDE CROSSES             ","22611":"99-00","22612":"7","22613":"BREAD WHEAT                                       ","22614":"1999B","8680":"LINPREFU","22651":"2414","8060":"","8030":"","28351":"60895"}},{"id":24847,"name":"LINEAS PRECOSES SEPTORIA_60894","objective":null,"startDate":19990101,"endDate":null,"description":"LINEAS PRECOSES SEPTORIA","studyType":"TRIAL","folder":null,"tid":null,"user":"3","extras":{"23264":"WIDE CROSSES             ","22611":"99-00","22612":"12","22613":"BREAD WHEAT                                       ","22614":"1999B","8680":"LINPRESE","22651":"2402","8060":"","8030":"","28351":"60894"}},{"id":24850,"name":"LINEAS PRECOSES KB_60893","objective":null,"startDate":19990101,"endDate":null,"description":"LINEAS PRECOSES KB","studyType":"TRIAL","folder":null,"tid":null,"user":"3","extras":{"23264":"WIDE CROSSES             ","22611":"99-00","22612":"10","22613":"BREAD WHEAT                                       ","22614":"1999B","8680":"LINPREKB","22651":"2392","8060":"","8030":"","28351":"60893"}},{"id":24958,"name":"LINEAS PRECOSES KB_60857","objective":null,"startDate":19990101,"endDate":null,"description":"LINEAS PRECOSES KB","studyType":"TRIAL","folder":null,"tid":null,"user":"3","extras":{"23264":"WIDE CROSSES             ","22611":"-99  ","22612":"17","22613":"BREAD WHEAT                                       ","22614":"1999A","8680":"LINPREKB","22651":"2474","8060":"","8030":"","28351":"60857"}},{"id":24961,"name":"LINEAS PRECOSES SEPTORIA_60856","objective":null,"startDate":19990101,"endDate":null,"description":"LINEAS PRECOSES SEPTORIA","studyType":"TRIAL","folder":null,"tid":null,"user":"3","extras":{"23264":"WIDE CROSSES             ","22611":"-99  ","22612":"17","22613":"BREAD WHEAT                                       ","22614":"1999A","8680":"LINPRESE","22651":"2491","8060":"","8030":"","28351":"60856"}},{"id":24964,"name":"LINEAS PRECOSES FUSARIUM_60855","objective":null,"startDate":19990101,"endDate":null,"description":"LINEAS PRECOSES FUSARIUM","studyType":"TRIAL","folder":null,"tid":null,"user":"3","extras":{"23264":"WIDE CROSSES             ","22611":"-99  ","22612":"12","22613":"BREAD WHEAT                                       ","22614":"1999A","8680":"LINPREFU","22651":"2508","8060":"","8030":"","28351":"60855"}},{"id":24967,"name":"LINEAS PRECOSES HELMINTH._60854","objective":null,"startDate":19990101,"endDate":null,"description":"LINEAS PRECOSES HELMINTH.","studyType":"TRIAL","folder":null,"tid":null,"user":"3","extras":{"23264":"WIDE CROSSES             ","22611":"-99  ","22612":"18","22613":"BREAD WHEAT                                       ","22614":"1999A","8680":"LINPREHE","22651":"2520","8060":"","8030":"","28351":"60854"}}]
		,"totalElements":12,"totalPages":15,"last":true,"numberOfElements":12,"sort":null,"first":true,"size":50,"number":0};
	
	var lastSearch;
	
	function calculatePagination(data, numPages){
		numPages = numPages - numPages%2;
		var surroundingPages = numPages/2;
		var pages = [];
		
		if(data.totalPages > numPages){
			if(data.number > surroundingPages){
				if(data.number + surroundingPages >= data.totalPages){
					pages = makeArray( data.totalPages-numPages, data.totalPages);
				}else{
					pages = makeArray(data.number-(surroundingPages-1),data.number+(surroundingPages+1));
				}
			}else{
				pages = makeArray(1,numPages+1);
			}
		}else{
			pages = makeArray(1,data.totalPages);   
		}
		return pages;
	}

	function makeArray(start, ends){
		var a =[];
		while(start < ends+1) a.push(start++);
		return a;
	}
	
	
		
})();


