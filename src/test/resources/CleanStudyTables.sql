use `ibdbv2_wheat_merged`;


DROP TABLE IF EXISTS `listdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `listdata` (
  `listid` int(11) NOT NULL DEFAULT '0',
  `gid` int(11) NOT NULL DEFAULT '0',
  `entryid` int(11) NOT NULL DEFAULT '0',
  `entrycd` varchar(47) NOT NULL DEFAULT '-',
  `source` varchar(255) NOT NULL DEFAULT '-',
  `desig` varchar(255) NOT NULL DEFAULT '-',
  `grpname` varchar(255) NOT NULL DEFAULT '-',
  `lrecid` int(11) NOT NULL DEFAULT '0',
  `lrstatus` int(11) NOT NULL DEFAULT '0',
  `llrecid` int(11) DEFAULT '0',
  PRIMARY KEY (`listid`,`lrecid`),
  KEY `listdata_idx02` (`entrycd`),
  KEY `listdata_idx03` (`gid`),
  KEY `listdata_idx04` (`source`),
  KEY `listdata_idx05` (`listid`,`gid`,`lrstatus`),
  KEY `listdata_idx06` (`listid`,`entryid`,`lrstatus`),
  KEY `listdata_idx07` (`listid`),
  KEY `listdata_idx08` (`listid`,`lrecid`),
  KEY `index_desig` (`desig`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `listdata_project`
--
DROP TABLE IF EXISTS `listdata_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `listdata_project` (
  `listdata_project_id` int(11) NOT NULL DEFAULT '0',
  `list_id` int(11) NOT NULL DEFAULT '0',
  `germplasm_id` int(11) NOT NULL DEFAULT '0',
  `check_type` int(11) NOT NULL DEFAULT '0',
  `entry_id` int(11) NOT NULL DEFAULT '0',
  `entry_code` varchar(47) NOT NULL DEFAULT '-',
  `seed_source` varchar(255) NOT NULL DEFAULT '-',
  `designation` varchar(255) NOT NULL DEFAULT '-',
  `group_name` varchar(255) NOT NULL DEFAULT '-',
  `duplicate_notes` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`listdata_project_id`,`list_id`),
  KEY `listdata_project_idx02` (`entry_code`),
  KEY `listdata_project_idx03` (`germplasm_id`),
  KEY `listdata_project_idx04` (`seed_source`),
  KEY `listdata_project_idx05` (`list_id`,`germplasm_id`),
  KEY `listdata_project_idx06` (`list_id`,`entry_id`),
  KEY `listdata_project_idx07` (`list_id`),
  KEY `index_desig` (`designation`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `listdataprops`
--

DROP TABLE IF EXISTS `listdataprops`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `listdataprops` (
  `listdataprop_id` int(11) NOT NULL AUTO_INCREMENT,
  `listdata_id` int(11) NOT NULL DEFAULT '0',
  `column_name` varchar(50) NOT NULL DEFAULT '-',
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`listdataprop_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `listnms`
--

DROP TABLE IF EXISTS `listnms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `listnms` (
  `listid` int(11) NOT NULL DEFAULT '0',
  `listname` varchar(50) NOT NULL DEFAULT '-',
  `listdate` int(11) NOT NULL DEFAULT '0',
  `listtype` varchar(10) NOT NULL DEFAULT 'LST',
  `listuid` int(11) NOT NULL DEFAULT '0',
  `listdesc` varchar(255) NOT NULL DEFAULT '-',
  `lhierarchy` int(11) DEFAULT '0',
  `liststatus` int(11) DEFAULT '1',
  `sdate` int(11) DEFAULT NULL,
  `edate` int(11) DEFAULT NULL,
  `listlocn` int(11) DEFAULT NULL,
  `listref` int(11) DEFAULT NULL,
  `projectid` int(11) DEFAULT '0',
  `notes` text,
  PRIMARY KEY (`listid`),
  KEY `listnms_idx01` (`listid`,`lhierarchy`),
  KEY `listnms_idx02` (`listid`),
  KEY `index_liststatus` (`liststatus`),
  KEY `index_listname` (`listname`),
  FULLTEXT KEY `listname` (`listname`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `nd_geolocationprop`;
DROP TABLE IF EXISTS `nd_experiment_phenotype`;
DROP TABLE IF EXISTS `nd_experiment_stock`;
DROP TABLE IF EXISTS `stockprop`;
DROP TABLE IF EXISTS `stock`;
DROP TABLE IF EXISTS `projectprop`;
DROP TABLE IF EXISTS `nd_experiment_project`;
DROP TABLE IF EXISTS `nd_experimentprop`;
DROP TABLE IF EXISTS `project_relationship`;
DROP TABLE IF EXISTS `phenotype`;
DROP TABLE IF EXISTS `nd_experiment`;
DROP TABLE IF EXISTS `nd_geolocation`;
DROP TABLE IF EXISTS `project`;

--
-- Table structure for table `nd_geolocation`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nd_geolocation` (
  `nd_geolocation_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `geodetic_datum` varchar(32) DEFAULT NULL,
  `altitude` float DEFAULT NULL,
  PRIMARY KEY (`nd_geolocation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nd_geolocationprop`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nd_geolocationprop` (
  `nd_geolocationprop_id` int(11) NOT NULL AUTO_INCREMENT,
  `nd_geolocation_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `rank` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`nd_geolocationprop_id`),
  UNIQUE KEY `nd_geolocationprop_idx1` (`nd_geolocation_id`,`type_id`,`rank`),
  KEY `nd_geolocationprop_idx2` (`type_id`),
  CONSTRAINT `nd_geolocationprop_fk1` FOREIGN KEY (`nd_geolocation_id`) REFERENCES `nd_geolocation` (`nd_geolocation_id`) ON DELETE CASCADE,
  CONSTRAINT `nd_geolocationprop_fk2` FOREIGN KEY (`type_id`) REFERENCES `cvterm` (`cvterm_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1121 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nd_experiment` (
  `nd_experiment_id` int(11) NOT NULL AUTO_INCREMENT,
  `nd_geolocation_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  PRIMARY KEY (`nd_experiment_id`),
  KEY `nd_experiment_idx1` (`nd_geolocation_id`),
  KEY `nd_experiment_idx2` (`type_id`),
  CONSTRAINT `nd_experiment_fk1` FOREIGN KEY (`nd_geolocation_id`) REFERENCES `nd_geolocation` (`nd_geolocation_id`) ON DELETE CASCADE,
  CONSTRAINT `nd_experiment_fk2` FOREIGN KEY (`type_id`) REFERENCES `cvterm` (`cvterm_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6295 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `phenotype`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phenotype` (
  `phenotype_id` int(11) NOT NULL AUTO_INCREMENT,
  `uniquename` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `observable_id` int(11) DEFAULT NULL,
  `attr_id` int(11) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `cvalue_id` int(11) DEFAULT NULL,
  `assay_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`phenotype_id`),
  UNIQUE KEY `phenotype_idx1` (`uniquename`),
  KEY `phenotype_idx2` (`assay_id`),
  KEY `phenotype_idx3` (`cvalue_id`),
  KEY `phenotype_idx4` (`observable_id`),
  KEY `phenotype_idx5` (`attr_id`),
  CONSTRAINT `phenotype_fk1` FOREIGN KEY (`observable_id`) REFERENCES `cvterm` (`cvterm_id`) ON DELETE CASCADE,
  CONSTRAINT `phenotype_fk2` FOREIGN KEY (`attr_id`) REFERENCES `cvterm` (`cvterm_id`) ON DELETE SET NULL,
  CONSTRAINT `phenotype_fk3` FOREIGN KEY (`cvalue_id`) REFERENCES `cvterm` (`cvterm_id`) ON DELETE SET NULL,
  CONSTRAINT `phenotype_fk4` FOREIGN KEY (`assay_id`) REFERENCES `cvterm` (`cvterm_id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=6589 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;



--
-- Table structure for table `nd_experiment_phenotype`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nd_experiment_phenotype` (
  `nd_experiment_phenotype_id` int(11) NOT NULL AUTO_INCREMENT,
  `nd_experiment_id` int(11) NOT NULL,
  `phenotype_id` int(11) NOT NULL,
  PRIMARY KEY (`nd_experiment_phenotype_id`),
  UNIQUE KEY `nd_experiment_phenotype_idx1` (`nd_experiment_id`,`phenotype_id`),
  KEY `nd_experiment_phenotype_idx2` (`phenotype_id`),
  CONSTRAINT `nd_experiment_phenotype_fk1` FOREIGN KEY (`nd_experiment_id`) REFERENCES `nd_experiment` (`nd_experiment_id`) ON DELETE CASCADE,
  CONSTRAINT `nd_experiment_phenotype_fk2` FOREIGN KEY (`phenotype_id`) REFERENCES `phenotype` (`phenotype_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6589 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `program_uuid` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`project_id`),
  UNIQUE KEY `project_idx1` (`name`,`program_uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=2271 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `nd_experiment_project`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nd_experiment_project` (
  `nd_experiment_project_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `nd_experiment_id` int(11) NOT NULL,
  PRIMARY KEY (`nd_experiment_project_id`),
  KEY `nd_experiment_project_idx1` (`project_id`),
  KEY `nd_experiment_project_idx2` (`nd_experiment_id`),
  CONSTRAINT `nd_experiment_project_fk1` FOREIGN KEY (`project_id`) REFERENCES `project` (`project_id`) ON DELETE CASCADE,
  CONSTRAINT `nd_experiment_project_fk2` FOREIGN KEY (`nd_experiment_id`) REFERENCES `nd_experiment` (`nd_experiment_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6295 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nd_experimentprop`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nd_experimentprop` (
  `nd_experimentprop_id` int(11) NOT NULL AUTO_INCREMENT,
  `nd_experiment_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `rank` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`nd_experimentprop_id`),
  UNIQUE KEY `nd_experimentprop_idx1` (`nd_experiment_id`,`type_id`,`rank`),
  KEY `nd_experimentprop_idx2` (`type_id`),
  CONSTRAINT `nd_experimentprop_fk1` FOREIGN KEY (`nd_experiment_id`) REFERENCES `nd_experiment` (`nd_experiment_id`) ON DELETE CASCADE,
  CONSTRAINT `nd_experimentprop_fk2` FOREIGN KEY (`type_id`) REFERENCES `cvterm` (`cvterm_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10355 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `project_relationship`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_relationship` (
  `project_relationship_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_project_id` int(11) NOT NULL,
  `object_project_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  PRIMARY KEY (`project_relationship_id`),
  UNIQUE KEY `project_relationship_idx1` (`subject_project_id`,`object_project_id`,`type_id`),
  KEY `project_relationship_idx2` (`object_project_id`),
  KEY `project_relationship_idx3` (`type_id`),
  CONSTRAINT `project_relationship_fk1` FOREIGN KEY (`subject_project_id`) REFERENCES `project` (`project_id`) ON DELETE CASCADE,
  CONSTRAINT `project_relationship_fk2` FOREIGN KEY (`object_project_id`) REFERENCES `project` (`project_id`) ON DELETE CASCADE,
  CONSTRAINT `project_relationship_fk3` FOREIGN KEY (`type_id`) REFERENCES `cvterm` (`cvterm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2288 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `projectprop`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projectprop` (
  `projectprop_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `rank` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`projectprop_id`),
  UNIQUE KEY `projectprop_idx1` (`project_id`,`type_id`,`rank`),
  KEY `projectprop_idx2` (`type_id`),
  CONSTRAINT `projectprop_fk1` FOREIGN KEY (`project_id`) REFERENCES `project` (`project_id`) ON DELETE CASCADE,
  CONSTRAINT `projectprop_fk2` FOREIGN KEY (`type_id`) REFERENCES `cvterm` (`cvterm_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=165846 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stock` (
  `stock_id` int(11) NOT NULL AUTO_INCREMENT,
  `dbxref_id` int(11) DEFAULT NULL,
  `organism_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `uniquename` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `is_obsolete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`stock_id`),
  UNIQUE KEY `stock_idx1` (`organism_id`,`uniquename`,`type_id`),
  KEY `stock_idx2` (`name`),
  KEY `stock_idx3` (`dbxref_id`),
  KEY `stock_idx4` (`organism_id`),
  KEY `stock_idx5` (`type_id`),
  KEY `stock_idx6` (`uniquename`),
  CONSTRAINT `stock_fk3` FOREIGN KEY (`type_id`) REFERENCES `cvterm` (`cvterm_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2357 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

CREATE TABLE `stockprop` (
  `stockprop_id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `rank` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`stockprop_id`),
  UNIQUE KEY `stockprop_idx1` (`stock_id`,`type_id`,`rank`),
  KEY `stockprop_idx2` (`stock_id`),
  KEY `stockprop_idx3` (`type_id`),
  CONSTRAINT `stockprop_fk1` FOREIGN KEY (`stock_id`) REFERENCES `stock` (`stock_id`) ON DELETE CASCADE,
  CONSTRAINT `stockprop_fk2` FOREIGN KEY (`type_id`) REFERENCES `cvterm` (`cvterm_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4909 DEFAULT CHARSET=utf8;


CREATE TABLE `nd_experiment_stock` (
  `nd_experiment_stock_id` int(11) NOT NULL AUTO_INCREMENT,
  `nd_experiment_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  PRIMARY KEY (`nd_experiment_stock_id`),
  KEY `nd_experiment_stock_idx1` (`nd_experiment_id`),
  KEY `nd_experiment_stock_idx2` (`stock_id`),
  KEY `nd_experiment_stock_idx3` (`type_id`),
  CONSTRAINT `nd_experiment_stock_fk1` FOREIGN KEY (`nd_experiment_id`) REFERENCES `nd_experiment` (`nd_experiment_id`) ON DELETE CASCADE,
  CONSTRAINT `nd_experiment_stock_fk2` FOREIGN KEY (`stock_id`) REFERENCES `stock` (`stock_id`) ON DELETE CASCADE,
  CONSTRAINT `nd_experiment_stock_fk3` FOREIGN KEY (`type_id`) REFERENCES `cvterm` (`cvterm_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6178 DEFAULT CHARSET=utf8;
/*
CREATE TABLE `users` (
  `userid` int(11) NOT NULL DEFAULT '0',
  `instalid` int(11) NOT NULL DEFAULT '0',
  `ustatus` int(11) NOT NULL DEFAULT '0',
  `uaccess` int(11) NOT NULL DEFAULT '0',
  `utype` int(11) NOT NULL DEFAULT '0',
  `uname` varchar(30) NOT NULL DEFAULT '-',
  `upswd` varchar(30) NOT NULL DEFAULT '-',
  `personid` int(11) NOT NULL DEFAULT '0',
  `adate` int(11) NOT NULL DEFAULT '0',
  `cdate` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userid`),
  KEY `users_idx01` (`instalid`),
  KEY `users_idx02` (`personid`),
  KEY `users_idx03` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `users_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `role` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `users_roles_idx01` (`userid`),
  CONSTRAINT `fk_users` FOREIGN KEY (`userid`) REFERENCES `users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

*/

INSERT INTO `ibdbv2_wheat_merged`.`project` (`project_id`, `name`, `description`) VALUES ('1', 'STUDIES', 'Root study folder');
