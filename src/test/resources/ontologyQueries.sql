select * from workbench.users;
/*
select * from cv where cv_id = 1040;
select * from cvterm where name like '%coopera%'; -- 8100(pi_name) ,38525 (pi_name2), 8373(cooperator)

select * from cvtermprop where cvterm_id in (38523,38524,8100,8110,8372,8373);
select * from cvterm where cvterm_id in (38523,38524,8100,8110,8372,8373);
*/

select * from workbench.users;

set @termId := 8110;
set @termId2 := 38523;
set @termId3 := 38524;
set @termId4 := 8100;
set @termId5 := 8372;
set @termId6 := 8373;

select * from cvterm_relationship where object_id in(@termId, @termId2, @termId3, @termId4, @termId5, @termId6)
or subject_id in(@termId, @termId2, @termId3, @termId4, @termId5, @termId6)

select r.subject_id
	,s.name
	,r.type_id
	,t.name
	,r.object_id
	,o.name
from cvterm_relationship r
inner join cvterm t on
	r.type_id = t.cvterm_id
inner join cvterm s on
	r.subject_id = s.cvterm_id
inner join cvterm o on
	r.object_id = o.cvterm_id
where object_id in(@termId, @termId2, @termId3, @termId4, @termId5, @termId6)
  or subject_id in(@termId, @termId2, @termId3, @termId4, @termId5, @termId6)

select * from cvterm t
where cvterm_id in(1131,1901,1902,2080,38523,38524,8100,8110,8372,8373);

/* --  ---------------------- addd values
INSERT INTO `ibdbv2_wheat_merged`.`cvterm` (`cv_id`, `name`, `definition`, `is_obsolete`, `is_relationshiptype`)
	VALUES ('1040', 'PI_NAME2', 'Secondary Investigator', '0', '0')
	VALUES ('1040', 'PI_ID2', 'Secondary Investigator id', '0', '0');

INSERT INTO `ibdbv2_wheat_merged`.`cvtermprop` (`cvterm_id`, `type_id`, `value`, `rank`) VALUES ('38523', '1800', 'Study Detail', '0');
INSERT INTO `ibdbv2_wheat_merged`.`cvtermprop` (`cvterm_id`, `type_id`, `value`, `rank`) VALUES ('38524', '1800', 'Study Detail', '0');


INSERT INTO `ibdbv2_wheat_merged`.`cvterm_relationship` (`type_id`, `subject_id`, `object_id`) VALUES ('1200', '38523', '2080');
INSERT INTO `ibdbv2_wheat_merged`.`cvterm_relationship` (`type_id`, `subject_id`, `object_id`) VALUES ('1210', '38523', '4030');
INSERT INTO `ibdbv2_wheat_merged`.`cvterm_relationship` (`type_id`, `subject_id`, `object_id`) VALUES ('1220', '38523', '1901');
INSERT INTO `ibdbv2_wheat_merged`.`cvterm_relationship` (`type_id`, `subject_id`, `object_id`) VALUES ('1200', '38524', '2080');
INSERT INTO `ibdbv2_wheat_merged`.`cvterm_relationship` (`type_id`, `subject_id`, `object_id`) VALUES ('1210', '38524', '4030');
INSERT INTO `ibdbv2_wheat_merged`.`cvterm_relationship` (`type_id`, `subject_id`, `object_id`) VALUES ('1220', '38524', '1902');

*/
