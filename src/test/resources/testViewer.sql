-- ==================== STUDY SERVICE TEST

set @studyName := 'Test1-CWM12SVK_925469';
select @listId := listId from listnms where listname = @studyName and listtype='LST';
select @listId2 := listId from listnms where listname = @studyName and listtype='TRIAL';

select @projectId1 := project_id from project where name = @studyName;
select @projectId2 := project_id from project where name = concat(@studyName,'-PLOTDATA');
select @projectId3 := project_id from project where name = concat(@studyName,'-ENVIRONMENT');


select * from listnms where listid in (@listid,@listid2);
select * from listdata where listid = @listId;
select * from listdata_project where list_id = @listId2;


-- project
select * from project where project_id in (@projectId1,@projectId2,@projectId3);
select * from project_relationship where 
	subject_project_id in (@projectId1,@projectId2,@projectId3);
select * from projectprop where project_id in (@projectId1,@projectId2,@projectId3) order by projectprop_id;

-- experiment
select * from nd_experiment_project where project_id in (@projectId1,@projectId2,@projectId3);
select * from nd_experiment where nd_experiment_id in (select nd_experiment_id from nd_experiment_project where project_id in (@projectId1,@projectId2,@projectId3));
select * from nd_experimentprop where nd_experiment_id in (select nd_experiment_id from nd_experiment_project where project_id in (@projectId1,@projectId2,@projectId3));

select * from nd_geolocation where nd_geolocation_id in (
	select nd_geolocation_id from nd_experiment where nd_experiment_id in (select nd_experiment_id from nd_experiment_project where project_id in (@projectId1,@projectId2,@projectId3))
);
select * from nd_geolocationprop where nd_geolocation_id in(
	select nd_geolocation_id from nd_experiment where nd_experiment_id in (select nd_experiment_id from nd_experiment_project where project_id in (@projectId1,@projectId2,@projectId3))
);

-- stock
select * from nd_experiment_stock where nd_experiment_id in(
	select nd_experiment_id from nd_experiment_project where  project_id in (@projectId1,@projectId2,@projectId3)
);

select * from stock where  stock_id in(
	select stock_id from nd_experiment_stock where nd_experiment_id in(
		select nd_experiment_id from nd_experiment_project where  project_id in (@projectId1,@projectId2,@projectId3)
	)
);

select * from stockprop where  stock_id in(
	select stock_id from nd_experiment_stock where nd_experiment_id in(
		select nd_experiment_id from nd_experiment_project where  project_id in (@projectId1,@projectId2,@projectId3)
	)
);


select * from nd_experiment_phenotype where nd_experiment_id in(
	select nd_experiment_id from nd_experiment_project where  project_id in (@projectId1,@projectId2,@projectId3)
);

select * from phenotype where phenotype_id in(
	select phenotype_id from nd_experiment_phenotype where nd_experiment_id in(
		select nd_experiment_id from nd_experiment_project where  project_id in (@projectId1,@projectId2,@projectId3))
);





-- DELETES
delete from listdata_project where list_id = @listId2 and listdata_project_id>0;
delete from listdata where listid = @listId;
delete from listnms where listid in (@listId,@listId2) and listid > 0;



delete from phenotype where phenotype_id in(
	select phenotype_id from nd_experiment_phenotype where nd_experiment_id in(
		select nd_experiment_id from nd_experiment_project where  project_id in (@projectId1,@projectId2,@projectId3))
) and phenotype_id>0;

delete from stock where  stock_id in(
	select stock_id from nd_experiment_stock where nd_experiment_id in(
		select nd_experiment_id from nd_experiment_project where  project_id in (@projectId1,@projectId2,@projectId3)
	)
) and stock_id > 0;

delete from nd_geolocation where nd_geolocation_id in (
	select nd_geolocation_id from nd_experiment where nd_experiment_id in (select nd_experiment_id from nd_experiment_project where project_id in (@projectId1,@projectId2,@projectId3))
)and nd_geolocation_id>0;

delete from project where project_id in (@projectId1,@projectId2,@projectId3); -- cascades to projectprop, project_relationship and experiment_prop



/*
********************************** DELETE ALL


delete from listdata_project where listdata_project_id>0;
delete from listdata where listid > 0;
delete from listnms where listid > 0;
delete from phenotype where phenotype_id>0;
delete from stock where  stock_id > 0;
delete from nd_geolocation where nd_geolocation_id>0;
delete from project where project_id not in (1,2000); -- root, templates and migration folders


select * from listnms; -- germplasm lists (definition)
select * from listdata; -- germplasm entries for generic list
select * from listdata_project; -- germplasm entries for trial list

select * from project; -- trial definition

select * from stock; -- germplasm list (for tial design)
select * from nd_geolocation; -- occurences go here
select * from nd_geolocationprop; -- occurrence metadata values go here
select * from phenotype;        -- trait values go here

*/
