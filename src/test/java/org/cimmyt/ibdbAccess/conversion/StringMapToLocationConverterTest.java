package org.cimmyt.ibdbAccess.conversion;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.cimmyt.ibdbAccess.IbdbAccessTest;
import org.cimmyt.ibdbAccess.core.domain.Location;
import org.cimmyt.ibdbAccess.front.domain.term.LocationTerm;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@IfProfileValue(name="test_group", values={"test"})
@RunWith(SpringJUnit4ClassRunner.class)
public class StringMapToLocationConverterTest extends IbdbAccessTest{

	@Autowired
	BeanConverter converter;
	
	@Test
	public void canFindLocationByAbbreviationAndCountry(){
		Map<Integer, String> occExtraMetadata = new HashMap<>();
		occExtraMetadata.put(LocationTerm.LOC_ABBREVIATION.id(), "MBY");
		occExtraMetadata.put(LocationTerm.OCC_COUNTRY.id(), "MEX");
		
		Location loc = converter.convertComposite(occExtraMetadata, Integer.class, String.class, Location.class);
		
		assertThat(loc.getLname(), equalTo("BATAN BARLEY YELLOW "));
		assertThat(loc.getLabbr(), equalTo("MBY  "));
		assertThat(loc.getCntryid(), equalTo(140));
		
	}

}
