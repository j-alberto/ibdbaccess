package org.cimmyt.ibdbAccess.core.repository;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.cimmyt.ibdbAccess.IbdbAccessTest;
import org.cimmyt.ibdbAccess.core.domain.Listnm;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@IfProfileValue(name="test_group", values={"test"})
@RunWith(SpringJUnit4ClassRunner.class)
public class ListnmRepositoryTest extends IbdbAccessTest{
	
	@Autowired
	ListnmRepository listRepository;
	
	@Test
	public void exists(){
		Listnm list = listRepository.findOne(1);
		
		assertThat(list.getListname(), is("Bread Wheat"));
	}
	
}
