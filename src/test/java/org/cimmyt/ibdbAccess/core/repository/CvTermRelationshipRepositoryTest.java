package org.cimmyt.ibdbAccess.core.repository;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.cimmyt.ibdbAccess.IbdbAccessTest;
import org.cimmyt.ibdbAccess.core.domain.CvtermRelationship;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@IfProfileValue(name="test_group", values={"test"})
@RunWith(SpringJUnit4ClassRunner.class)
public class CvTermRelationshipRepositoryTest extends IbdbAccessTest{
	
	@Autowired
	CvTermRelationshipRepository cvTermRelRepo;
	
	@Test
	public void exists(){
		int termId = 28351;
		int relationshipType = 1044;
		int expectedTermId = 1010;
		
		CvtermRelationship rel = cvTermRelRepo.findBySubjectCvtermIdAndTypeId(termId,relationshipType);
		assertThat(rel, notNullValue());
		assertThat(rel.getObject().getCvtermId() , equalTo(expectedTermId));
		
	}
	
	
	@Test
	public void listExists(){
		
		Set<Integer> ids = new HashSet<>();
		ids.add(28351);
		ids.add(8680);
		ids.add(23264);
		int relationshipType = 1044;
		
		
		List<CvtermRelationship> rels = cvTermRelRepo.findBySubjectCvtermIdInAndTypeId(ids,relationshipType);
		assertThat(rels.size(), equalTo(3));

		Set<String> termNames = new HashSet<String>();
		for(CvtermRelationship rel : rels){
			termNames.add(rel.getObject().getName());
			termNames.add(rel.getSubject().getName());
		}
		
		assertThat(termNames, containsInAnyOrder("Study Information","STUDY_ABBR","Organization","TID"));
		
	}
	
}
