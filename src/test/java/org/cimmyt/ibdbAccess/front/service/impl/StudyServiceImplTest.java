package org.cimmyt.ibdbAccess.front.service.impl;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertThat;

import org.cimmyt.ibdbAccess.IbdbAccessTest;
import org.cimmyt.ibdbAccess.front.service.StudyService;
import org.cimmyt.ibdbAccess.util.MockStudy;
import org.cimmyt.ibdbAccess.util.QueryTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@IfProfileValue(name="test_group", values={"test"})
@RunWith(SpringJUnit4ClassRunner.class)
public class StudyServiceImplTest extends IbdbAccessTest{
	
	@Autowired
	private StudyService studyService;
	
	@Test
	public void saveStudy(){
	    Runtime r = Runtime.getRuntime();
	    long millis = System.currentTimeMillis();
	    
	    boolean cleanAtEnd = true;
	            
        LOG.info("Testing saveStudy. BEFORE> maxMex: {}, freeMem: {}, totalMem: {}", r.maxMemory()/1024, r.freeMemory()/1024, r.totalMemory()/1024);
		Integer studyId = studyService.saveStudy(MockStudy.createMockStudy());
        LOG.info("Testing saveStudy. AFTER> maxMex: {}, freeMem: {}, totalMem: {}, time: {}", r.maxMemory()/1024, r.freeMemory()/1024, r.totalMemory()/1024,
                (System.currentTimeMillis()-millis)/1000.0);

		assertThat(studyId, greaterThan(0));
		
		if( cleanAtEnd ){
	        LOG.info("Finished saveStudy, removing test entities...");
		    QueryTemplate.removeAllStudyEntities(jdbcTmp, studyId);
	        LOG.info("test entities removed");
		} else {
		    LOG.warn("Test entities not erased after testing, please check your test DB");
		}
		
	}
	

}
