package org.cimmyt.ibdbAccess.front.manager;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.cimmyt.ibdbAccess.IbdbAccessTest;
import org.cimmyt.ibdbAccess.core.domain.ListdataProject;
import org.cimmyt.ibdbAccess.core.domain.Project;
import org.cimmyt.ibdbAccess.core.domain.StudyId;
import org.cimmyt.ibdbAccess.front.domain.GermplasmListEntry;
import org.cimmyt.ibdbAccess.front.domain.ListEntryType;
import org.cimmyt.ibdbAccess.front.domain.Study;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@IfProfileValue(name="test_group", values={"test"})
@RunWith(SpringJUnit4ClassRunner.class)
public class ListManagerTest extends IbdbAccessTest{
	
	@Autowired
	private ListManager listManager;

	private Study testStudy = initTestStudy();

	private Study initTestStudy(){

        GermplasmListEntry entry1 = new GermplasmListEntry();
        GermplasmListEntry entry2 = new GermplasmListEntry();
        
        entry1.setEntryNum(1);
        entry1.setSelHist("test sel hist 1");
        entry1.setGid(12345);
        entry1.setCrossName("cross name 1");
        entry1.setSource("source 1");
        entry1.setType(ListEntryType.TEST);

        entry2.setEntryNum(2);
        entry2.setSelHist("test sel hist 2");
        entry2.setGid(12346);
        entry2.setCrossName("cross name 2");
        entry2.setSource("source 2");
        entry2.setType(ListEntryType.CHECK);

        
        List<GermplasmListEntry> entries = new ArrayList<>();
        entries.add(entry1);
        entries.add(entry2);

        Study study = new Study();
        study.setName("test name");
        study.setStartDate(20151001);
        study.setDescription("test description");
        study.setStudyType("T");

        study.setTid("123321");
        study.setUserId(102);
        
        study.setGermplasmEntries(entries);
        
        return study;
    }

	@Test
	public void saveListWithEntries(){
		LOG.info("Testing saveListWithEntries...");
		
		int listId = listManager.saveList(testStudy);

		Integer numEntities = jdbcTmp.queryForObject(
				"select count(1) from listnms where listid ="+listId,
				Integer.class);
		Integer numListDataEntities = jdbcTmp.queryForObject(
				"select count(1) from listdata where listid ="+listId,
				Integer.class);

		assertThat(numEntities, equalTo(1));
		assertThat(numListDataEntities, equalTo(2));
		
		LOG.info("Finished saveListWithEntries, removing test entities");
		jdbcTmp.execute("delete from listdata where listid = "+listId);
		jdbcTmp.execute("delete from listnms where listid = "+listId);
		LOG.info("saveListWithEntries test entities removed");
		
	}
	
	   @Test
	    public void saveListWithEntriesForTrial(){

	        StudyId studyId = new StudyId(new Project(111), null, null);
	        Integer referencedListId = 999;
	        
	        LOG.info("Testing saveListWithEntriesForTrial...");
	        
	        List<ListdataProject> listDataProjects = listManager.saveStudyList(testStudy, studyId, referencedListId);
	        int listId = listDataProjects.get(0).getList().getListid();
	        
	        Integer numEntities = jdbcTmp.queryForObject(
	                "select count(1) from listnms where listid ="+listId,
	                Integer.class);
	        Integer numListDataProjectEntities = jdbcTmp.queryForObject(
	                "select count(1) from listdata_project where list_id ="+listId,
	                Integer.class);

	        assertThat(numEntities, equalTo(1));
	        assertThat(numListDataProjectEntities, equalTo(2));
	        
	        LOG.info("Finished saveListWithEntriesForTrial, removing test entities");
	        jdbcTmp.execute("delete from listdata_project where list_id = "+listId);
	        jdbcTmp.execute("delete from listnms where listid = "+listId);
	        LOG.info("saveListWithEntriesForTrial test entities removed");
	        
	    }

	
	


	
	
}
