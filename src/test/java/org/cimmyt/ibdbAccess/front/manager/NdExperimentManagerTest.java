package org.cimmyt.ibdbAccess.front.manager;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.cimmyt.ibdbAccess.IbdbAccessTest;
import org.cimmyt.ibdbAccess.core.domain.NdGeolocation;
import org.cimmyt.ibdbAccess.core.domain.Project;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@IfProfileValue(name="test_group", values={"test"})
@RunWith(SpringJUnit4ClassRunner.class)
public class NdExperimentManagerTest extends IbdbAccessTest{
	
	@Autowired
	NdExperimentManager ndExperimentManager;

	@Test
	public void canFindGeolocationsByProject(){
		int trialId = 2127; //environment dataset of 2126, 28 occurrences
		Project trial = new Project(trialId);
		Page<NdGeolocation> geolocations = ndExperimentManager.findByProject(new PageRequest(0, 20), trial);
		
		assertThat(geolocations.getTotalElements(), equalTo(91L));
		
	}
}
