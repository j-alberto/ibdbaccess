package org.cimmyt.ibdbAccess.front.manager;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.cimmyt.ibdbAccess.IbdbAccessTest;
import org.cimmyt.ibdbAccess.core.domain.WorkbenchProject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@IfProfileValue(name="test_group", values={"test"})
@RunWith(SpringJUnit4ClassRunner.class)
public class ProgramManagerTest extends IbdbAccessTest{
	
	@Autowired
	ProgramManager programManager;

	@Test
	public void canAddNewProgram(){
		int adminUserId = 3;
		WorkbenchProject program = programManager.saveNewProgram("test program", adminUserId);
		
		assertThat(program, allOf(
				hasProperty("projectName", equalTo("test program")),
				hasProperty("projectUuid", notNullValue()),
				hasProperty("projectId", greaterThan(0))));
		
		LOG.debug("Cleaning test canAddNewProgram");
		
		int id= program.getProjectId();
		jdbcTmp.execute("delete from workbench.workbench_ibdb_user_map where project_id = "+id+" and ibdb_user_map_id>0");
		jdbcTmp.execute("delete from workbench.workbench_project_user_info where project_id = "+id);
		jdbcTmp.execute("delete from workbench.workbench_project_user_mysql_account where project_id = "+id);
		jdbcTmp.execute("delete from workbench.workbench_project_user_role where project_id = "+id);
		jdbcTmp.execute("delete from workbench.workbench_project_activity where project_id = "+id);
		jdbcTmp.execute("delete from workbench.workbench_project where project_id = "+id);
		
		LOG.debug("Cleaned test canAddNewProgram successfully");
	}
}
