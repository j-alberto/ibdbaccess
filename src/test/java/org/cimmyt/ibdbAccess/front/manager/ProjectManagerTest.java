package org.cimmyt.ibdbAccess.front.manager;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.cimmyt.ibdbAccess.IbdbAccessTest;
import org.cimmyt.ibdbAccess.core.domain.Project;
import org.cimmyt.ibdbAccess.core.domain.WorkbenchProject;
import org.cimmyt.ibdbAccess.core.repository.WorkbenchProjectRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

@IfProfileValue(name="test_group", values={"test"})
@RunWith(SpringJUnit4ClassRunner.class)
public class ProjectManagerTest extends IbdbAccessTest{
	
	@Autowired
	ProjectManager projectManager;
	@Autowired
	WorkbenchProjectRepository programRepo;
	
	private WorkbenchProject program = null;
	
	@Before
	public void setUp(){
		if(program == null){
			program = programRepo.findByProjectName("Winter Wheat");
		}
	}

	@Test
	public void givenANameCanFindAFolderInsideAProgram(){
		Assert.notNull(program);
		Project p = projectManager.findFolderByNameInProgram("Migration", program);
		
		assertThat(p, notNullValue());
		assertThat(p.getName(), equalTo("Migration"));
		assertThat(p.getProgramUuid(), equalTo(program.getProjectUuid()));
	}
	
	@Test
	public void canSaveAFolderInsideAProgram(){
		Assert.notNull(program);
		Project p = projectManager.saveFolderInProgram("test Folder", program);
		
		assertThat(p, notNullValue());
		assertThat(p.getName(), equalTo("test Folder"));
		assertThat(p.getProgramUuid(), equalTo(program.getProjectUuid()));
		
		jdbcTmp.execute("delete from project where project_id = "+p.getProjectId());
	}
}
