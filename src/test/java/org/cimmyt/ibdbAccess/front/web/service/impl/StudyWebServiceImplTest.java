package org.cimmyt.ibdbAccess.front.web.service.impl;

import static org.cimmyt.ibdbAccess.util.QueryTemplate.countListEntries;
import static org.cimmyt.ibdbAccess.util.QueryTemplate.removeAllStudyEntities;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashSet;
import java.util.Set;

import org.cimmyt.ibdbAccess.IbdbAccessTest;
import org.cimmyt.ibdbAccess.util.MockStudy;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@IfProfileValue(name="test_group", values={"test"})
@RunWith(SpringJUnit4ClassRunner.class)
public class StudyWebServiceImplTest extends IbdbAccessTest{

    private static Set<Integer> studiesToRemove = new HashSet<>();
	
	private Integer rootFolderId = 1;
	
	@Test
	public void canSaveSingleOccurrenceWithoutTraits() throws Exception{

        String studyName="Test-CWM13ZAF_925625";
        int expectedEntries = 242;
        
	    String jsonStudy = MockStudy.loadJSONFromFile("/json/Study925625_1occ_242entries.json");
        
        mvc.perform(post("/siuapi/study")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonStudy)
                )
            .andExpect(status().isOk())
            .andExpect(
                content().string(greaterThan(rootFolderId.toString())));

        Integer studyId = jdbcTmp.queryForObject("select project_id from project where name = '"+studyName+"'", Integer.class);
        studiesToRemove.add(studyId);
        Integer numEntries = countListEntries(jdbcTmp, studyId);
        
        assertThat(studyId, greaterThan(rootFolderId));
        assertThat(numEntries, equalTo(expectedEntries));

	}
	
    @Test
    public void canSaveMultipleOccurrencesWithoutTraits() throws Exception{

        String studyName="Test2-CWM13ZAF_925650";
        int expectedEntries = 15;
        
        String jsonStudy = MockStudy.loadJSONFromFile("/json/StudyX_2occ_15entries.json");
        
        mvc.perform(post("/siuapi/study")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonStudy)
                )
            .andExpect(status().isOk())
            .andExpect(
                content().string(greaterThan(rootFolderId.toString())));

        Integer studyId = jdbcTmp.queryForObject("select project_id from project where name = '"+studyName+"'", Integer.class);
        studiesToRemove.add(studyId);
        Integer numEntries = countListEntries(jdbcTmp, studyId);
        
        assertThat(studyId, greaterThan(rootFolderId));
        assertThat(numEntries, equalTo(expectedEntries));

    }
	   
    @Test
    public void canSaveGermplasmListWithoutOccurrences() throws Exception{

       String studyName="Test1-CWM12SVK_925469";
       int expectedEntries = 5;
       
       String jsonStudy = MockStudy.loadJSONFromFile("/json/StudyX_germplasmList-withoutOccurrences.json");
       
       mvc.perform(post("/siuapi/study")
               .contentType(MediaType.APPLICATION_JSON)
               .content(jsonStudy)
               )
           .andExpect(status().isOk())
           .andExpect(
               content().string(greaterThan(rootFolderId.toString())));

       Integer studyId = jdbcTmp.queryForObject("select project_id from project where name = '"+studyName+"'", Integer.class);
       studiesToRemove.add(studyId);
       Integer numEntries = countListEntries(jdbcTmp, studyId);
       
       assertThat(studyId, greaterThan(rootFolderId));
       assertThat(numEntries, equalTo(expectedEntries));

    }

    @Test
    public void canSaveOccurrencesWithoutGermplasmList() throws Exception{

       String studyName="Test3-CWM12SVK_925469";
       
       String jsonStudy = MockStudy.loadJSONFromFile("/json/StudyX_3occ-withoutGermplasmList.json");
       
       mvc.perform(post("/siuapi/study")
               .contentType(MediaType.APPLICATION_JSON)
               .content(jsonStudy)
               )
           .andExpect(status().isOk())
           .andExpect(
               content().string(greaterThan(rootFolderId.toString())));

       Integer studyId = jdbcTmp.queryForObject("select project_id from project where name = '"+studyName+"'", Integer.class);
       studiesToRemove.add(studyId);
       
       assertThat(studyId, greaterThan(rootFolderId));

    }
    
  @Test
  public void canSaveMultipleOccurencesWithTraits() throws Exception{

     String studyName="12TH IBYT_101123";
     int expectedEntries = 25;
     
     String jsonStudy = MockStudy.loadJSONFromFile("/json/studyX_WithTraits.json");
     
     mvc.perform(post("/siuapi/study")
             .contentType(MediaType.APPLICATION_JSON)
             .content(jsonStudy)
             )
         .andExpect(status().isOk())
         .andExpect(
             content().string(greaterThan(rootFolderId.toString())));

     Integer studyId = jdbcTmp.queryForObject("select project_id from project where name = '"+studyName+"'", Integer.class);
     studiesToRemove.add(studyId);
     Integer numEntries = countListEntries(jdbcTmp, studyId);
     
     assertThat(studyId, greaterThan(rootFolderId));
     assertThat(numEntries, equalTo(expectedEntries));

  }

  @Test
  public void canSaveOnlyStudyDetail() throws Exception{

     String studyName="Test-B5_112233";
     
     String jsonStudy = MockStudy.loadJSONFromFile("/json/StudyX_onlyStudyDetail.json");
     
     mvc.perform(post("/siuapi/study")
             .contentType(MediaType.APPLICATION_JSON)
             .content(jsonStudy)
             )
         .andExpect(status().isOk())
         .andExpect(
             content().string(greaterThan(rootFolderId.toString())));

     Integer studyId = jdbcTmp.queryForObject("select project_id from project where name = '"+studyName+"'", Integer.class);
     studiesToRemove.add(studyId);
     
     assertThat(studyId, greaterThan(rootFolderId));

  }


    /**
     * Removing entities from jdbcTemplate causes hibernate EntityManager to have unexpected behaviour
     * 
     */
    @AfterClass
    public static void atEndClasss(){
        for (Integer id : studiesToRemove) {
            removeAllStudyEntities(jdbcTmp, id);
        }
    }
	   

}
