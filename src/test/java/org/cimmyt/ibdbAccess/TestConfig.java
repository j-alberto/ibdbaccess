package org.cimmyt.ibdbAccess;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@EnableAutoConfiguration
@ComponentScan(basePackages = { "org.cimmyt.ibdbAccess" })
@Configuration
public class TestConfig {

}
