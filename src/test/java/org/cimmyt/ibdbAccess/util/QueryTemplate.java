package org.cimmyt.ibdbAccess.util;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.cimmyt.ibdbAccess.front.domain.ListType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

public class QueryTemplate {
    
    private static final String COUNT_STUDY_ENTRIES_BY_STUDYID = "select count(1) from listdata_project p"
            + " inner join listnms l on l.listid = p.list_id where l.projectid = %s";
    private static String SELECT_LIST_BY_NAME_AND_TYPE = "select listId from listnms where listname = '%s' and listtype='%s'";

    private static final Logger LOG = LoggerFactory.getLogger(QueryTemplate.class);
    
    public static void removeAllStudyEntities(JdbcTemplate jdbc, int studyId){
        String studyName = jdbc.queryForObject("select name from project where project_id = "+ studyId, String.class);
        LOG.debug("Cleaning test study: {}", studyName);

        try{
            int studyId1 =  jdbc.queryForObject(String.format(SELECT_LIST_BY_NAME_AND_TYPE, studyName, ListType.LST.toString()), Integer.class);
            int studyId2 =  jdbc.queryForObject(String.format(SELECT_LIST_BY_NAME_AND_TYPE, studyName, ListType.TRIAL.toString()), Integer.class);
    
            jdbc.execute("delete from listdata_project where list_id = "+studyId2);
            jdbc.execute("delete from listdata where listid = "+studyId1);
            jdbc.execute(String.format("delete from listnms where listid in(%1$s,%2$s)",studyId1,studyId2));
        
        }catch(DataAccessException ex){
            LOG.warn("No list to delete for study {}", studyName);
        }
        
        String filterByProjects = String.format(" where project_id in (%1$s, %2$s, %3$s)", studyId, studyId+1, studyId+2);
        String deleteFromPhenotype = String.format("delete from phenotype where phenotype_id in("
                 + "select phenotype_id from nd_experiment_phenotype where nd_experiment_id in("
                 + "select nd_experiment_id from nd_experiment_project"
                 + "%s"
                 +"))and phenotype_id>0", filterByProjects);
        String deleteFromStock = String.format("delete from stock where  stock_id in("
                 + "select stock_id from nd_experiment_stock where nd_experiment_id in("
                 + "select nd_experiment_id from nd_experiment_project"
                 + "%s"
                 +")) and stock_id > 0", filterByProjects);
        String deleteFromGeolocation = String.format("delete from nd_geolocation where nd_geolocation_id in ("
                 + "select nd_geolocation_id from nd_experiment where nd_experiment_id in (select nd_experiment_id from nd_experiment_project "
                 + "%s))and nd_geolocation_id>0", filterByProjects);
        String deleteFromProject = String.format("delete from project %s", filterByProjects);
         
        jdbc.execute(deleteFromPhenotype);
        jdbc.execute(deleteFromStock);
        jdbc.execute(deleteFromGeolocation);
        jdbc.execute(deleteFromProject); 
        
        LOG.debug("Test {} removed", studyName);
    }
    
    public static void removeAllStudyEntities(EntityManager em, int studyId){
        em.getTransaction().begin();
        Query query = em.createNativeQuery("select name from project where project_id = "+ studyId, String.class);
        String studyName =query.getSingleResult().toString();
        
        String selectListByNameAndType = "select listId from listnms where listname = '%s' and listtype='%s'";
        
        query = em.createNativeQuery(String.format(selectListByNameAndType, studyName, ListType.LST.toString()), Integer.class);
        int studyId1 = Integer.parseInt(query.getSingleResult().toString());
        query =  em.createNativeQuery(String.format(selectListByNameAndType, studyName, ListType.TRIAL.toString()), Integer.class);
        int studyId2 = Integer.parseInt(query.getSingleResult().toString());
                
        em.createNativeQuery("delete from listdata_project where list_id = "+studyId2).executeUpdate();
        em.createNativeQuery("delete from listdata where listid = "+studyId1).executeUpdate();
        em.createNativeQuery(String.format("delete from listnms where listid in(%1$s,%2$s)",studyId1,studyId2)).executeUpdate();
         
        String filterByProjects = String.format(" where project_id in (%1$s, %2$s, %3$s)", studyId, studyId+1, studyId+2);
        String deleteFromPhenotype = String.format("delete from phenotype where phenotype_id in("
                 + "select phenotype_id from nd_experiment_phenotype where nd_experiment_id in("
                 + "select nd_experiment_id from nd_experiment_project"
                 + "%s"
                 +"))and phenotype_id>0", filterByProjects);
        String deleteFromStock = String.format("delete from stock where  stock_id in("
                 + "select stock_id from nd_experiment_stock where nd_experiment_id in("
                 + "select nd_experiment_id from nd_experiment_project"
                 + "%s"
                 +")) and stock_id > 0", filterByProjects);
        String deleteFromGeolocation = String.format("delete from nd_geolocation where nd_geolocation_id in ("
                 + "select nd_geolocation_id from nd_experiment where nd_experiment_id in (select nd_experiment_id from nd_experiment_project "
                 + "%s))and nd_geolocation_id>0", filterByProjects);
        String deleteFromProject = String.format("delete from project %s", filterByProjects);
         
        em.createNativeQuery(deleteFromPhenotype).executeUpdate();
        em.createNativeQuery(deleteFromStock).executeUpdate();
        em.createNativeQuery(deleteFromGeolocation).executeUpdate();
        em.createNativeQuery(deleteFromProject).executeUpdate();
        em.getTransaction().commit();
    }

    
    public static Integer countListEntries(JdbcTemplate jdbc, int studyId){
        return jdbc.queryForObject(String.format(COUNT_STUDY_ENTRIES_BY_STUDYID,studyId), Integer.class);
    }
    
}
