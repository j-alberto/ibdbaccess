package org.cimmyt.ibdbAccess.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cimmyt.ibdbAccess.front.domain.GermplasmListEntry;
import org.cimmyt.ibdbAccess.front.domain.ListEntryType;
import org.cimmyt.ibdbAccess.front.domain.Occurrence;
import org.cimmyt.ibdbAccess.front.domain.Study;
import org.cimmyt.ibdbAccess.front.domain.StudyEntry;
import org.cimmyt.ibdbAccess.front.domain.term.ProjectPropDescriptorTerm;
import org.cimmyt.ibdbAccess.front.web.service.impl.StudyWebServiceImplTest;

public class MockStudy {

    public static Study createMockStudy(){
        int entriesByOccurrence = 10;
        int numOccurrences = 2;

        Study testStudy = new Study();
        testStudy.setDescription("test description");
        testStudy.setEndDate(20150930);
        testStudy.setName("test name");
        testStudy.setObjective("test objective");
        testStudy.setStartDate(20150921);
        testStudy.setStudyType("T");
        testStudy.setTid("9999");
        testStudy.setUserId(3); //3: admin
        
        Map<Integer, String> studyMeta = new HashMap<>();
        studyMeta.put(ProjectPropDescriptorTerm.STUDY_NEW_CYCLE.asTerm().getCvtermId(), "2015Test");
        studyMeta.put(ProjectPropDescriptorTerm.STUDY_PROGRAM.asTerm().getCvtermId(), "test program");
        testStudy.setExtraMetadata(studyMeta);
        
        List<GermplasmListEntry> germEntries = new ArrayList<>(3);
        germEntries.add(new GermplasmListEntry(1, 12345, "sel hist 1", "source 1",
                "cross name 1", ListEntryType.TEST));
        germEntries.add(new GermplasmListEntry(2, 12346, "sel hist 2", "source 2",
                "cross name 2", ListEntryType.TEST));
        germEntries.add(new GermplasmListEntry(3, 12347, "sel hist 3", "source 3",
                "cross name 3", ListEntryType.CHECK));
        
        testStudy.setGermplasmEntries(germEntries);
        
        Occurrence instance1 = new Occurrence();
        instance1.setNumber(1);

        Occurrence instance2 = new Occurrence();
        instance2.setNumber(2);

        List<StudyEntry> entries = new ArrayList<StudyEntry>();

        for(int i = 1; i <= entriesByOccurrence; i++){
            StudyEntry entry = new StudyEntry();
            entry.setCol(null);
            entry.setRow(null);
            entry.setEntryNum(3-i%3);
            entry.setPlot(i);
            entry.setRep(1);
            entry.setSubBlock(null);
            
            Map<Integer, String> traitMap = new HashMap<>();
            traitMap.put(29087, "12."+i);
            entry.setTraitValues(traitMap);
            
            entries.add(entry);
        }
        
        instance1.setEntries(entries);
        instance1.getExtraMetadata().put(22621, "45"); //occ_offset
        instance1.getExtraMetadata().put(8189, "MBY"); //loc abbreviation
        instance1.getExtraMetadata().put(22616,"MEX"); //country code (occ_country)
        instance1.getExtraMetadata().put(8131,"1"); //replicates
        instance1.getExtraMetadata().put(8135,"10110"); //design (rnd complete block)
        instance2.setEntries(entries);
        instance2.getExtraMetadata().put(22621, "450"); //occ_offset
        instance2.getExtraMetadata().put(8131,"1"); //replicates
        instance2.getExtraMetadata().put(8135,"10110"); //design (rnd complete block) 

        List<Occurrence> instances = new ArrayList<>();
        instances.add(instance1);
        
      for(int i=1; i < numOccurrences; i++){
            instances.add(instance2);
      }
        testStudy.setOccurrences(instances);

        return testStudy;
    }
    
    public static String loadJSONFromFile(String filename) throws IOException{
        InputStream s = StudyWebServiceImplTest.class.getResourceAsStream(filename);
        BufferedReader reader = new BufferedReader(new InputStreamReader(s));

        StringBuilder jsonStudy = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            jsonStudy.append(line);
        }
        
        return jsonStudy.toString();
    }
}
